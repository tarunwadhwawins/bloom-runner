<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Bloom Runner</title>
      <!--favicon-->
      <link rel="icon" type="image/x-icon" href="/assets/images/favicon.png">
      <!--common css-->
      <link rel="stylesheet" href="assets/css/common.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/header.css" type="text/css"/>
	   <link rel="stylesheet" href="assets/css/sidebar.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/fonts.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/footer.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/materialdesignicons.min.css" type="text/css"/>
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic|Roboto+Mono:400,500|Material+Icons" rel="stylesheet">
      <!---->
      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet"/>
<!--      <link rel="stylesheet" href="assets/css/dataTables.css" type="text/css"/>-->
      <!---->
   </head>
   <body>
      <!---->
      <div class="pcoded-container navbar-wrapper">
      <?php include_once('common/header.php') ?>
      <!---->
      <div class="mainContainer">
         <?php include_once('common/sidebar.php') ?> 
         <!----> 
         <div class="pcodedContent">
            <div class="pcodedInnerContent">
               <div class="pageBody">
				  <div class="row">
				  <div class="col-sm-12">
					<h4 class="page-title">Product List</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Product List</li>
					 </ol>
				 </div>
				  </div>
                  <div class="row">
                     <div class="col-12 col-lg-12">
                        <div class="card ">
                           <div class="card-body orderTables">
                              <div class="card-title">Product List</div>
                              <div class="btn-group mr10 toolbar">
                                 <form id="product_grid_search" method="" name="product_grid_search" class="form-inline" role="form">
                                    <div class="form-group">
                                       <div class="input-group input-group-sm">
                                          <input type="text" name="keyword" id="product_grid_search_keyword" class="form-control atext " value="" data-orgvalue="" placeholder="Search for product">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="input-group input-group-sm">
                                          <select class="form-control aselect " data-placeholder="" name="match" id="product_grid_search_match" data-orgvalue="">
                                             <option value="any" data-orgvalue="false">Any of these words</option>
                                             <option value="all" data-orgvalue="false">All of these words</option>
                                             <option value="exact" data-orgvalue="false">The exact phrase</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="input-group input-group-sm">
                                          <input type="text" name="pfrom" id="product_grid_search_pfrom" class="form-control atext small-field" value="" data-orgvalue="" placeholder="0">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="input-group input-group-sm">
                                          <input type="text" name="pto" id="product_grid_search_pto" class="form-control atext small-field" value="" data-orgvalue="" placeholder="max">
                                       </div>
                                    </div>
									<div class="form-group">
                                       <div class="input-group input-group-sm">
                                          <select class="form-control aselect " data-placeholder="- Select Category" name="Category">
                                             <option value="0" data-orgvalue="false">Flowers</option>
                                             <option value="1" data-orgvalue="true">Flowers</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="input-group input-group-sm">
                                          <select class="form-control aselect " data-placeholder="- Select Status -" name="status" id="product_grid_search_status" data-orgvalue="">
                                             <option value="1" data-orgvalue="false">Enabled</option>
                                             <option value="0" data-orgvalue="true">Disabled</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <button type="submit" class="btn btn-xs btn-primary tooltips" title="" data-original-title="Filter">
                                       GO							</button>
                                       <button type="reset" class="btn btn-xs btn-default tooltips" title="" data-original-title="Reset">
                                       <i class="fa fa-refresh"></i>
                                       </button>
                                    </div>
                                 </form>

                              </div>
							  <div class="proListing">
                              <div class="table-responsive">
                                 <table class="table align-items-center table-flush" id="dataTable1">
                                    <thead>
                                       <tr>
                                          <th>Photo</th>
                                          <th>Product</th>
                                          <th>Amount</th>
                                          <th>Status</th>
                                          <th class="text-right">Actions</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr>
                                          <td>
                                             <img alt="Image placeholder" src="assets/images/product.jpg" class="product-img">
                                          </td>
                                          <td>Headphone GL</td>
                                          <td>$1,840 USD</td>
                                          <td>
                                             <span class="badge-dot">
                                             <i class="bg-danger"></i> pending
                                             </span>
                                          </td>
                                          <td class="td-actions text-right">
                                             <button type="button" rel="tooltip" class="btn btn-info btn-sm">
                                             <i class="material-icons">person</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-success btn-sm">
                                             <i class="material-icons">edit</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                                             <i class="material-icons">close</i>
                                             </button>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <img alt="Image placeh
                                                older" src="assets/images/product.jpg" class="product-img">
                                          </td>
                                          <td>Clasic Shoes</td>
                                          <td>$1,520 USD</td>
                                          <td>
                                             <span class="badge-dot">
                                             <i class="bg-success"></i> completed
                                             </span>
                                          </td>
                                          <td class="td-actions text-right">
                                             <button type="button" rel="tooltip" class="btn btn-info btn-sm">
                                             <i class="material-icons">person</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-success btn-sm">
                                             <i class="material-icons">edit</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                                             <i class="material-icons">close</i>
                                             </button>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <img alt="Image placeholder" src="assets/images/product.jpg" class="product-img">
                                          </td>
                                          <td>Hand Watch</td>
                                          <td>$1,620 USD</td>
                                          <td>
                                             <span class="badge-dot">
                                             <i class="bg-warning"></i> delayed
                                             </span>
                                          </td>
                                          <td class="td-actions text-right">
                                             <button type="button" rel="tooltip" class="btn btn-info btn-sm">
                                             <i class="material-icons">person</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-success btn-sm">
                                             <i class="material-icons">edit</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                                             <i class="material-icons">close</i>
                                             </button>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <img alt="Image placeholder" src="assets/images/product.jpg" class="product-img">
                                          </td>
                                          <td>Hand Camera</td>
                                          <td>$2,220 USD</td>
                                          <td>
                                             <span class="badge-dot">
                                             <i class="bg-info"></i> on schedule
                                             </span>
                                          </td>
                                          <td class="td-actions text-right">
                                             <button type="button" rel="tooltip" class="btn btn-info btn-sm">
                                             <i class="material-icons">person</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-success btn-sm">
                                             <i class="material-icons">edit</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                                             <i class="material-icons">close</i>
                                             </button>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <img alt="Image placeholder" src="assets/images/product.jpg" class="product-img">
                                          </td>
                                          <td>Iphone-X Pro</td>
                                          <td>$9,890 USD</td>
                                          <td>
                                             <span class="badge-dot">
                                             <i class="bg-success"></i> completed
                                             </span>
                                          </td>
                                          <td class="td-actions text-right">
                                             <button type="button" rel="tooltip" class="btn btn-info btn-sm">
                                             <i class="material-icons">person</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-success btn-sm">
                                             <i class="material-icons">edit</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                                             <i class="material-icons">close</i>
                                             </button>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <img alt="Image placeholder" src="assets/images/product.jpg" class="product-img">
                                          </td>
                                          <td>Ladies Purse</td>
                                          <td>$3,420 USD</td>
                                          <td>
                                             <span class="badge-dot">
                                             <i class="bg-danger"></i> pending
                                             </span>
                                          </td>
                                          <td class="td-actions text-right">
                                             <button type="button" rel="tooltip" class="btn btn-info btn-sm">
                                             <i class="material-icons">person</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-success btn-sm">
                                             <i class="material-icons">edit</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                                             <i class="material-icons">close</i>
                                             </button>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <img alt="Image placeholder" src="assets/images/product.jpg" class="product-img">
                                          </td>
                                          <td>Ladies Purse</td>
                                          <td>$3,420 USD</td>
                                          <td>
                                             <span class="badge-dot">
                                             <i class="bg-danger"></i> pending
                                             </span>
                                          </td>
                                          <td class="td-actions text-right">
                                             <button type="button" rel="tooltip" class="btn btn-info btn-sm">
                                             <i class="material-icons">person</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-success btn-sm">
                                             <i class="material-icons">edit</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                                             <i class="material-icons">close</i>
                                             </button>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <img alt="Image placeholder" src="assets/images/product.jpg" class="product-img">
                                          </td>
                                          <td>Ladies Purse</td>
                                          <td>$3,420 USD</td>
                                          <td>
                                             <span class="badge-dot">
                                             <i class="bg-danger"></i> pending
                                             </span>
                                          </td>
                                          <td class="td-actions text-right">
                                             <button type="button" rel="tooltip" class="btn btn-info btn-sm">
                                             <i class="material-icons">person</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-success btn-sm">
                                             <i class="material-icons">edit</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                                             <i class="material-icons">close</i>
                                             </button>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <img alt="Image placeholder" src="assets/images/product.jpg" class="product-img">
                                          </td>
                                          <td>Ladies Purse</td>
                                          <td>$3,420 USD</td>
                                          <td>
                                             <span class="badge-dot">
                                             <i class="bg-danger"></i> pending
                                             </span>
                                          </td>
                                          <td class="td-actions text-right">
                                             <button type="button" rel="tooltip" class="btn btn-info btn-sm">
                                             <i class="material-icons">person</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-success btn-sm">
                                             <i class="material-icons">edit</i>
                                             </button>
                                             <button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                                             <i class="material-icons">close</i>
                                             </button>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
							   </div>
							  <div class="proPagination">
								   <ul class="pagination pagination-outline-primary">
									  <li class="page-item"><a class="page-link" href="#">Previous</a></li>
									  <li class="page-item active"><a class="page-link" href="#">1</a></li>
									  <li class="page-item"><a class="page-link" href="#">2</a></li>
									  <li class="page-item"><a class="page-link" href="#">3</a></li>
									  <li class="page-item"><a class="page-link" href="#">Next</a></li>
									</ul>
								   </div>
                           </div>
                        </div>
                     </div>
                     <!---->
                  </div>
               </div>
            </div>
         </div>
         <!---->
      </div>
      <!---->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<!--
      <script src="assets/js/jquery.dataTables.min.js"></script>
      <script src="assets/js/dataTables.bootstrap4.min.js"></script>
      <script src="assets/js/datatables-demo.js"></script>
-->
      <script type="text/javascript">
         $(document).ready(function(){
         $(".menu-link ul").hide();
         // $(".menu-link ul").find('li').hide();
         $('.menu-link').click(function() {
         $(this).children('ul').find('li').show();
         $(this).children('ul').slideToggle();
         });
         });
      </script>
   </body>
</html>