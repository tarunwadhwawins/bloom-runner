<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
	   <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" /> 
	   <meta http-equiv="Pragma" content="no-cache" /> 
	   <meta http-equiv="Expires" content="0" />
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Product - Bloom Runner</title>
      <!--favicon-->
      <link rel="icon" type="image/x-icon" href="assets/images/favicon.png">
      <!--common css-->
      <link rel="stylesheet" href="assets/css/common.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/header.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/sidebar.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/fonts.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/floral.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/footer.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/materialdesignicons.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/tagger.css" type="text/css"/>
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic|Roboto+Mono:400,500|Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/select.dataTables.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css" type="text/css"/>
   </head>
   <body>
      <!---->
      <div class="pcoded-container navbar-wrapper">
      <?php include_once('common/header.php') ?>
      <!---->
      <div class="mainContainer">
      <?php include_once('common/sidebar.php') ?> 
      <div class="pcodedContent">
         <div class="pcodedInnerContent">
            <div class="pageBody">
               <div class="breadcrumbInfo">
                  <div class="row">
                     <div class="col-sm-12">                        
						<div class="row mb-3">
							<div class="col-sm-12 col-md-10">
                              <h4 class="page-title">Product</h4>
							</div>
								<div class="col-sm-12 col-md-2 text-right">
								<div class="headerCommon">
									 <div class="backBtn">
										<a class="btn btn-outline-primary btn-round waves-effect waves-light backActionBtn" href="#" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
									 </div>
								  </div>
							</div>
						</div>
                        <ol class="breadcrumb">
                           <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
                           <li class="breadcrumb-item active" aria-current="page">Product</li>
                        </ol>
                     </div>
                  </div>
               </div>
               <!---->
               <div class="commonTabbing">
<!--
                  <div class="headerCommon">
                     <div class="backBtn">
                        <a class="btn btn-outline-primary btn-round waves-effect waves-light" href="#" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                     </div>
                  </div>
-->
                  <div class="usersListing">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="card">
                              <div class="card-body">
                                 <div class="usersList">
                                    <div class="row">
                                       <!---->
                                       <div class="col-sm-12 col-md-12">
                                          <div class="searchFilter">
                                             <label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="dtBasicExample"></label>
                                          </div>
                                          <div class="advanceSearch">
                                             <div class="dropdown">
                                                <button class="btn btn-outline-primary btn-round waves-effect waves-light dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-filter"></i></button>
                                                <ul class="dropdown-menu">
                                                   <input class="form-control" id="myInput" type="text" placeholder="Search..">
                                                   <li><a href="#">Sort A -> Z</a></li>
                                                   <li><a href="#">Sort Z -> A</a></li>
                                                   <li><a href="#">Sort by Color</a></li>
                                                </ul>
                                             </div>
                                          </div>
										      <div class="createUser">
                                             <a class="btn btn-icon add-product" href="#" title="Add User"><span><img src="assets/images/plus.png" alt="image"/></span></a>
                                          </div> 
                                       </div>
                                       <!---->
                                       <div class="col-sm-12">
                                          <div class="tableGrid">
                                             <div class="table-responsive">
                                                <table id="dtBasicExample" class="table table-hover">
                                                   <thead>
                                                      <tr>
                                                         <th>Image</th>
                                                         <th>Name</th>
                                                         <th>Cost</th>
                                                         <th>Selling Price</th>
                                                         <th>Total Sold</th>
                                                         <th>Status</th>
                                                         <th>Actions</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      <tr>
                                                         <td data-label="Image">
                                                            <img src="assets/images/image-1.jpg">
                                                         </td>
                                                         <td data-label="Name">Succulent Terrariums</td>
                                                         <td data-label="Cost">$15</td>
                                                         <td data-label="Selling Price">$10</td>
                                                         <td data-label="Total Sold">$30</td>
                                                         <td data-label="Status"><span class="badge-dot"><i class="bg-success"></i> Completed</span></td>
                                                         <td data-label="Actions" class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td data-label="Image">
                                                            <img src="assets/images/image-3.jpg">
                                                         </td>
                                                         <td data-label="Name">Planters</td>
                                                         <td data-label="Cost">$15</td>
                                                         <td data-label="Selling Price">$20</td>
                                                         <td data-label="Total Sold">$30</td>
                                                         <td data-label="Status"><span class="badge-dot"><i class="bg-success"></i> Completed</span></td>
                                                         <td data-label="Actions" class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                          <td data-label="Image">
                                                            <img src="assets/images/image-4.jpg">
                                                         </td>
                                                         <td data-label="Name">Chocolates </td>
                                                         <td data-label="Cost">$15</td>
                                                         <td data-label="Selling Price">$15</td>
                                                         <td data-label="Total Sold">$39</td>
                                                         <td data-label="Status"><span class="badge-dot"><i class="bg-danger"></i> pending</span></td>
                                                         <td data-label="Actions" class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td data-label="Image">
                                                            <img src="assets/images/image-1.jpg">
                                                         </td>
                                                         <td data-label="Name">Fragrance</td>
                                                         <td data-label="Cost">$15</td>
                                                         <td data-label="Selling Price">$40</td>
                                                         <td data-label="Total Sold">$30</td>
                                                         <td data-label="Status"><span class="badge-dot"><i class="bg-danger"></i> pending</span></td>
                                                         <td data-label="Actions" class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td data-label="Image">
                                                            <img src="assets/images/image-3.jpg">
                                                         </td>
                                                         <td data-label="Name">Greeting Cards</td>
                                                         <td data-label="Cost">$15</td>
                                                         <td data-label="Selling Price">$10</td>
                                                         <td data-label="Total Sold">$30</td>
                                                         <td data-label="Status"><span class="badge-dot"><i class="bg-success"></i> Completed</span></td>
                                                         <td data-label="Actions" class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                          <td data-label="Image">
                                                            <img src="assets/images/image-4.jpg">
                                                         </td>
                                                         <td data-label="Name">Green and Flowering house plants</td>
                                                         <td data-label="Cost">$15</td>
                                                         <td data-label="Selling Price">$20</td>
                                                         <td data-label="Total Sold">$10</td>
                                                         <td data-label="Status"><span class="badge-dot"><i class="bg-danger"></i> pending</span></td>
                                                         <td data-label="Actions" class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                          <td data-label="Image">
                                                            <img src="assets/images/image-5.jpg">
                                                         </td>
                                                         <td data-label="Name">Hand -tied bouquets</td>
                                                         <td data-label="Cost">$15</td>
                                                         <td data-label="Selling Price">$10</td>
                                                         <td data-label="Total Sold">$20</td>
                                                         <td data-label="Status"><span class="badge-dot"><i class="bg-danger"></i> pending</span></td>
                                                         <td data-label="Actions" class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                          <td data-label="Image">
                                                            <img src="assets/images/image-1.jpg">
                                                         </td>
                                                         <td data-label="Name">Unique containers</td>
                                                         <td data-label="Cost">$15</td>
                                                         <td data-label="Selling Price">$10</td>
                                                         <td data-label="Total Sold">$30</td>
                                                         <td data-label="Status"><span class="badge-dot"><i class="bg-danger"></i> pending</span></td>
                                                         <td data-label="Actions" class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td data-label="Image">
                                                            <img src="assets/images/image-3.jpg">
                                                         </td>
                                                         <td data-label="Name">Vases </td>
                                                         <td data-label="Cost">$15</td>
                                                         <td data-label="Selling Price">$15</td>
                                                         <td data-label="Total Sold">$10</td>
                                                         <td data-label="Status"><span class="badge-dot"><i class="bg-success"></i> Completed</span></td>
                                                         <td data-label="Actions" class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                     <tr>
                                                          <td data-label="Image">
                                                            <img src="assets/images/image-3.jpg">
                                                         </td>
                                                         <td data-label="Name">Planters</td>
                                                         <td data-label="Cost">$15</td>
                                                         <td data-label="Selling Price">$20</td>
                                                         <td data-label="Total Sold">$30</td>
                                                         <td data-label="Status"><span class="badge-dot"><i class="bg-success"></i> Completed</span></td>
                                                         <td data-label="Actions" class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                       </div>
                                       <!---->
                                       <div class="col-sm-6 col-md-6">
                                          <div class="showEntries">
                                             <label>
                                                Show 
                                                <select class="custom-select custom-select-sm form-control form-control-sm">
                                                   <option value="10">10</option>
                                                   <option value="25">25</option>
                                                   <option value="50">50</option>
                                                   <option value="100">100</option>
                                                </select>
                                                entries
                                             </label>
                                          </div>
                                       </div>
                                       <div class="col-sm-6 col-md-6">
                                          <div class="gridPag">
                                             <ul class="pagination">
                                                <li class="page-item disabled">
                                                   <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                </li>
                                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                <li class="page-item active">
                                                   <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                                </li>
                                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                <li class="page-item">
                                                   <a class="page-link" href="#">Next</a>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                       <!---->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="usersData">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="commonTabs">
                              <div class="card">
                                 <div class="card-body">
                                    <ul class="nav nav-tabs nav-tabs-primary">
                                       <li class="nav-item">
                                          <a class="nav-link active" data-toggle="tab" href="#tabe-1"><i class="icon-home"></i> <span class="hidden-xs">Product Details</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-2"><i class="icon-user"></i> <span class="hidden-xs">Seo Fields</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-3"><i class="icon-user"></i> <span class="hidden-xs">Tags</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-4"><i class="icon-user"></i> <span class="hidden-xs">Color Palette</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-5"><i class="icon-user"></i> <span class="hidden-xs">Gallery</span></a>
                                       </li>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-6"><i class="icon-user"></i> <span class="hidden-xs">Pricing</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-7"><i class="icon-user"></i> <span class="hidden-xs">Availability</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-8"><i class="icon-user"></i> <span class="hidden-xs">Addons</span></a>
                                       </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                       <div id="tabe-1" class=" tab-pane active">
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Product Name</label>
                                                   <input type="text" class="form-control"> 
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Product Status</label>
                                                     <select class="form-control">
                                                      <option value="0" selected>Select Status</option>
                                                      <option value="1">Item1</option>
                                                      <option value="1">Item2</option>
                                                   </select>
                                                </div>
                                             </div>
											  <div class="col-sm-12">
                                                <div class="form-group">
                                                   <label>Product Description</label>
                                                   <textarea class="form-control"></textarea>
                                                </div>
                                             </div>
                                             <div class="col-sm-12">
                                                <hr>
                                             </div>
                                             <div class="col-sm-12">
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="tabe-2" class=" tab-pane fade">
                                          <div class="row">
                                             <div class="col-sm-12">
                                                <div class="form-group">
                                                   <label>Meta Description</label>
                                                  <textarea class="form-control"></textarea>
                                                </div>
                                             </div>
                                             <div class="col-sm-12">
                                                <div class="form-group">
                                                   <label for="shtdesc">Meta Keywords</label>
                                                   <input type="text" value="Lorem Ipsum, Lorem Ipsum, Lorem Ipsum" name="tags3" />
                                                </div>
                                             </div>
                                             <div class="col-sm-12">
                                                <hr>
                                             </div>
                                             <div class="col-sm-12">
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Cancel</button>
                                                </div>
                                             </div>
                                          </div>
                                          
                                       </div>
                                       <div id="tabe-3" class=" tab-pane fade">
                                          <div class="row">
                                               <div class="col-sm-12">
                                                <div class="form-group">
                                                   <label for="shtdesc">Tags</label>
                                                   <input type="text" value="Lorem Ipsum, Lorem Ipsum, Lorem Ipsum" name="tags4" />
                                                </div>
                                             </div>
               								<div class="col-sm-12">
                                                <hr>
                                             </div>
                                             <div class="col-sm-12">
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Cancel</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="tabe-4" class=" tab-pane fade">
                                         <div class="row">
										  <div class="col-sm-12">
                                                         <form role="form">
                                                            <fieldset>
                                                               <legend>
                                                                  Choose Color
                                                               </legend>
                                                               <label class="custom-control black-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control lime-green-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control silver-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control blue-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control maroon-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control brown-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control khaki-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control orange-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control silver-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control gray-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control red-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control yellow-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control olive-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control lime-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control green-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control aqua-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control honeydew-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control azure-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control beige-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control ivory-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control lavenderblush-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control darkslategray-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control green-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control brown-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control aliceblue-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control antiquewhite-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control aquamarine-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control bisque-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control blanchedalmond-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control blueviolet-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control cadetblue-checkbox checkBox">

                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control chartreuse-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                            </fieldset>
                                                         </form>
                                                      </div>  
											<div class="col-sm-12">
                                                <hr>
                                             </div>
                                             <div class="col-sm-12">
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Cancel</button>
                                                </div>
                                             </div>
										 </div>   
                                       </div>
                                       <div id="tabe-5" class=" tab-pane fade">
                                          <div class="row">
                                                   <div class="col-md-3 col-sm-6">
                                                      <div class="box">
                                                         <div class="js--image-preview"></div>
                                                         <div class="upload-options">
                                                            <label>
                                                            <input type="file" class="image-upload" accept="image/*" />
                                                            </label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                  <div class="col-md-3 col-sm-6">
                                                      <div class="box">
                                                         <div class="js--image-preview"></div>
                                                         <div class="upload-options">
                                                            <label>
                                                            <input type="file" class="image-upload" accept="image/*" />
                                                            </label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                  <div class="col-md-3 col-sm-6">
                                                      <div class="box">
                                                         <div class="js--image-preview"></div>
                                                         <div class="upload-options">
                                                            <label>
                                                            <input type="file" class="image-upload" accept="image/*" />
                                                            </label>
                                                         </div>
                                                      </div>
                                                   </div>
											        <div class="col-md-3 col-sm-6">
                                                      <div class="box">
                                                         <div class="js--image-preview"></div>
                                                         <div class="upload-options">
                                                            <label>
                                                            <input type="file" class="image-upload" accept="image/*" />
                                                            </label>
                                                         </div>
                                                      </div>
                                                   </div>
											  	<div class="col-sm-12">
                                                <hr>
                                             </div>
                                             <div class="col-sm-12">
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Cancel</button>
                                                </div>
                                             </div>
                                                </div>
                                          
                                       </div>
                                       <div id="tabe-6" class=" tab-pane fade">
                                          <div class="row">
										   
										   <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Tax Excluding Price</label>
                                                   <input type="text" class="form-control"> 
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Tax Including Price</label>
                                                      <input type="text" class="form-control"> 
                                                </div>
                                             </div>
											  <div class="col-sm-4">
                                                <div class="form-group">
                                                   <label>Tax Rate</label>
                                                  <input type="text" class="form-control"> 
                                                </div>
                                             </div>
											    <div class="col-sm-4">
                                                <div class="form-group">
                                                   <label>Regular Price</label>
                                                  <input type="text" class="form-control"> 
                                                </div>
                                             </div>
											    <div class="col-sm-4">
                                                <div class="form-group">
                                                   <label>Sale Price</label>
                                                  <input type="text" class="form-control"> 
                                                </div>
                                             </div>
                                             <div class="col-sm-12">
                                                <hr>
                                             </div>
                                             <div class="col-sm-12">
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                </div>
                                             </div>
										   </div>
                                       </div>
                                       <div id="tabe-7" class=" tab-pane fade">
                                          <div class="row">
										   <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Stock Quantity</label>
                                                   <input type="text" class="form-control"> 
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Stock Availability</label>
                                                    <select class="form-control">
                                                      <option value="0" selected>Limited Stock</option>
                                                      <option value="1">Unlimited Stock</option>
                                                      <option value="1">No Stock</option>
                                                   </select> 
                                                </div>
                                             </div>
								
                                             <div class="col-sm-12">
                                                <hr>
                                             </div>
                                             <div class="col-sm-12">
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                </div>
                                             </div>
										   </div>
                                       </div>
                                       <div id="tabe-8" class=" tab-pane fade">
                                          <div class="row">
											 <div class="col-sm-12">
                                             <form enctype="multipart/form-data">
												  <div class="table-responsive">
                                                    <table class="table table-borderless add-ons-table" id="tbl_posts">
                                                      <thead>
                                                         <tr>
                                                            <th>Option</th>
                                                            <th>Price</th>
                                                            <th></th>
                                                            <th>Actions</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody id="tbl_posts_body">
                                                         <tr id="rec-1">
                                                            <td>
                                                               <div class="form-group">
                                                                  <input type="text" class="form-control" id="description" placeholder="Option">
                                                               </div>
                                                            </td>
                                                            <td>
                                                               <div class="form-group">
                                                                  <select class="form-control" id="stu">
                                                                     <option>Flat Rate</option>
                                                                     <option>Standard Rate</option>
                                                                  </select>
                                                               </div>
                                                            </td>
                                                            <td>
                                                               <div class="form-group">
                                                                  <input type="number" class="form-control" id="description" value="0.00">
                                                               </div>
                                                            </td>
                                                            <td>
                                                               <div class="delete-btn"><a class="btn btn-xs delete-record" data-id="0"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
											   	  </div>
                                                   <div style="display:none;">
                                                      <table id="sample_table">
                                                         <tr id="">
                                                            <td>
                                                               <div class="form-group">
                                                                  <input type="text" class="form-control" id="description" placeholder="Option">
                                                               </div>
                                                            </td>
                                                            <td>
                                                               <div class="form-group">
                                                                  <select class="form-control" id="stu">
                                                                     <option>Flat Rate</option>
                                                                     <option>Standard Rate</option>
                                                                  </select>
                                                               </div>
                                                            </td>
                                                            <td>
                                                               <div class="form-group">
                                                                  <input type="number" class="form-control" id="description" value="0.00">
                                                               </div>
                                                            </td>
                                                            <td>
                                                               <div class="delete-btn"><a class="btn btn-xs delete-record" data-id="0"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                                            </td>
                                                         </tr>
                                                      </table>
                                                   </div>
                                                   <div class="well mt-3">
                                                      <button class="btn btn-outline-primary btn-round waves-effect waves-light add-record" data-added="0"><i class="glyphicon glyphicon-plus"></i> Add Row</button>
                                                   </div>
                                                </form>
                                           </div>  
                                          </div>
                                       </div>
                                       <!---->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!---->
            </div>
         </div>
         <!---->
      </div> 
      <!---->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
      <script src="assets/js/custom.js"></script>
      <script src="assets/js/tagger.js"></script>
      <script>
         var t3 = tagger(document.querySelector('[name="tags3"]'), {
                  allow_duplicates: true,
                  allow_spaces: true,
                });
		   var t3 = tagger(document.querySelector('[name="tags4"]'), {
                  allow_duplicates: true,
                  allow_spaces: true,
                });
           
      </script>
      <script>
         $(document).ready(function() {
         $('#example').DataTable( {
         columnDefs: [ {
         orderable: false,
         className: 'select-checkbox',
         targets:   0
         } ],
         select: {
         style:    'os',
         selector: 'td:first-child'
         },
         order: [[ 1, 'asc' ]]
         } );
         $('#example1').DataTable( {
         columnDefs: [ {
         orderable: false,
         className: 'select-checkbox',
         targets:   0
         } ],
         select: {
         style:    'os',
         selector: 'td:first-child'
         },
         order: [[ 1, 'asc' ]]
         } );
         } ); 
      </script>
      <script>
         $(".add-product").click(function(){
                	$('.usersData').addClass("show");
                	$('.usersListing').addClass("hide");
             $('.headerCommon').addClass("show");
             $('.shortcut').addClass("hide");
                  });  
         $(".backBtn").click(function(){
                	$('.usersData').removeClass("show");
                	$('.usersListing').removeClass("hide");
             $('.headerCommon').removeClass("show");
             $('.shortcut').removeClass("hide");
                  });  
          
      </script>
      <script>
         $(document).ready(function(){
         $('.barIcon').click(function() { 
         $('.br-sideleft').toggleClass('show')
         $('.pcodedContent').toggleClass('show')
             });  
         });  
		 $(document).ready(function(){
			$('.searchBar').click(function() { 
			$('.searchBox').toggleClass('show')
		      });  
			});  
      </script>
		  <script>
         jQuery(document).delegate('.add-record', 'click', function(e) {
            e.preventDefault();    
            var content = jQuery('#sample_table tr'),
            size = jQuery('#tbl_posts >tbody >tr').length + 1,
            element = null,    
            element = content.clone();
            element.attr('id', 'rec-'+size);
            element.find('.delete-record').attr('data-id', size);
            element.appendTo('#tbl_posts_body');
            element.find('.sn').html(size);
          });
      </script>
		        <script>
         jQuery(document).delegate('.delete-record', 'click', function(e) {
            e.preventDefault();    
            var didConfirm = confirm("Are you sure You want to delete");
            if (didConfirm == true) {
             var id = jQuery(this).attr('data-id');
             var targetDiv = jQuery(this).attr('targetDiv');
             jQuery('#rec-' + id).remove();
             
           $('#tbl_posts_body tr').each(function(index) {
             $(this).find('span.sn').html(index+1);
           });
           return true;
         } else {
           return false;
         }
         });
      </script>
      <!---->
   </body>
</html>