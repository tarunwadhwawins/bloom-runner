<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Bloom Runner</title>
      <!--favicon-->
      <link rel="icon" type="image/x-icon" href="/assets/images/favicon.png">
      <!--common css-->
      <link rel="stylesheet" href="assets/css/common.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/header.css" type="text/css"/>
	   <link rel="stylesheet" href="assets/css/sidebar.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/fonts.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/footer.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/materialdesignicons.min.css" type="text/css"/>
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic|Roboto+Mono:400,500|Material+Icons" rel="stylesheet">
      <!---->
      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet"/>
<!--      <link rel="stylesheet" href="assets/css/dataTables.css" type="text/css"/>-->
      <!---->
   </head>
   <body>
      <!---->
      <div class="pcoded-container navbar-wrapper">
      <?php include_once('common/header.php') ?>
      <!---->
      <div class="mainContainer">
         <?php include_once('common/sidebar.php') ?> 
         <!----> 
         <div class="pcodedContent">
            <div class="pcodedInnerContent">
               <div class="pageBody">
				  <div class="row">
				  <div class="col-sm-12">
					<h4 class="page-title">Product List</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Product List</li>
					 </ol>
				 </div>
				  </div>

					<!--<div class="shortcut">
                  <h4>Shortcut</h4>
					<div class="wrapper">
					<a class="btn btn-icon" href="#"><i class="fa fa-product-hunt" aria-hidden="true"></i><span>Add More Products</span></a>
					<a class="btn btn-icon" href="#"><i class="fa fa-trash-o" aria-hidden="true"></i><span>Delete Products</span></a>
					<a class="btn btn-icon" href="#"><i class="fa fa-money" aria-hidden="true"></i><span>Quick Pricing</span></a>
					<a class="btn btn-icon" href="#"><i class="fa fa-hourglass-start" aria-hidden="true"></i><span>Quick Status</span></a>
					<a class="btn btn-icon" href="#"><i class="fa fa-clone" aria-hidden="true"></i><span>Clone/Copy Product</span></a>
					
				</div>
                  </div>-->
                  <div class="row">
                     <div class="col-12 col-lg-12">
                        <div class="card ">
                           <div class="card-body orderTables">
                              <div class="card-title">Product List</div>
								<div class="grid">
								<div class="row">
								<div class="col-sm-2">
	
								<div class="grid-icon">
								<ul>
								<li><a href="#" class="gridView"><i class="fa fa-th" aria-hidden="true"></i></a></li>
								<li><a href="#" class="listingView"><i class="fa fa-list" aria-hidden="true"></i></a></li>
								<li>
								  </li>
								<li>
								
								</li>
								</ul>
								</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-search">
									<span class="fa fa-search form-control-feedback"></span>
									<input type="text" class="form-control" placeholder="Search" data-toggle="collapse" data-target="#demo99"></div>
									  <div id="demo99" class="collapse">
										<div class="row">
										<div class="col-md-6">
										<h5>New</h5>
										<ul>
										<li><img src="assets/images/image-1.jpg"><a href="#">Succulent</a></li>
										<li><img src="assets/images/image-3.jpg"><a href="#">Planters</a></li>
										<li><img src="assets/images/image-4.jpg"><a href="#">Chocolates</a></li>
										<li><img src="assets/images/image-5.jpg"><a href="#">Fragrance</a></li>
										</ul>
										</div>
										<div class="col-md-6 pt-2 pr-4">
										<select class="browser-default custom-select">
									  <option selected value="0">Category</option>
									  <option value="1">Category 1</option>
									  <option value="2">Category 2</option>
									  <option value="3">Category 3</option>
									</select>
										</div>
										</div>
										


									  </div>
									  </div>
								 
								<div class="col-sm-6">
								<div class="shortcut">
									<div class="wrapper">
									<a class="btn btn-icon" href="#"><i class="fa fa-product-hunt" aria-hidden="true"></i><span>Add More Products</span></a>
									<a class="btn btn-icon" href="#"><i class="fa fa-filter" aria-hidden="true"></i><span>Filter</span></a>
									<!--<a class="btn btn-icon" href="#"><i class="fa fa-money" aria-hidden="true"></i><span>Quick Pricing</span></a>
									<a class="btn btn-icon" href="#"><i class="fa fa-hourglass-start" aria-hidden="true"></i><span>Quick Status</span></a>-->
								</div>
								  </div>
								</div>
								</div>
								</div>
							  <div class="proListing">
                              <div class="table-responsive">
                                 <table class="table align-items-center table-flush" id="dataTable1">
                                    <thead>
                                       <tr>
                                          <th>Image</th>
                                          <th>Name</th>
                                          <th>Cost</th>
										  <th>Selling Price</th>
                                          <th>Total Sold</th>
										  <th>Status</th>
                                          <th></th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr>
											<td>
												<img src="assets/images/image-1.jpg">
											</td>
										    <td>Succulent Terrariums</td>
											<td>$15</td>
											<td>$10</td>
											<td>$30</td>
											<td>Lorum Ipsum</td>
											<td>
											<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
											</td>
                                       </tr>

										 <tr>
											<td>
												<img src="assets/images/image-3.jpg">
											</td>
										    <td>Planters</td>
											<td>$15</td>
											<td>$20</td>
											<td>$30</td>
											<td>Lorum Ipsum</td>
											<td>
											<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
											</td>
                                       </tr>

										 <tr>
											<td>
												<img src="assets/images/image-4.jpg">
											</td>
										    <td>Chocolates </td>
											<td>$15</td>
											<td>$15</td>
											<td>$39</td>
											<td>Lorum Ipsum</td>
											<td>
											<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
											</td>
                                       </tr>


										 <tr>
											<td>
												<img src="assets/images/image-1.jpg">
											</td>
										    <td>Fragrance</td>
											<td>$15</td>
											<td>$40</td>
											<td>$30</td>
											<td>Lorum Ipsum</td>
											<td>
											<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
											</td>
                                       </tr>



										 <tr>
											<td>
												<img src="assets/images/image-3.jpg">
											</td>
										    <td>Greeting Cards</td>
											<td>$15</td>
											<td>$10</td>
											<td>$30</td>
											<td>Lorum Ipsum</td>
											<td>
											<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
											</td>
                                       </tr>

										 <tr>
											<td>
												<img src="assets/images/image-4.jpg">
											</td>
										    <td>Green and Flowering house plants</td>
											<td>$15</td>
											<td>$20</td>
											<td>$10</td>
											<td>Lorum Ipsum</td>
											<td>
											<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
											</td>
                                       </tr>


										 <tr>
											<td>
												<img src="assets/images/image-5.jpg">
											</td>
										    <td>Hand -tied bouquets</td>
											<td>$15</td>
											<td>$10</td>
											<td>$20</td>
											<td>Lorum Ipsum</td>
											<td>
											<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
											</td>
                                       </tr>

										 <tr>
											<td>
												<img src="assets/images/image-1.jpg">
											</td>
										    <td>Unique containers</td>
											<td>$15</td>
											<td>$10</td>
											<td>$30</td>
											<td>Lorum Ipsum</td>
											<td>
											<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
											</td>
                                       </tr>

										<tr>
											<td>
												<img src="assets/images/image-3.jpg">
											</td>
										    <td>Vases </td>
											<td>$15</td>
											<td>$15</td>
											<td>$10</td>
											<td>Lorum Ipsum</td>
											<td>
											<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
											</td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>

<div class="product-descr">

 <div class="row">
            <div class="col-md-12">
                <nav class="nav-justified ">
                  <div class="nav nav-tabs " id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="pop1-tab" data-toggle="tab" href="#pop1" role="tab" aria-controls="pop1" aria-selected="true">Basic Info</a>
                    <a class="nav-item nav-link" id="pop2-tab" data-toggle="tab" href="#pop2" role="tab" aria-controls="pop2" aria-selected="false">Product Images</a>
                    <a class="nav-item nav-link" id="pop3-tab" data-toggle="tab" href="#pop3" role="tab" aria-controls="pop3" aria-selected="false">Pricing</a>
                    
                  </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                  <div class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">
                        <div class="pt-3"></div>
                        <form action="/action_page.php">
					  <div class="form-group">
						<label for="prd">Product Name</label>
						<input type="text" class="form-control" id="prd" value="Vases">
					  </div>
					  <div class="form-group">
						<label for="stu">Status</label>
						<input type="text" class="form-control" id="stu" value="Lorum Ipsum">
					  </div>
					  
					</form>
                      </div>
                  <div class="tab-pane fade" id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
                       <div class="pt-3"></div>
                        <div class="row">
						<div class="col-sm-4">
						<div class="product-image"><img src="assets/images/image-1.jpg" alt="image"></div>
						</div>
						<div class="col-sm-4">
						<div class="product-image"><img src="assets/images/image-1.jpg" alt="image"></div>
						</div>
						<div class="col-sm-4">
						<div class="product-image"><img src="assets/images/image-1.jpg" alt="image"></div>
						</div>
						<div class="col-sm-4">
						<div class="product-image"><img src="assets/images/image-1.jpg" alt="image"></div>
						</div>
						</div>
                      
                  </div>
                  <div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
                       <div class="pt-3"></div>
                        <form action="/action_page.php">
					  <div class="form-group">
						<label for="cst">Cost</label>
						<input type="text" class="form-control" id="cst" value="$15">
					  </div>
					  <div class="form-group">
						<label for="sell">Selling Price</label>
						<input type="text" class="form-control" id="sell" value="$20">
					  </div>

						<div class="form-group">
						<label for="sol">Total Solid</label>
						<input type="text" class="form-control" id="sol" value="$20">
					  </div>
					  
					</form>
                      
                  </div>
                  
                </div>
            </div>
        </div>
</div>

								<div class="grid-view">
								<div class="row">

								<div class="col-sm-4">
								<div class="grid-box">
								<div class="image"><img src="assets/images/image-1.jpg"></div>
								<div class="name">Succulent Terrariums</div>
								<div class="grid-desc">
								<div class="row">
								<div class="col-sm-6 py-2">
								COST - $15
								</div>
								<div class="col-sm-6 text-right py-2">
								SELLING PRICE - $15
								</div>
								<div class="col-sm-6 py-2">
								STATUS - Lorum Ipsum	
								</div>
								<div class="col-sm-6 text-right py-2">
								TOTAL SOLD -$20
								</div>
								<div class="col-sm-12 text-right py-2">
								<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
								</div>
								</div>

								</div>
								</div>
					
								</div>

								<div class="col-sm-4">
								<div class="grid-box">
								<div class="image"><img src="assets/images/image-3.jpg"></div>
								<div class="name">Succulent Terrariums</div>
								<div class="grid-desc">
								<div class="row">
								<div class="col-sm-6 py-2">
								COST - $15
								</div>
								<div class="col-sm-6 text-right py-2">
								SELLING PRICE - $15
								</div>
								<div class="col-sm-6 py-2">
								STATUS - Lorum Ipsum	
								</div>
								<div class="col-sm-6 text-right py-2">
								TOTAL SOLD -$20
								</div>
								<div class="col-sm-12 text-right py-2">
								<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
								</div>
								</div>

								</div>
								</div>
					
								</div>

								<div class="col-sm-4">
								<div class="grid-box">
								<div class="image"><img src="assets/images/image-4.jpg"></div>
								<div class="name">Succulent Terrariums</div>
								<div class="grid-desc">
								<div class="row">
								<div class="col-sm-6 py-2">
								COST - $15
								</div>
								<div class="col-sm-6 text-right py-2">
								SELLING PRICE - $15
								</div>
								<div class="col-sm-6 py-2">
								STATUS - Lorum Ipsum	
								</div>
								<div class="col-sm-6 text-right py-2">
								TOTAL SOLD -$20
								</div>
								<div class="col-sm-12 text-right py-2">
								<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
								</div>
								</div>

								</div>
								</div>
					
								</div>

								<div class="col-sm-4">
								<div class="grid-box">
								<div class="image"><img src="assets/images/product.jpg"></div>
								<div class="name">Succulent Terrariums</div>
								<div class="grid-desc">
								<div class="row">
								<div class="col-sm-6 py-2">
								COST - $15
								</div>
								<div class="col-sm-6 text-right py-2">
								SELLING PRICE - $15
								</div>
								<div class="col-sm-6 py-2">
								STATUS - Lorum Ipsum	
								</div>
								<div class="col-sm-6 text-right py-2">
								TOTAL SOLD -$20
								</div>
								<div class="col-sm-12 text-right py-2">
								<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
								</div>
								</div>

								</div>
								</div>
					
								</div>



								<div class="col-sm-4">
								<div class="grid-box">
								<div class="image"><img src="assets/images/image-1.jpg"></div>
								<div class="name">Succulent Terrariums</div>
								<div class="grid-desc">
								<div class="row">
								<div class="col-sm-6 py-2">
								COST - $15
								</div>
								<div class="col-sm-6 text-right py-2">
								SELLING PRICE - $15
								</div>
								<div class="col-sm-6 py-2">
								STATUS - Lorum Ipsum	
								</div>
								<div class="col-sm-6 text-right py-2">
								TOTAL SOLD -$20
								</div>
								<div class="col-sm-12 text-right py-2">
								<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
								</div>
								</div>

								</div>
								</div>
					
								</div>


								<div class="col-sm-4">
								<div class="grid-box">
								<div class="image"><img src="assets/images/image-3.jpg"></div>
								<div class="name">Succulent Terrariums</div>
								<div class="grid-desc">
								<div class="row">
								<div class="col-sm-6 py-2">
								COST - $15
								</div>
								<div class="col-sm-6 text-right py-2">
								SELLING PRICE - $15
								</div>
								<div class="col-sm-6 py-2">
								STATUS - Lorum Ipsum	
								</div>
								<div class="col-sm-6 text-right py-2">
								TOTAL SOLD -$20
								</div>
								<div class="col-sm-12 text-right py-2">
								<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
								</div>
								</div>

								</div>
								</div>
					
								</div>


								<div class="col-sm-4">
								<div class="grid-box">
								<div class="image"><img src="assets/images/image-4.jpg"></div>
								<div class="name">Succulent Terrariums</div>
								<div class="grid-desc">
								<div class="row">
								<div class="col-sm-6 py-2">
								COST - $15
								</div>
								<div class="col-sm-6 text-right py-2">
								SELLING PRICE - $15
								</div>
								<div class="col-sm-6 py-2">
								STATUS - Lorum Ipsum	
								</div>
								<div class="col-sm-6 text-right py-2">
								TOTAL SOLD -$20
								</div>
								<div class="col-sm-12 text-right py-2">
								<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
								</div>
								</div>

								</div>
								</div>
					
								</div>


								<div class="col-sm-4">
								<div class="grid-box">
								<div class="image"><img src="assets/images/product.jpg"></div>
								<div class="name">Succulent Terrariums</div>
								<div class="grid-desc">
								<div class="row">
								<div class="col-sm-6 py-2">
								COST - $15
								</div>
								<div class="col-sm-6 text-right py-2">
								SELLING PRICE - $15
								</div>
								<div class="col-sm-6 py-2">
								STATUS - Lorum Ipsum	
								</div>
								<div class="col-sm-6 text-right py-2">
								TOTAL SOLD -$20
								</div>
								<div class="col-sm-12 text-right py-2">
								<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
								</div>
								</div>

								</div>
								</div>
					
								</div>

								<div class="col-sm-4">
								<div class="grid-box">
								<div class="image"><img src="assets/images/image-1.jpg"></div>
								<div class="name">Succulent Terrariums</div>
								<div class="grid-desc">
								<div class="row">
								<div class="col-sm-6 py-2">
								COST - $15
								</div>
								<div class="col-sm-6 text-right py-2">
								SELLING PRICE - $15
								</div>
								<div class="col-sm-6 py-2">
								STATUS - Lorum Ipsum	
								</div>
								<div class="col-sm-6 text-right py-2">
								TOTAL SOLD -$20
								</div>
								<div class="col-sm-12 text-right py-2">
								<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
								</div>
								</div>

								</div>
								</div>
					
								</div>

								<div class="col-sm-4">
								<div class="grid-box">
								<div class="image"><img src="assets/images/image-3.jpg"></div>
								<div class="name">Succulent Terrariums</div>
								<div class="grid-desc">
								<div class="row">
								<div class="col-sm-6 py-2">
								COST - $15
								</div>
								<div class="col-sm-6 text-right py-2">
								SELLING PRICE - $15
								</div>
								<div class="col-sm-6 py-2">
								STATUS - Lorum Ipsum	
								</div>
								<div class="col-sm-6 text-right py-2">
								TOTAL SOLD -$20
								</div>
								<div class="col-sm-12 text-right py-2">
								<div class="edit">
											<ul>
											<li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
											</ul>
											</div>
								</div>
								</div>

								</div>
								</div>
					
								</div>
								</div>
								</div>
							   </div>
							  <!--<div class="proPagination">
								   <ul class="pagination pagination-outline-primary">
									  <li class="page-item"><a class="page-link" href="#">Previous</a></li>
									  <li class="page-item active"><a class="page-link" href="#">1</a></li>
									  <li class="page-item"><a class="page-link" href="#">2</a></li>
									  <li class="page-item"><a class="page-link" href="#">3</a></li>
									  <li class="page-item"><a class="page-link" href="#">Next</a></li>
									</ul>
								   </div>-->
                           </div>
                        </div>
                     </div>
                     <!---->
                  </div>
               </div>
            </div>
         </div>
         <!---->
      </div>

      <!---->
      <script src="assets/js/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="assets/js/bootstrap.min.js"></script>
<!--
      <script src="assets/js/jquery.dataTables.min.js"></script>
      <script src="assets/js/dataTables.bootstrap4.min.js"></script>
      <script src="assets/js/datatables-demo.js"></script>
-->
      <script type="text/javascript">
         $(document).ready(function(){
         $(".menu-link ul").hide();
         // $(".menu-link ul").find('li').hide();
         $('.menu-link').click(function() {
         $(this).children('ul').find('li').show();
         $(this).children('ul').slideToggle();
         });
         });
      </script>
		<script>
		$(document).ready(function(){
		  $(".gridView").click(function(){
			$('.grid-view').addClass("display-block");
			$('.table-responsive').addClass("display-none");
		  });
		$(".listingView").click(function(){
			$('.grid-view').removeClass("display-block");
			$('.table-responsive').removeClass("display-none");
		  });
		});
		</script>
   </body>
</html>