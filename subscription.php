<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
	   <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" /> 
	   <meta http-equiv="Pragma" content="no-cache" /> 
	   <meta http-equiv="Expires" content="0" />
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Subscription - Bloom Runner</title>
      <!--favicon-->
      <link rel="icon" type="image/x-icon" href="assets/images/favicon.png">
      <!--common css-->
      <link rel="stylesheet" href="assets/css/common.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/header.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/sidebar.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/fonts.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/floral.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/footer.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/materialdesignicons.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/tagger.css" type="text/css"/>
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic|Roboto+Mono:400,500|Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/select.dataTables.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css" type="text/css"/>
   </head>
   <body>
      <!---->
      <div class="pcoded-container navbar-wrapper">
      <?php include_once('common/header.php') ?>
      <!---->
      <div class="mainContainer">
      <?php include_once('common/sidebar.php') ?> 
      <!----> 
      <div class="pcodedContent">
         <div class="pcodedInnerContent">
            <div class="pageBody">
               <div class="breadcrumbInfo">
                  <div class="row">
                     <div class="col-sm-12">
						<div class="row mb-3">
							<div class="col-sm-12 col-md-10">
								<h4 class="page-title">Subscription</h4>
							</div>
							<div class="col-sm-12 col-md-2 text-right">
								<div class="headerCommon">
									 <div class="backBtn">
										<a class="btn btn-outline-primary btn-round waves-effect waves-light backActionBtn" href="#" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
									 </div>
								  </div>
							</div>
						</div>
                        <ol class="breadcrumb">
                           <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
                           <li class="breadcrumb-item active" aria-current="page">Subscription</li>
                        </ol>
                     </div>
                  </div>
               </div>
               <!---->
               <div class="commonTabbing">
                  <!--div class="headerCommon">
                     <div class="backBtn">
                        <a class="btn btn-outline-primary btn-round waves-effect waves-light" href="#" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                     </div>
                  </div-->
                  <div class="usersListing">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="card">
                              <div class="card-body">
                                 <div class="usersList">
                                    <div class="row">
                                       <!---->
                                       <div class="col-sm-12 col-md-6">
                                          <div class="searchFilter">
                                             <label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="dtBasicExample"></label>
                                          </div>
                                          <div class="advanceSearch">
                                             <div class="dropdown">
                                                <button class="btn btn-outline-primary btn-round waves-effect waves-light dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-filter"></i></button>
                                                <ul class="dropdown-menu">
                                                   <input class="form-control" id="myInput" type="text" placeholder="Search..">
                                                   <li><a href="#">Sort A -> Z</a></li>
                                                   <li><a href="#">Sort Z -> A</a></li>
                                                   <li><a href="#">Sort by Color</a></li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-sm-12 col-md-6">
                                          <div class="createUser">
                                             <a class="btn btn-icon add-product" href="#" title="Add User"><span><img src="assets/images/plus.png" alt="image"/></span></a>
                                          </div>
                                       </div>
                                       <!---->
                                       <div class="col-sm-12">
                                          <div class="tableGrid">
                                             <div class="table-responsive">
                                                <table id="dtBasicExample" class="table table-hover">
                                                   <thead>
                                                      <tr>
                                                         <th>Plan Name</th>
                                                         <th>Type </th>
                                                         <th>No. Of subscribers </th>
                                                         <th>Amount</th>
                                                         <th>Start Date</th>
                                                         <th>End Date</th>
                                                         <th>Action</th>
                                                   </thead>
                                                   <tbody>
                                                      <tr>
                                                         <td>Lorum Ipsum</td>
                                                         <td>Weekly</td>
                                                         <td>2</td>
                                                         <td>$67</td>
                                                         <td>June 30, 2020</td>
                                                         <td>July 7, 2020</td>
                                                         <td class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td>Lorum Ipsum</td>
                                                         <td>Weekly</td>
                                                         <td>2</td>
                                                         <td>$67</td>
                                                         <td>June 30, 2020</td>
                                                         <td>July 7, 2020</td>
                                                         <td class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td>Lorum Ipsum</td>
                                                         <td>Yearly</td>
                                                         <td>2</td>
                                                         <td>$67</td>
                                                         <td>June 30, 2020</td>
                                                         <td>July 7, 2020</td>
                                                         <td class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td>Lorum Ipsum</td>
                                                         <td>Weekly</td>
                                                         <td>2</td>
                                                         <td>$67</td>
                                                         <td>June 30, 2020</td>
                                                         <td>July 7, 2020</td>
                                                         <td class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td>Lorum Ipsum</td>
                                                         <td>Yearly</td>
                                                         <td>2</td>
                                                         <td>$67</td>
                                                         <td>June 30, 2020</td>
                                                         <td>July 7, 2020</td>
                                                         <td class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td>Lorum Ipsum</td>
                                                         <td>Weekly</td>
                                                         <td>2</td>
                                                         <td>$67</td>
                                                         <td>June 30, 2020</td>
                                                         <td>July 7, 2020</td>
                                                         <td class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td>Lorum Ipsum</td>
                                                         <td>Yearly</td>
                                                         <td>2</td>
                                                         <td>$67</td>
                                                         <td>June 30, 2020</td>
                                                         <td>July 7, 2020</td>
                                                         <td class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td>Lorum Ipsum</td>
                                                         <td>Yearly</td>
                                                         <td>2</td>
                                                         <td>$67</td>
                                                         <td>June 30, 2020</td>
                                                         <td>July 7, 2020</td>
                                                         <td class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td>Lorum Ipsum</td>
                                                         <td>Weekly</td>
                                                         <td>2</td>
                                                         <td>$67</td>
                                                         <td>June 30, 2020</td>
                                                         <td>July 7, 2020</td>
                                                         <td class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td>Lorum Ipsum</td>
                                                         <td>Weekly</td>
                                                         <td>2</td>
                                                         <td>$67</td>
                                                         <td>June 30, 2020</td>
                                                         <td>July 7, 2020</td>
                                                         <td class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                       </div>
                                       <!---->
                                       <div class="col-sm-12 col-md-6">
                                          <div class="showEntries">
                                             <label>
                                                Show 
                                                <select class="custom-select custom-select-sm form-control form-control-sm">
                                                   <option value="10">10</option>
                                                   <option value="25">25</option>
                                                   <option value="50">50</option>
                                                   <option value="100">100</option>
                                                </select>
                                                entries
                                             </label>
                                          </div>
                                       </div>
                                       <div class="col-sm-12 col-md-6">
                                          <div class="gridPag">
                                             <ul class="pagination">
                                                <li class="page-item disabled">
                                                   <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                </li>
                                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                <li class="page-item active">
                                                   <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                                </li>
                                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                <li class="page-item">
                                                   <a class="page-link" href="#">Next</a>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                       <!---->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="usersData">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="commonTabs">
                              <div class="card">
                                 <div class="card-body">
                                    <ul class="nav nav-tabs nav-tabs-primary">
                                       <li class="nav-item">
                                          <a class="nav-link active" data-toggle="tab" href="#tabe-1"><i class="icon-home"></i> <span class="hidden-xs">General</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-2"><i class="icon-user"></i> <span class="hidden-xs">Inventory</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-3"><i class="icon-user"></i> <span class="hidden-xs">Shipping</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-4"><i class="icon-user"></i> <span class="hidden-xs">Attributes</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-5"><i class="icon-user"></i> <span class="hidden-xs">Advanced</span></a>
                                       </li>
                                       </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                       <div id="tabe-1" class=" tab-pane active">
                                          <div class="row">
                                             <div class="col-md-4">
                                                <div class="form-group">
                                                   <label for="prd">Subscription Price</label>
                                                   <input type="text" class="form-control" id="prd" value="">
                                                </div>
                                             </div>
                                             <div class="col-md-4">
                                                <div class="form-group">
													 <label for="prd" style="visibility: hidden;">Subscription Price</label>
                                                   <select class="form-control" id="stu">
                                                      <option>Every </option>
                                                      <option>Every 2nd</option>
                                                      <option>Every 3rd</option>
                                                      <option>Every 4th</option>
                                                      <option>Every 5th</option>
                                                      <option>Every 6th</option>
                                                      <option>Every 7th</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-md-4">
                                                <div class="form-group">
													 <label for="prd" style="visibility: hidden;">Subscription Price</label>
                                                   <select class="form-control" id="stu">
                                                      <option>Month</option>
                                                      <option>Year</option>
                                                      <option>Week</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="prd">Expired After</label>
                                                   <select class="form-control" id="stu">
                                                      <option>12 Months</option>
                                                      <option>1 Month</option>
                                                      <option>1 Week</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="prd">Sign Up Fee($)</label>
                                                   <input type="text" class="form-control" id="prd" value="">
                                                </div>
                                             </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="prd">Free Trail</label>
                                                   <input type="number" class="form-control" id="prd" value="">
                                                </div>
                                             </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
													 <label for="prd" style="visibility: hidden;">Subscription Price</label>
                                                   <select class="form-control" id="stu">
                                                      <option> Month</option>
                                                      <option>day</option>
                                                      <option>Week</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-md-12">
                                                <div class="form-group">
                                                   <label for="prd">Sale Price($)</label>
                                                   <input type="text" class="form-control" id="prd" value="">
                                                </div>
                                             </div>
											  <div class="col-sm-12">
                                                <hr>
                                             </div>
											  <div class="col-sm-12">
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="tabe-2" class=" tab-pane fade">
                                          <div class="row">
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="prd">Available Quantity</label>
                                                   <input type="date" class="form-control" id="prd" value="">
                                                </div>
                                             </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="stu">Allow Backorders?</label>
                                                   <select class="form-control" id="stu">
                                                      <option>Allow</option>
                                                      <option>Not allow</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="stu">Low Stock threshold</label>
                                                   <input type="number" class="form-control" id="prd" value="">
                                                </div>
                                             </div>
											  	  <div class="col-sm-12">
                                                <hr>
                                             </div>
											  <div class="col-sm-12">
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="tabe-3" class=" tab-pane fade">
                                          <div class="row">
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="prd">Weight</label>
                                                   <input type="number" class="form-control" id="prd" value="">
                                                </div>
                                             </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="stu">Shipping Class</label>
                                                   <select class="form-control" id="stu">
                                                      <option>Class 1</option>
                                                      <option>Class 2</option>
                                                      <option>Class 3</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-md-6">
                                                <div class="custom-control custom-checkbox">
                                                   <input type="checkbox" class="custom-control-input" id="customCheck1" checked="">
                                                   <label class="custom-control-label" for="customCheck1">One time Shipping</label>
                                                </div>
                                             </div>
											  	  <div class="col-sm-12">
                                                <hr>
                                             </div>
											  <div class="col-sm-12">
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="tabe-4" class=" tab-pane fade">
                                          <div class="row">
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="prd">Maximum number of Categories subscriptions</label>
                                                   <input type="Number" class="form-control" id="prd" value="">
                                                </div>
                                             </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="prd">Maximum number of Discussion subscriptions</label>
                                                   <input type="Number" class="form-control" id="prd" value="">
                                                </div>
                                             </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="prd">Maximum number of Saved searches</label>
                                                   <input type="Number" class="form-control" id="prd" value="">
                                                </div>
                                             </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="stu">Conflict Resolution Leve</label>
                                                   <select class="form-control" id="stu">
                                                      <option>Highest</option>
                                                      <option>Medium</option>
                                                      <option>Low</option>
                                                   </select>
                                                </div>
                                             </div>
											  	  <div class="col-sm-12">
                                                <hr>
                                             </div>
											  <div class="col-sm-12">
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="tabe-5" class=" tab-pane fade">
                                          <div class="row">
                                             <div class="col-md-12">
                                                <div class="form-group">
                                                   <label for="prd">Purchase Note</label>
                                                   <textarea type="number" class="form-control" id="prd" value=""></textarea>
                                                </div>
                                             </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="prd">Menu Order</label>
                                                   <input type="number" class="form-control" id="prd" value="">
                                                </div>
                                             </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="stu">Limit Subscription</label>
                                                   <select class="form-control" id="stu">
                                                      <option>Do not Limit</option>
                                                      <option>Limit to one active Subscription</option>
                                                      <option>Limit to one any status</option>
                                                   </select>
                                                </div>
                                             </div>
											  	  <div class="col-sm-12">
                                                <hr>
                                             </div>
											  <div class="col-sm-12">
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!---->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!---->
            </div>
         </div>
         <!---->
      </div>
      <!--notes modalsss-->
      <div id="myModalNotes" class="modal fade" role="dialog">
         <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title"> Notes</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <div class="shopTables">
                     <div class="row">
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label>Note Date</label> 
                              <input type="date" class="form-control"/> 
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label>Note Priority</label> 
                              <select class="form-control">
                                 <option value="0">Select Priority </option>
                                 <option value="0">High </option>
                                 <option value="0">Medium </option>
                                 <option value="0">Low </option>
                              </select>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label>Follow Up Needed</label> 
                              <div class="radioBtn">
                                 <label class="radioBox"><input type="radio" id="success1" name="successname" checked=""><span class="primary"></span>
                                 </label>
                                 <span class="checkActive">Yes</span>
                              </div>
                              <div class="radioBtn">
                                 <label class="radioBox"><input type="radio" id="success2" name="successname"><span class="primary"></span>
                                 </label>	
                                 <span class="checkActive">No</span>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label>Follow Up Date</label> 
                              <input type="date" class="form-control"/> 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss ">Save</button> 
               </div>
            </div>
         </div>
      </div>
      <!---->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
      <script src="assets/js/custom.js"></script>
      <script>
         $(document).ready(function() {
         $('#example').DataTable( {
         columnDefs: [ {
         orderable: false,
         className: 'select-checkbox',
         targets:   0
         } ],
         select: {
         style:    'os',
         selector: 'td:first-child'
         },
         order: [[ 1, 'asc' ]]
         } );
         $('#example1').DataTable( {
         columnDefs: [ {
         orderable: false,
         className: 'select-checkbox',
         targets:   0
         } ],
         select: {
         style:    'os',
         selector: 'td:first-child'
         },
         order: [[ 1, 'asc' ]]
         } );
         } ); 
      </script>
      <script>
         $(".add-product").click(function(){
                	$('.usersData').addClass("show");
                	$('.usersListing').addClass("hide");
             $('.headerCommon').addClass("show");
             $('.shortcut').addClass("hide");
                  });  
         $(".backBtn").click(function(){
                	$('.usersData').removeClass("show");
                	$('.usersListing').removeClass("hide");
             $('.headerCommon').removeClass("show");
             $('.shortcut').removeClass("hide");
                  });  
          
      </script>
      <script>
         $(document).ready(function(){
         $('.barIcon').click(function() { 
         $('.br-sideleft').toggleClass('show')
         $('.pcodedContent').toggleClass('show')
             });  
         });  
      </script>
      <!---->
   </body>
</html>