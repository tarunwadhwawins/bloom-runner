<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
      <meta http-equiv="Pragma" content="no-cache" />
      <meta http-equiv="Expires" content="0" />
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>User Management- Bloom Runner</title>
      <!--favicon-->
      <link rel="icon" type="image/x-icon" href="assets/images/favicon.png">
      <!--common css-->
      <link rel="stylesheet" href="assets/css/common.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/header.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/sidebar.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/fonts.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/floral.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/footer.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/materialdesignicons.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/tagger.css" type="text/css"/>
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic|Roboto+Mono:400,500|Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/select.dataTables.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css" type="text/css"/>
      <!--	  <link href="assets/css/steppers.css" rel="stylesheet">-->
      <link href="assets/css/steppers.css" rel="stylesheet">
   </head>
   <body>
      <!---->
      <div class="pcoded-container navbar-wrapper">
      <?php include_once('common/header.php') ?>
      <!---->
      <div class="mainContainer">
      <?php include_once('common/sidebar.php') ?> 
      <!----> 
      <div class="pcodedContent">
         <div class="pcodedInnerContent">
            <div class="pageBody">
               <div class="breadcrumbInfo">
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="row mb-3">
                           <div class="col-sm-12 col-md-10">
                              <h4 class="page-title">User Management</h4>
                           </div>
                           <div class="col-sm-12 col-md-2 text-right">
                              <div class="headerCommon">
                                 <div class="backBtn">
                                    <a class="btn btn-outline-primary btn-round waves-effect waves-light backActionBtn" href="#" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <ol class="breadcrumb">
                           <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
                           <li class="breadcrumb-item active" aria-current="page">User Management</li>
                        </ol>
                     </div>
                  </div>
               </div>
               <!---->
               <div class="commonTabbing">
                  <div class="usersListing">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="card">
                              <div class="card-body">
                                 <div class="usersList">
                                    <div class="row">
                                       <!---->
                                       <div class="col-sm-12 col-md-12">
                                          <div class="searchFilter">
                                             <label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="dtBasicExample"></label>
                                          </div>
                                          <div class="advanceSearch">
                                             <div class="dropdown">
                                                <button class="btn btn-outline-primary btn-round waves-effect waves-light dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-filter"></i></button>
                                                <ul class="dropdown-menu">
                                                   <input class="form-control" id="myInput" type="text" placeholder="Search..">
                                                   <li><a href="#">Sort A -> Z</a></li>
                                                   <li><a href="#">Sort Z -> A</a></li>
                                                   <li><a href="#">Sort by Color</a></li>
                                                </ul>
                                             </div>
                                          </div>
                                          <div class="createUser">
                                             <a class="btn btn-icon add-product" href="#" title="Add User"><span><img src="assets/images/plus.png" alt="image"/></span></a>
                                          </div>
                                       </div>
                                       <!---->
                                       <div class="col-sm-12">
                                          <div class="tableGrid">
                                             <div class="table-responsive">
                                                <table id="dtBasicExample" class="table table-hover">
                                                   <thead>
                                                      <tr>
                                                         <th>User Name</th>
                                                         <th>Email</th>
                                                         <th>First Name</th>
                                                         <th>Last Name</th>
                                                         <th>Created At </th>
                                                         <th>Active Status</th>
                                                         <th>Actions</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      <tr>
                                                         <td  data-label="User Name">Admin</td>
                                                         <td  data-label="Email">admin@admin.com</td>
                                                         <td  data-label="First Name">Tarun</td>
                                                         <td  data-label="Last Name">Wadhwa</td>
                                                         <td  data-label="Created At">June 22,2020</td>
                                                         <td  data-label="Active Status"><span class="badge-dot"><i class="bg-danger"></i> pending</span></td>
                                                         <td data-label="Actions" class="td-actions text-right">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td  data-label="User Name">Ria Sahni</td>
                                                         <td data-label="Email">ria@admin.com</td>
                                                         <td data-label="First Name">Ria</td>
                                                         <td data-label="Last Name">Sahni</td>
                                                         <td data-label="Created At">June 25,2020</td>
                                                         <td data-label="Active Status"><span class="badge-dot"><i class="bg-success"></i> Completed</span></td>
                                                         <td  data-label="Actions" class="td-actions text-right">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit" class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td data-label="User Name">Deepak Sharma</td>
                                                         <td data-label="Email">deepak@admin.com</td>
                                                         <td data-label="First Name">Deepak</td>
                                                         <td data-label="Last Name">Sharma</td>
                                                         <td data-label="Created At">June 26,2020</td>
                                                         <td data-label="Active Status"><span class="badge-dot"><i class="bg-warning"></i> Delayed </span> </td>
                                                         <td data-label="Actions" class="td-actions text-right">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button"title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td data-label="User Name">Nidhi Thakur</td>
                                                         <td data-label="Email">nidhi@admin.com</td>
                                                         <td data-label="First Name">Nidhi</td>
                                                         <td data-label="Last Name">Thakur</td>
                                                         <td data-label="Created At">June 28,2020</td>
                                                         <td data-label="Active Status"><span class="badge-dot"><i class="bg-warning"></i> Delayed </span> </td>
                                                         <td data-label="Actions" class="td-actions text-right">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td data-label="User Name">Princy Dhupar</td>
                                                         <td data-label="Email">princy@admin.com</td>
                                                         <td data-label="First Name">Princy</td>
                                                         <td data-label="Last Name">Dhupar</td>
                                                         <td data-label="Created At">June 30,2020</td>
                                                         <td data-label="Active Status"><span class="badge-dot"><i class="bg-danger"></i> pending</span></td>
                                                         <td data-label="Actions" class="td-actions text-right">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit" class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td data-label="User Name">Neeraj Saini</td>
                                                         <td data-label="Email">neeraj@admin.com</td>
                                                         <td data-label="First Name">Neeraj</td>
                                                         <td data-label="Last Name">Saini</td>
                                                         <td data-label="Created At">July 05,2020</td>
                                                         <td data-label="Active Status"><span class="badge-dot"><i class="bg-success"></i> Completed</span></td>
                                                         <td data-label="Actions" class="td-actions text-right">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td data-label="User Name">Nidhi Thakur</td>
                                                         <td data-label="Email">nidhi@admin.com</td>
                                                         <td data-label="First Name">Nidhi</td>
                                                         <td data-label="Last Name">Thakur</td>
                                                         <td data-label="Created At">June 28,2020</td>
                                                         <td data-label="Active Status"><span class="badge-dot"><i class="bg-warning"></i> Delayed </span> </td>
                                                         <td data-label="Actions" class="td-actions text-right">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td data-label="User Name">Princy Dhupar</td>
                                                         <td data-label="Email">princy@admin.com</td>
                                                         <td data-label="First Name">Princy</td>
                                                         <td data-label="Last Name">Dhupar</td>
                                                         <td data-label="Created At">June 30,2020</td>
                                                         <td data-label="Active Status"><span class="badge-dot"><i class="bg-danger"></i> pending</span></td>
                                                         <td data-label="Actions" class="td-actions text-right">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit" class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td data-label="User Name">Neeraj Saini</td>
                                                         <td data-label="Email">neeraj@admin.com</td>
                                                         <td data-label="First Name">Neeraj</td>
                                                         <td data-label="Last Name">Saini</td>
                                                         <td data-label="Created At">July 05,2020</td>
                                                         <td data-label="Active Status"><span class="badge-dot"><i class="bg-success"></i> Completed</span></td>
                                                         <td data-label="Actions" class="td-actions text-right">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td data-label="User Name">Nidhi Thakur</td>
                                                         <td data-label="Email">nidhi@admin.com</td>
                                                         <td data-label="First Name">Nidhi</td>
                                                         <td data-label="Last Name">Thakur</td>
                                                         <td data-label="Created At">June 28,2020</td>
                                                         <td data-label="Active Status"><span class="badge-dot"><i class="bg-warning"></i> Delayed </span> </td>
                                                         <td data-label="Actions" class="td-actions text-right">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                            </button><button type="button" title="Delete"  class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                            </button>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                       </div>
                                       <!---->
                                       <div class="col-sm-12 col-md-6">
                                          <div class="showEntries">
                                             <label>
                                                Show 
                                                <select class="custom-select custom-select-sm form-control form-control-sm">
                                                   <option value="10">10</option>
                                                   <option value="25">25</option>
                                                   <option value="50">50</option>
                                                   <option value="100">100</option>
                                                </select>
                                                entries
                                             </label>
                                          </div>
                                       </div>
                                       <div class="col-sm-12 col-md-6">
                                          <div class="gridPag">
                                             <ul class="pagination">
                                                <li class="page-item disabled">
                                                   <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                </li>
                                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                <li class="page-item active">
                                                   <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                                </li>
                                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                <li class="page-item">
                                                   <a class="page-link" href="#">Next</a>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                       <!---->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="usersData stepperWrapper">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="commonTabs">
                              <div class="card">
                                 <div class="card-body">
                                    <!---->
                                    <div class="stepperFormSection">
                                       <ul class="stepper horizontal" id="horizontal-stepper">
                                          <li class="step active">
                                             <div class="step-title waves-effect waves-dark">Add</div>
                                             <div class="step-new-content">
                                                <div class="row">
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>Owner</label>
                                                         <select class="form-control">
                                                            <option value="0" selected> Please select Owner</option>
                                                            <option value="1">Main</option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>Status</label>
                                                         <select class="form-control">
                                                            <option value="0" selected> Please select Status</option>
                                                            <option value="1">Active</option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>Username</label>
                                                         <input type="text" class="form-control"> 
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>Email</label>
                                                         <input type="text" class="form-control"> 
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>Password</label>
                                                         <input type="text" class="form-control"> 
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>Re-Enter Password</label>
                                                         <input type="text" class="form-control"> 
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>Name Prefix</label>
                                                         <input type="text" class="form-control"> 
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>First Name</label>
                                                         <input type="text" class="form-control"> 
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>Middle Name</label>
                                                         <input type="text" class="form-control"> 
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>Last Name</label>
                                                         <input type="text" class="form-control"> 
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>Name Suffix</label>
                                                         <input type="text" class="form-control"> 
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>Birthday</label>
                                                         <input type="date" class="form-control"> 
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-12">
                                                      <hr>
                                                   </div>
                                                </div>
                                                <div class="step-actions">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                   <button class="waves-effect waves-dark btn btn-sm btn-primary next-step" data-feedback="someFunction21">Next</button>
                                                </div>
                                             </div>
                                          </li>
                                          <li class="step">
                                             <div class="step-title waves-effect waves-dark">Roles</div>
                                             <div class="step-new-content">
                                                <div class="addBtn">
                                                   <button type="button" data-toggle="modal" data-target="#myModalRoles"><span><img src="assets/images/plus.png" alt="image"/></span></button>
                                                </div>
                                                <div class="row">
                                                   <div class="col-sm-12">
                                                      <div class="tableGrid">
                                                         <div class="table-responsive">
                                                            <table class="table table-hover">
                                                               <thead>
                                                                  <tr>
                                                                     <th>Roles</th>
                                                                     <th>Actions</th>
                                                                  </tr>
                                                               </thead>
                                                               <tbody>
                                                                  <tr>
                                                                     <td data-label="Roles">Admin</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Roles">Super Admin</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Roles">Manager</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Roles">Admin</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Roles">Super Admin</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Roles">Manager</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Roles">Admin</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Roles">Super Admin</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Roles">Manager</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Roles">Super Admin</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-12 col-md-6">
                                                      <div class="showEntries">
                                                         <label>
                                                            Show 
                                                            <select class="custom-select custom-select-sm form-control form-control-sm">
                                                               <option value="10">10</option>
                                                               <option value="25">25</option>
                                                               <option value="50">50</option>
                                                               <option value="100">100</option>
                                                            </select>
                                                            entries
                                                         </label>
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-12 col-md-6">
                                                      <div class="gridPag">
                                                         <ul class="pagination">
                                                            <li class="page-item disabled">
                                                               <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                            </li>
                                                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                            <li class="page-item active">
                                                               <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                                            </li>
                                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                            <li class="page-item">
                                                               <a class="page-link" href="#">Next</a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="step-actions">
                                                   <button class="waves-effect waves-dark btn btn-sm btn-secondary previous-step">BACK</button>
                                                   <button class="waves-effect waves-dark btn btn-sm btn-primary next-step" data-feedback="someFunction21">Next</button>										
                                                </div>
                                             </div>
                                          </li>
                                          <li class="step">
                                             <div class="step-title waves-effect waves-dark">Notes</div>
                                             <div class="step-new-content">
                                                <!---->
                                                <div class="addBtn">
                                                   <button type="button" data-toggle="modal" data-target="#myModalNotes"><span><img src="assets/images/plus.png" alt="image"/></span></button>
                                                </div>
                                                <div class="row">
                                                   <div class="col-sm-12">
                                                      <div class="tableGrid">
                                                         <div class="table-responsive">
                                                            <table class="table table-hover">
                                                               <thead>
                                                                  <tr>
                                                                     <th>Note Date</th>
                                                                     <th>Note Priority</th>
                                                                     <th>Follow up Needed</th>
                                                                     <th>Follow Up Date</th>
                                                                     <th>Actions</th>
                                                                  </tr>
                                                               </thead>
                                                               <tbody>
                                                                  <tr>
                                                                     <td data-label="Note Date">06/24/2020</td>
                                                                     <td data-label="Note Priority">High</td>
                                                                     <td data-label="Follow up Neede">Yes</td>
                                                                     <td data-label="Follow Up Date">06/25/2020</td>
                                                                     <td data-label="Actions" class="td-actions"> <button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Note Date">06/24/2020</td>
                                                                     <td data-label="Note Priority">Medium</td>
                                                                     <td data-label="Follow up Neede">Yes</td>
                                                                     <td data-label="Follow Up Date">06/25/2020</td>
                                                                     <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Note Date">06/24/2020</td>
                                                                     <td data-label="Note Priority">Low</td>
                                                                     <td data-label="Follow up Neede">No</td>
                                                                     <td data-label="Follow Up Date">06/25/2020</td>
                                                                     <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Note Date">06/24/2020</td>
                                                                     <td data-label="Note Priority">High</td>
                                                                     <td data-label="Follow up Neede">Yes</td>
                                                                     <td data-label="Follow Up Date">06/25/2020</td>
                                                                     <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Note Date">06/24/2020</td>
                                                                     <td data-label="Note Priority">High</td>
                                                                     <td data-label="Follow up Neede"> Yes</td>
                                                                     <td data-label="Follow Up Date">06/25/2020</td>
                                                                     <td data-label="Actions"  class="td-actions"> <button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Note Date">06/24/2020</td>
                                                                     <td data-label="Note Priority">Medium</td>
                                                                     <td data-label="Follow up Neede">Yes</td>
                                                                     <td data-label="Follow Up Date">06/25/2020</td>
                                                                     <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Note Date">06/24/2020</td>
                                                                     <td data-label="Note Priority">Low</td>
                                                                     <td data-label="Follow up Neede">No</td>
                                                                     <td data-label="Follow Up Date">06/25/2020</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Note Date">06/24/2020</td>
                                                                     <td data-label="Note Priority">High</td>
                                                                     <td data-label="Follow up Neede">Yes</td>
                                                                     <td data-label="Follow Up Date">06/25/2020</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Note Date">06/24/2020</td>
                                                                     <td data-label="Note Priority">Low</td>
                                                                     <td data-label="Follow up Neede">No</td>
                                                                     <td data-label="Follow Up Date">06/25/2020</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Note Date">06/24/2020</td>
                                                                     <td data-label="Note Priority">High</td>
                                                                     <td data-label="Follow up Neede">Yes</td>
                                                                     <td data-label="Follow Up Date">06/25/2020</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-12 col-md-6">
                                                      <div class="showEntries">
                                                         <label>
                                                            Show 
                                                            <select class="custom-select custom-select-sm form-control form-control-sm">
                                                               <option value="10">10</option>
                                                               <option value="25">25</option>
                                                               <option value="50">50</option>
                                                               <option value="100">100</option>
                                                            </select>
                                                            entries
                                                         </label>
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-12 col-md-6">
                                                      <div class="gridPag">
                                                         <ul class="pagination">
                                                            <li class="page-item disabled">
                                                               <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                            </li>
                                                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                            <li class="page-item active">
                                                               <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                                            </li>
                                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                            <li class="page-item">
                                                               <a class="page-link" href="#">Next</a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="step-actions">
                                                   <button class="waves-effect waves-dark btn btn-sm btn-secondary previous-step">BACK</button>
                                                   <button class="waves-effect waves-dark btn btn-sm btn-primary next-step" data-feedback="someFunction21">Next</button>										
                                                </div>
                                                <!---->
                                             </div>
                                          </li>
                                          <li class="step">
                                             <div class="step-title waves-effect waves-dark">Tags</div>
                                             <div class="step-new-content">
                                                <!---->
                                                <div class="row">
                                                   <div class="col-sm-12">
                                                      <div class="form-group">
                                                         <label for="shtdesc">Tags</label>
                                                         <input type="text" value="Lorem Ipsum, Lorem Ipsum, Lorem Ipsum" name="tags3" />
                                                      </div>
                                                   </div>
                                                </div>
												 <div class="step-actions">
                                                   <button class="waves-effect waves-dark btn btn-sm btn-secondary previous-step">BACK</button>
                                                   <button class="waves-effect waves-dark btn btn-sm btn-primary next-step" data-feedback="someFunction21">Next</button>										
                                                </div>
                                                <!---->
                                             </div>
                                          </li>
                                          <li class="step">
                                             <div class="step-title waves-effect waves-dark">Activities</div>
                                             <div class="step-new-content">
                                                <!---->
                                                <div class="row">
                                                   <div class="col-sm-12">
                                                      <div class="tableGrid">
                                                         <div class="table-responsive">
                                                            <table class="table table-hover">
                                                               <thead>
                                                                  <tr>
                                                                     <th>ID</th>
                                                                     <th>Order Name</th>
                                                                     <th>Order Type</th>
                                                                     <th>Order Date</th>
                                                                     <th>Actions</th>
                                                                  </tr>
                                                               </thead>
                                                               <tbody>
                                                                  <tr>
                                                                     <td data-label="ID" >1</td>
                                                                     <td data-label="Order Name" >Lorem Ipsum</td>
                                                                     <td data-label="Order Type" >Lorem Ipsum</td>
                                                                     <td data-label="Order Date" >06/24/2020</td>
                                                                     <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="ID" >2</td>
                                                                     <td data-label="Order Name" >Lorem Ipsum</td>
                                                                     <td data-label="Order Type" >Lorem Ipsum</td>
                                                                     <td data-label="Order Date" >06/28/2020</td>
                                                                     <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="ID"  >3</td>
                                                                     <td data-label="Order Name" >Lorem Ipsum</td>
                                                                     <td data-label="Order Type" >Lorem Ipsum</td>
                                                                     <td data-label="Order Date" >07/02/2020</td>
                                                                     <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="ID" >4</td>
                                                                     <td data-label="Order Name" >Lorem Ipsum</td>
                                                                     <td data-label="Order Type" >Lorem Ipsum</td>
                                                                     <td data-label="Order Date" >07/10/2020</td>
                                                                     <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="ID" >5</td>
                                                                     <td data-label="Order Name" >Lorem Ipsum</td>
                                                                     <td data-label="Order Type" >Lorem Ipsum</td>
                                                                     <td data-label="Order Date" >07/15/2020</td>
                                                                     <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="ID" >6</td>
                                                                     <td data-label="Order Name" >Lorem Ipsum</td>
                                                                     <td data-label="Order Type" >Lorem Ipsum</td>
                                                                     <td data-label="Order Date" >06/24/2020</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="ID" >7</td>
                                                                     <td data-label="Order Name" >Lorem Ipsum</td>
                                                                     <td data-label="Order Type" >Lorem Ipsum</td>
                                                                     <td data-label="Order Date" >06/28/2020</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="ID" >8</td>
                                                                     <td data-label="Order Name" >Lorem Ipsum</td>
                                                                     <td data-label="Order Type" >Lorem Ipsum</td>
                                                                     <td data-label="Order Date" >07/02/2020</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="ID" >9</td>
                                                                     <td data-label="Order Name" >Lorem Ipsum</td>
                                                                     <td data-label="Order Type" >Lorem Ipsum</td>
                                                                     <td data-label="Order Date" >07/10/2020</td>
                                                                     <td data-label="Actions" class="td-actions text-right"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="ID" >10</td>
                                                                     <td data-label="Order Name" >Lorem Ipsum</td>
                                                                     <td data-label="Order Type" >Lorem Ipsum</td>
                                                                     <td data-label="Order Date" >07/15/2020</td>
                                                                     <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                        </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-12 col-md-6">
                                                      <div class="showEntries">
                                                         <label>
                                                            Show 
                                                            <select class="custom-select custom-select-sm form-control form-control-sm">
                                                               <option value="10">10</option>
                                                               <option value="25">25</option>
                                                               <option value="50">50</option>
                                                               <option value="100">100</option>
                                                            </select>
                                                            entries
                                                         </label>
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-12 col-md-6">
                                                      <div class="gridPag">
                                                         <ul class="pagination">
                                                            <li class="page-item disabled">
                                                               <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                            </li>
                                                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                            <li class="page-item active">
                                                               <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                                            </li>
                                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                            <li class="page-item">
                                                               <a class="page-link" href="#">Next</a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
												 <div class="step-actions">
                                                   <button class="waves-effect waves-dark btn btn-sm btn-secondary previous-step">BACK</button>
                                                   <button class="waves-effect waves-dark btn btn-sm btn-primary next-step" data-feedback="someFunction21">Next</button>										
                                                </div>
                                                <!---->
                                             </div>
                                          </li>
                                          <li class="step">
                                             <div class="step-title waves-effect waves-dark">Locations</div>
                                             <div class="step-new-content">
                                                <!---->
                                                <div class="addBtn">
                                                   <button type="button" data-toggle="modal" data-target="#myModalLocation"><span><img src="assets/images/plus.png" alt="image"/></span></button>
                                                </div>
                                                <div class="row">
                                                   <div class="col-sm-12">
                                                      <div class="tableGrid">
                                                         <div class="table-responsive">
                                                            <table class="table table-hover">
                                                               <thead>
                                                                  <tr>
                                                                     <th>Location Name</th>
                                                                     <th>Actions</th>
                                                                  </tr>
                                                               </thead>
                                                               <tbody>
                                                                  <tr>
                                                                     <td data-label="Location Name">Location 1</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td  data-label="Location Name">Location 2</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td  data-label="Location Name">Location 3</td>
                                                                     <td class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td  data-label="Location Name">Location 1</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td  data-label="Location Name">Location 1</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td  data-label="Location Name">Location 1</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td data-label="Location Name">Location 2</td>
                                                                     <td class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td  data-label="Location Name">Location 3</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td  data-label="Location Name">Location 1</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td  data-label="Location Name">Location 1</td>
                                                                     <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                        </button>
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-12 col-md-6">
                                                      <div class="showEntries">
                                                         <label>
                                                            Show 
                                                            <select class="custom-select custom-select-sm form-control form-control-sm">
                                                               <option value="10">10</option>
                                                               <option value="25">25</option>
                                                               <option value="50">50</option>
                                                               <option value="100">100</option>
                                                            </select>
                                                            entries
                                                         </label>
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-12 col-md-6">
                                                      <div class="gridPag">
                                                         <ul class="pagination">
                                                            <li class="page-item disabled">
                                                               <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                            </li>
                                                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                            <li class="page-item active">
                                                               <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                                            </li>
                                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                            <li class="page-item">
                                                               <a class="page-link" href="#">Next</a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
												<div class="step-actions">
                                                   <button class="waves-effect waves-dark btn btn-sm btn-secondary previous-step">BACK</button>
                                                   <button class="waves-effect waves-dark btn btn-sm btn-primary next-step" data-feedback="someFunction21">Next</button>										
                                                </div>
                                                <!---->
                                             </div>
                                          </li>
                                          <li class="step">
                                             <div class="step-title waves-effect waves-dark">Settings</div>
                                             <div class="step-new-content">
                                                <!---->
                                                <div class="row">
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>Default Grid Page Size</label> 
                                                         <select class="form-control">
                                                            <option value="0">Select Grid Page </option>
                                                            <option value="0">10 </option>
                                                            <option value="0">20 </option>
                                                            <option value="0">30 </option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>Default Location</label> 
                                                         <select class="form-control">
                                                            <option value="0">Select Default Location </option>
                                                            <option value="0">Location 1</option>
                                                            <option value="0">Location 2 </option>
                                                            <option value="0">Location 3 </option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label>Home Screen</label> 
                                                         <select class="form-control">
                                                            <option value="0">Select Home Screen</option>
                                                            <option value="0">Dashboard </option>
                                                            <option value="0">New Order </option>
                                                            <option value="0">Customer List </option>
                                                            <option value="0">Product List </option>
                                                            <option value="0">Users List </option>
                                                         </select>
                                                      </div>
                                                   </div>
  
                                                </div>
												 <div class="step-actions">
                                                   <button class="waves-effect waves-dark btn btn-sm btn-secondary previous-step">BACK</button>
                                                   <button class="waves-effect waves-dark btn btn-sm btn-primary next-step" data-feedback="someFunction21">Save</button>										
                                                </div>
                                                <!---->
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                    <!---->
                                    <ul class="nav nav-tabs nav-tabs-primary" style="display: none;">
                                       <li class="nav-item">
                                          <a class="nav-link active" data-toggle="tab" href="#tabe-1"><i class="icon-home"></i> <span class="hidden-xs">Add</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-2"><i class="icon-user"></i> <span class="hidden-xs">Roles</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-3"><i class="icon-user"></i> <span class="hidden-xs">Notes</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-4"><i class="icon-user"></i> <span class="hidden-xs">Tags</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-5"><i class="icon-user"></i> <span class="hidden-xs">Activities</span></a>
                                       </li>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-6"><i class="icon-user"></i> <span class="hidden-xs">Locations</span></a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#tabe-7"><i class="icon-user"></i> <span class="hidden-xs">Settings</span></a> 
                                       </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content" style="display:none;">
                                       <div id="tabe-1" class=" tab-pane active">
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Owner</label>
                                                   <select class="form-control">
                                                      <option value="0" selected> Please select Owner</option>
                                                      <option value="1">Main</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Status</label>
                                                   <select class="form-control">
                                                      <option value="0" selected> Please select Status</option>
                                                      <option value="1">Active</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Username</label>
                                                   <input type="text" class="form-control"> 
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Email</label>
                                                   <input type="text" class="form-control"> 
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Password</label>
                                                   <input type="text" class="form-control"> 
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Re-Enter Password</label>
                                                   <input type="text" class="form-control"> 
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Name Prefix</label>
                                                   <input type="text" class="form-control"> 
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>First Name</label>
                                                   <input type="text" class="form-control"> 
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Middle Name</label>
                                                   <input type="text" class="form-control"> 
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Last Name</label>
                                                   <input type="text" class="form-control"> 
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Name Suffix</label>
                                                   <input type="text" class="form-control"> 
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Birthday</label>
                                                   <input type="date" class="form-control"> 
                                                </div>
                                             </div>
                                             <div class="col-sm-12">
                                                <hr>
                                             </div>
                                             <div class="col-sm-12">
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="tabe-2" class=" tab-pane fade">
                                          <div class="addBtn">
                                             <button type="button" data-toggle="modal" data-target="#myModalRoles"><span><img src="assets/images/plus.png" alt="image"/></span></button>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-12">
                                                <div class="tableGrid">
                                                   <div class="table-responsive">
                                                      <table class="table table-hover">
                                                         <thead>
                                                            <tr>
                                                               <th>Roles</th>
                                                               <th>Actions</th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <tr>
                                                               <td data-label="Roles">Admin</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Roles">Super Admin</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Roles">Manager</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Roles">Admin</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Roles">Super Admin</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Roles">Manager</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Roles">Admin</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Roles">Super Admin</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Roles">Manager</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Roles">Super Admin</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-sm-12 col-md-6">
                                                <div class="showEntries">
                                                   <label>
                                                      Show 
                                                      <select class="custom-select custom-select-sm form-control form-control-sm">
                                                         <option value="10">10</option>
                                                         <option value="25">25</option>
                                                         <option value="50">50</option>
                                                         <option value="100">100</option>
                                                      </select>
                                                      entries
                                                   </label>
                                                </div>
                                             </div>
                                             <div class="col-sm-12 col-md-6">
                                                <div class="gridPag">
                                                   <ul class="pagination">
                                                      <li class="page-item disabled">
                                                         <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                      </li>
                                                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                      <li class="page-item active">
                                                         <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                                      </li>
                                                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                      <li class="page-item">
                                                         <a class="page-link" href="#">Next</a>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="tabe-3" class=" tab-pane fade">
                                          <div class="addBtn">
                                             <button type="button" data-toggle="modal" data-target="#myModalNotes"><span><img src="assets/images/plus.png" alt="image"/></span></button>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-12">
                                                <div class="tableGrid">
                                                   <div class="table-responsive">
                                                      <table class="table table-hover">
                                                         <thead>
                                                            <tr>
                                                               <th>Note Date</th>
                                                               <th>Note Priority</th>
                                                               <th>Follow up Needed</th>
                                                               <th>Follow Up Date</th>
                                                               <th>Actions</th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <tr>
                                                               <td data-label="Note Date">06/24/2020</td>
                                                               <td data-label="Note Priority">High</td>
                                                               <td data-label="Follow up Neede">Yes</td>
                                                               <td data-label="Follow Up Date">06/25/2020</td>
                                                               <td data-label="Actions" class="td-actions"> <button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Note Date">06/24/2020</td>
                                                               <td data-label="Note Priority">Medium</td>
                                                               <td data-label="Follow up Neede">Yes</td>
                                                               <td data-label="Follow Up Date">06/25/2020</td>
                                                               <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Note Date">06/24/2020</td>
                                                               <td data-label="Note Priority">Low</td>
                                                               <td data-label="Follow up Neede">No</td>
                                                               <td data-label="Follow Up Date">06/25/2020</td>
                                                               <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Note Date">06/24/2020</td>
                                                               <td data-label="Note Priority">High</td>
                                                               <td data-label="Follow up Neede">Yes</td>
                                                               <td data-label="Follow Up Date">06/25/2020</td>
                                                               <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Note Date">06/24/2020</td>
                                                               <td data-label="Note Priority">High</td>
                                                               <td data-label="Follow up Neede"> Yes</td>
                                                               <td data-label="Follow Up Date">06/25/2020</td>
                                                               <td data-label="Actions"  class="td-actions"> <button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Note Date">06/24/2020</td>
                                                               <td data-label="Note Priority">Medium</td>
                                                               <td data-label="Follow up Neede">Yes</td>
                                                               <td data-label="Follow Up Date">06/25/2020</td>
                                                               <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Note Date">06/24/2020</td>
                                                               <td data-label="Note Priority">Low</td>
                                                               <td data-label="Follow up Neede">No</td>
                                                               <td data-label="Follow Up Date">06/25/2020</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Note Date">06/24/2020</td>
                                                               <td data-label="Note Priority">High</td>
                                                               <td data-label="Follow up Neede">Yes</td>
                                                               <td data-label="Follow Up Date">06/25/2020</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Note Date">06/24/2020</td>
                                                               <td data-label="Note Priority">Low</td>
                                                               <td data-label="Follow up Neede">No</td>
                                                               <td data-label="Follow Up Date">06/25/2020</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Note Date">06/24/2020</td>
                                                               <td data-label="Note Priority">High</td>
                                                               <td data-label="Follow up Neede">Yes</td>
                                                               <td data-label="Follow Up Date">06/25/2020</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-sm-12 col-md-6">
                                                <div class="showEntries">
                                                   <label>
                                                      Show 
                                                      <select class="custom-select custom-select-sm form-control form-control-sm">
                                                         <option value="10">10</option>
                                                         <option value="25">25</option>
                                                         <option value="50">50</option>
                                                         <option value="100">100</option>
                                                      </select>
                                                      entries
                                                   </label>
                                                </div>
                                             </div>
                                             <div class="col-sm-12 col-md-6">
                                                <div class="gridPag">
                                                   <ul class="pagination">
                                                      <li class="page-item disabled">
                                                         <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                      </li>
                                                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                      <li class="page-item active">
                                                         <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                                      </li>
                                                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                      <li class="page-item">
                                                         <a class="page-link" href="#">Next</a>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="tabe-4" class=" tab-pane fade">
                                          <div class="row">
                                             <div class="col-sm-12">
                                                <div class="form-group">
                                                   <label for="shtdesc">Tags</label>
                                                   <input type="text" value="Lorem Ipsum, Lorem Ipsum, Lorem Ipsum" name="tags3" />
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="tabe-5" class=" tab-pane fade">
                                          <div class="row">
                                             <div class="col-sm-12">
                                                <div class="tableGrid">
                                                   <div class="table-responsive">
                                                      <table class="table table-hover">
                                                         <thead>
                                                            <tr>
                                                               <th>ID</th>
                                                               <th>Order Name</th>
                                                               <th>Order Type</th>
                                                               <th>Order Date</th>
                                                               <th>Actions</th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <tr>
                                                               <td data-label="ID" >1</td>
                                                               <td data-label="Order Name" >Lorem Ipsum</td>
                                                               <td data-label="Order Type" >Lorem Ipsum</td>
                                                               <td data-label="Order Date" >06/24/2020</td>
                                                               <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="ID" >2</td>
                                                               <td data-label="Order Name" >Lorem Ipsum</td>
                                                               <td data-label="Order Type" >Lorem Ipsum</td>
                                                               <td data-label="Order Date" >06/28/2020</td>
                                                               <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="ID"  >3</td>
                                                               <td data-label="Order Name" >Lorem Ipsum</td>
                                                               <td data-label="Order Type" >Lorem Ipsum</td>
                                                               <td data-label="Order Date" >07/02/2020</td>
                                                               <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="ID" >4</td>
                                                               <td data-label="Order Name" >Lorem Ipsum</td>
                                                               <td data-label="Order Type" >Lorem Ipsum</td>
                                                               <td data-label="Order Date" >07/10/2020</td>
                                                               <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="ID" >5</td>
                                                               <td data-label="Order Name" >Lorem Ipsum</td>
                                                               <td data-label="Order Type" >Lorem Ipsum</td>
                                                               <td data-label="Order Date" >07/15/2020</td>
                                                               <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="ID" >6</td>

                                                               <td data-label="Order Name" >Lorem Ipsum</td>
                                                               <td data-label="Order Type" >Lorem Ipsum</td>
                                                               <td data-label="Order Date" >06/24/2020</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="ID" >7</td>
                                                               <td data-label="Order Name" >Lorem Ipsum</td>
                                                               <td data-label="Order Type" >Lorem Ipsum</td>
                                                               <td data-label="Order Date" >06/28/2020</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="ID" >8</td>
                                                               <td data-label="Order Name" >Lorem Ipsum</td>
                                                               <td data-label="Order Type" >Lorem Ipsum</td>
                                                               <td data-label="Order Date" >07/02/2020</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="ID" >9</td>
                                                               <td data-label="Order Name" >Lorem Ipsum</td>
                                                               <td data-label="Order Type" >Lorem Ipsum</td>
                                                               <td data-label="Order Date" >07/10/2020</td>
                                                               <td data-label="Actions" class="td-actions text-right"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="ID" >10</td>
                                                               <td data-label="Order Name" >Lorem Ipsum</td>
                                                               <td data-label="Order Type" >Lorem Ipsum</td>
                                                               <td data-label="Order Date" >07/15/2020</td>
                                                               <td data-label="Actions"  class="td-actions"><button type="button"  title="Edit" class="btn btn-success btn-sm"><i class="material-icons">edit</i>
                                                                  </button><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-sm-12 col-md-6">
                                                <div class="showEntries">
                                                   <label>
                                                      Show 
                                                      <select class="custom-select custom-select-sm form-control form-control-sm">
                                                         <option value="10">10</option>
                                                         <option value="25">25</option>
                                                         <option value="50">50</option>
                                                         <option value="100">100</option>
                                                      </select>
                                                      entries
                                                   </label>
                                                </div>
                                             </div>
                                             <div class="col-sm-12 col-md-6">
                                                <div class="gridPag">
                                                   <ul class="pagination">
                                                      <li class="page-item disabled">
                                                         <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                      </li>
                                                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                      <li class="page-item active">
                                                         <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                                      </li>
                                                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                      <li class="page-item">
                                                         <a class="page-link" href="#">Next</a>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="tabe-6" class=" tab-pane fade">
                                          <div class="addBtn">
                                             <button type="button" data-toggle="modal" data-target="#myModalLocation"><span><img src="assets/images/plus.png" alt="image"/></span></button>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-12">
                                                <div class="tableGrid">
                                                   <div class="table-responsive">
                                                      <table class="table table-hover">
                                                         <thead>
                                                            <tr>
                                                               <th>Location Name</th>
                                                               <th>Actions</th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <tr>
                                                               <td data-label="Location Name">Location 1</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td  data-label="Location Name">Location 2</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td  data-label="Location Name">Location 3</td>
                                                               <td class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td  data-label="Location Name">Location 1</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td  data-label="Location Name">Location 1</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td  data-label="Location Name">Location 1</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td data-label="Location Name">Location 2</td>
                                                               <td class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td  data-label="Location Name">Location 3</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td  data-label="Location Name">Location 1</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td  data-label="Location Name">Location 1</td>
                                                               <td data-label="Actions" class="td-actions"><button type="button" title="Delete" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                                  </button>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-sm-12 col-md-6">
                                                <div class="showEntries">
                                                   <label>
                                                      Show 
                                                      <select class="custom-select custom-select-sm form-control form-control-sm">
                                                         <option value="10">10</option>
                                                         <option value="25">25</option>
                                                         <option value="50">50</option>
                                                         <option value="100">100</option>
                                                      </select>
                                                      entries
                                                   </label>
                                                </div>
                                             </div>
                                             <div class="col-sm-12 col-md-6">
                                                <div class="gridPag">
                                                   <ul class="pagination">
                                                      <li class="page-item disabled">
                                                         <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                      </li>
                                                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                      <li class="page-item active">
                                                         <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                                      </li>
                                                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                      <li class="page-item">
                                                         <a class="page-link" href="#">Next</a>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="tabe-7" class=" tab-pane fade">
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Default Grid Page Size</label> 
                                                   <select class="form-control">
                                                      <option value="0">Select Grid Page </option>
                                                      <option value="0">10 </option>
                                                      <option value="0">20 </option>
                                                      <option value="0">30 </option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Default Location</label> 
                                                   <select class="form-control">
                                                      <option value="0">Select Default Location </option>
                                                      <option value="0">Location 1</option>
                                                      <option value="0">Location 2 </option>
                                                      <option value="0">Location 3 </option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Home Screen</label> 
                                                   <select class="form-control">
                                                      <option value="0">Select Home Screen</option>
                                                      <option value="0">Dashboard </option>
                                                      <option value="0">New Order </option>
                                                      <option value="0">Customer List </option>
                                                      <option value="0">Product List </option>
                                                      <option value="0">Users List </option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-sm-12">
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!---->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!---->
            </div>
         </div>
         <!---->
      </div>
      <!--Roles modal-->
      <div id="myModalRoles" class="modal fade" role="dialog">
         <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title"> Roles</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <div class="shopTables">
                     <div class="row">
                        <div class="col-sm-12">
                           <table id="example1" class="display table">
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th>Roles</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td></td>
                                    <td>Admin</td>
                                 </tr>
                                 <tr>
                                    <td></td>
                                    <td>Super Admin</td>
                                 </tr>
                                 <tr>
                                    <td></td>
                                    <td>Manager</td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss ">Save</button> 
               </div>
            </div>
         </div>
      </div>
      <!--Notes modal-->
      <div id="myModalNotes" class="modal fade" role="dialog">
         <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title"> Notes</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <div class="shopTables">
                     <div class="row">
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label>Note Date</label> 
                              <input type="date" class="form-control"/> 
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label>Note Priority</label> 
                              <select class="form-control">
                                 <option value="0">Select Priority </option>
                                 <option value="0">High </option>
                                 <option value="0">Medium </option>
                                 <option value="0">Low </option>
                              </select>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label>Follow Up Needed</label> 
                              <div class="radioBtn">
                                 <label class="radioBox"><input type="radio" id="success1" name="successname" checked=""><span class="primary"></span>
                                 </label>
                                 <span class="checkActive">Yes</span>
                              </div>
                              <div class="radioBtn">
                                 <label class="radioBox"><input type="radio" id="success2" name="successname"><span class="primary"></span>
                                 </label>	
                                 <span class="checkActive">No</span>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label>Follow Up Date</label> 
                              <input type="date" class="form-control"/> 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss ">Save</button> 
               </div>
            </div>
         </div>
      </div>
      <!--location modal-->
      <div id="myModalLocation" class="modal fade" role="dialog">
         <div class="modal-dialog modal-dialog-centered modal-md">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title"> Location</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <div class="shopTables">
                     <div class="row">
                        <div class="col-sm-12">
                           <table id="example" class="display table">
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th>Location Name</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td></td>
                                    <td>Location1</td>
                                 </tr>
                                 <tr>
                                    <td></td>
                                    <td>Location2</td>
                                 </tr>
                                 <tr>
                                    <td></td>
                                    <td>Location3</td>
                                 </tr>
                                 <tr>
                                    <td></td>
                                    <td>Location4</td>
                                 </tr>
                                 <tr>
                                    <td></td>
                                    <td>Location5</td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss ">Save</button> 
               </div>
            </div>
         </div>
      </div>
      <!---->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
      <script src="assets/js/custom.js"></script>
      <script src="assets/js/tagger.js"></script>
      <script>
         var t3 = tagger(document.querySelector('[name="tags3"]'), {
                  allow_duplicates: true,
                  allow_spaces: true,
                });
           
      </script>
      <script>
         $(document).ready(function () {
         $('#createUser').click(function(){
         $('.card.list').addClass('hide');
         $('.card.users').addClass('show');
         });	  
         });  
      </script>
      <script>
         $(document).ready(function() {
         $('#example').DataTable( {
         columnDefs: [ {
         orderable: false,
         className: 'select-checkbox',
         targets:   0
         } ],
         select: {
         style:    'os',
         selector: 'td:first-child'
         },
         order: [[ 1, 'asc' ]]
         } );
         $('#example1').DataTable( {
         columnDefs: [ {
         orderable: false,
         className: 'select-checkbox',
         targets:   0
         } ],
         select: {
         style:    'os',
         selector: 'td:first-child'
         },
         order: [[ 1, 'asc' ]]
         } );
         } ); 
      </script>
      <script>
         $(".add-product").click(function(){
                	$('.usersData').addClass("show");
                	$('.usersListing').addClass("hide");
             $('.headerCommon').addClass("show");
             $('.shortcut').addClass("hide");
                  });  
         $(".backBtn").click(function(){
                	$('.usersData').removeClass("show");
                	$('.usersListing').removeClass("hide");
             $('.headerCommon').removeClass("show");
             $('.shortcut').removeClass("hide");
                  });  
          
      </script>
      <script>
         $(document).ready(function(){
         $('.barIcon').click(function() { 
         $('.br-sideleft').toggleClass('show')
         $('.pcodedContent').toggleClass('show')
             });  
         }); 
         $(document).ready(function(){
         $('.searchBar').click(function() { 
         $('.searchBox').toggleClass('show')
             });  
         });  
      </script>
      <!--		  <script type="text/javascript" src="assets/js/steppers.js"></script>-->
      <script type="text/javascript" src="assets/js/steppers.min.js"></script>
      <script>
         $(document).ready(function () {
         $('.stepper').mdbStepper();
         })
         
         function someFunction21() {
         setTimeout(function () {
         $('#horizontal-stepper').nextStep();
         }, 2000);
         }
      </script>
      <!---->
   </body>
</html>