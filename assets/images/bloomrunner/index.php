<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Bloom Runner</title>
      <!--favicon-->
      <link rel="icon" type="image/x-icon" href="/assets/images/favicon.png">
      <!--common css-->
      <link rel="stylesheet" href="assets/css/common.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/header.css" type="text/css"/>
	  <link rel="stylesheet" href="assets/css/sidebar.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/fonts.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/footer.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/materialdesignicons.min.css" type="text/css"/>
      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet"
      <!---->
   </head>
   <body>
      <!---->
      <div class="pcoded-container navbar-wrapper">
      <?php include_once('common/header.php') ?>
      <!---->
      <div class="mainContainer">
       <?php include_once('common/sidebar.php') ?> 
	  <!----> 
         <div class="pcodedContent">
            <div class="pcodedInnerContent">
               <div class="pageBody">
				<div class="row">
				  <div class="col-sm-12">
					<h4 class="page-title">Dashboard</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
					 </ol>
				 </div>
				  </div>
                  <div class="row">
                     <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                        <div class="card card-statistics">
                           <div class="card-body">
                              <div class="d-flex align-items-center justify-content-center">
                                 <div class="highlight-icon bg-light mr-3">
                                    <i class="mdi mdi-cube text-success icon-lg"></i>
                                 </div>
                                 <div class="wrapper">
                                    <p class="card-text mb-0">Revenue</p>
                                    <div class="fluid-container">
                                       <h3 class="card-title mb-0">$65,650</h3>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                        <div class="card card-statistics">
                           <div class="card-body">
                              <div class="d-flex align-items-center justify-content-center">
                                 <div class="highlight-icon bg-light mr-3">
                                    <i class="mdi mdi-briefcase-check text-primary icon-lg"></i>
                                 </div>
                                 <div class="wrapper">
                                    <p class="card-text mb-0">Orders</p>
                                    <div class="fluid-container">
                                       <h3 class="card-title mb-0">$45,650</h3>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                        <div class="card card-statistics">
                           <div class="card-body">
                              <div class="d-flex align-items-center justify-content-center">
                                 <div class="highlight-icon bg-light mr-3">
                                    <i class="mdi mdi-account-multiple text-danger icon-lg"></i>
                                 </div>
                                 <div class="wrapper">
                                    <p class="card-text mb-0">Returns</p>
                                    <div class="fluid-container">
                                       <h3 class="card-title mb-0">$12,357</h3>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                        <div class="card card-statistics">
                           <div class="card-body">
                              <div class="d-flex align-items-center justify-content-center">
                                 <div class="highlight-icon bg-light mr-3">
                                    <i class="mdi mdi-airballoon text-info icon-lg"></i>
                                 </div>
                                 <div class="wrapper">
                                    <p class="card-text mb-0">Sales</p>
                                    <div class="fluid-container">
                                       <h3 class="card-title mb-0">$45,650</h3>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="card">
                           <div class="card-body orderStatus">
                              <h5 class="card-title mb-4">Order Status</h5>
                              <div class="row mb-5">
                                 <div class="col-md-3 col-sm-6 col-6">
                                    <div class="d-flex">
                                       <h2 class="mb-0">5675</h2>
                                       <i class="mdi mdi-arrow-up h2 mb-0 text-success"></i>
                                    </div>
                                    <p>Daily Order</p>
                                 </div>
                                 <div class="col-md-3 col-sm-6 col-6">
                                    <div class="d-flex">
                                       <h2 class="mb-0">7841</h2>
                                       <i class="mdi mdi-arrow-up h2 mb-0 text-success"></i>
                                    </div>
                                    <p>Tasks Completed</p>
                                 </div>
                                 <div class="col-md-3 col-sm-6 col-6">
                                    <div class="d-flex">
                                       <h2 class="mb-0">6521</h2>
                                       <i class="mdi mdi-arrow-down h2 mb-0 text-danger"></i>
                                    </div>
                                    <p>Monthly Sales</p>
                                 </div>
                                 <div class="col-md-3 col-sm-6 col-6">
                                    <div class="d-flex">
                                       <h2 class="mb-0">8954</h2>
                                       <i class="mdi mdi-arrow-up h2 mb-0 text-success"></i>
                                    </div>
                                    <p>Total Revenue</p>
                                 </div>
                              </div>
                              <div class="orderImage">
                                 <img src="assets/images/order-statusimage.jpg" alt="image"/>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  
               </div>
            </div>
         </div>
         <!---->
      </div>
      <!---->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	  <script src="assets/js/custom.js"></script>
   </body>
</html>