<nav class="navbar header-navbar">
         <div class="navbar-wrapper">
            <div class="navbar-logo" logo-theme="theme1">
               <a href="index.php">
               <img class="img-fluid" src="assets/images/logo.png" alt="Theme-Logo">
               </a>
            </div>
            <div class="navbar-container">
               <ul class="nav-left">
                  <div class="barIcon">
                     <span><i class="fa fa-bars"></i></span> 
                  </div>
                  <div class="search-field">
                     <form class="d-flex align-items-center h-100" action="#">
                        <div class="input-group">
                           <div class="input-group-prepend bg-transparent">
                              <i class="input-group-text border-0 fa fa-search"></i>
                           </div>
                           <input type="text" class="form-control bg-transparent border-0" placeholder="Search...">
                        </div>
                     </form>
                  </div>
               </ul>
               <ul class="nav-right">
                  <li class="header-notification">
                     <div class="dropdown-primary dropdown">
                        <div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                           <i class="fa fa-bell"></i>
                           <span class="badge bg-c-pink">5</span>
                        </div>
                        <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                           <li>
                              <h6>Notifications</h6>
                           </li>
                           <li>
                              <div class="media">
                                 <img class="d-flex align-self-center img-radius" src="assets/images/avatar-4.jpg" alt="Generic placeholder image">
                                 <div class="media-body">
                                    <h5 class="notification-user">John Doe</h5>
                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                    <span class="notification-time">30 minutes ago</span>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="media">
                                 <img class="d-flex align-self-center img-radius" src="assets/images/avatar-4.jpg" alt="Generic placeholder image">
                                 <div class="media-body">
                                    <h5 class="notification-user">Joseph William</h5>
                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                    <span class="notification-time">30 minutes ago</span>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="media">
                                 <img class="d-flex align-self-center img-radius" src="assets/images/avatar-4.jpg" alt="Generic placeholder image">
                                 <div class="media-body">
                                    <h5 class="notification-user">Sara Soudein</h5>
                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                    <span class="notification-time">30 minutes ago</span>
                                 </div>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </li>
                  <li class="user-profile header-notification">
                     <div class="dropdown-primary dropdown">
                        <div class="dropdown-toggle" data-toggle="dropdown">
                           <img src="assets/images/face1.jpg" class="img-radius" alt="User-Profile-Image">
                           <i class="feather icon-chevron-down"></i>
                        </div>
                        <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                           <li class="dropdown-item user-details">
                              <a href="javaScript:void();">
                                 <div class="media">
                                    <div class="avatar"><img class="align-self-start mr-3" src="assets/images/face1.jpg" alt="user avatar"></div>
                                    <div class="media-body">
                                       <h6 class="mt-2 user-title">Dinesh Kumar</h6>
                                       <p class="user-subtitle">dineshwins@gmail.com</p>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li class="dropdown-item"><i class="fa fa-inbox mr-2"></i> Inbox</li>
                           <li class="dropdown-item"><i class="fa fa-gear mr-2"></i> Setting</li>
                           <li class="dropdown-item"><i class="fa fa-sign-out mr-2"></i> Logout</li>
                        </ul>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
      </nav>