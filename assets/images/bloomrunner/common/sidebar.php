<div class="br-sideleft ">
            <ul class="br-sideleft-menu">
               <li class="menu-link">
                  <a href="index" class="active br-menu-link">
                  <span class="icon"><i class="fa fa-home"></i></span>
                  <span class="menuItem">Dashboard</span>
                  </a>
                  <ul class="br-menu-sub" style="">
                     <li><a href="#"><i class="fa fa-angle-double-right"></i>Add Product</a></li>
                     <li><a href="product-list"><i class="fa fa-angle-double-right"></i>Product List</a></li>
                  </ul>
               </li>
               <li class="menu-link">
                  <a href="#" class="has-menu">
                  <span class="icon"><i class="fa fa-home"></i></span>
                  <span class="menuItem">CRM</span>
                  </a>
               </li>
               <li class="menu-link">
                  <a href="#">
                  <span class="icon"><i class="fa fa-home"></i></span>
                  <span class="menuItem">Resources</span>
                  </a>
               </li>
               <li class="menu-link">
                  <a href="#">
                  <span class="icon"><i class="fa fa-home"></i></span>
                  <span class="menuItem">Inventory</span>
                  </a>
               </li>
               <li class="menu-link">
                  <a href="#"> <span class="icon"><i class="fa fa-home"></i></span>
                  <span class="menuItem">Floral Selector</span>
                  </a>
               </li>
               <li class="menu-link">
                  <a href="#"> <span class="icon"><i class="fa fa-home"></i></span>
                  <span class="menuItem">Orders</span>
                  </a>
               </li>
               <li class="menu-link">
                  <a href="#"> <span class="icon"><i class="fa fa-home"></i></span>
                  <span class="menuItem">Events</span>
                  </a>
               </li>
               <li class="menu-link">
                  <a href="#"> <span class="icon"><i class="fa fa-handshake-o"></i></span>
                  <span class="menuItem">Customers</span>
                  </a>
               </li>
               <li class="menu-link">
                  <a href="#"> <span class="icon"><i class="fa fa-home"></i></span>
                  <span class="menuItem">Delivery</span>
                  </a>
               </li>
               <li>
                  <a href="#"> <span class="icon"><i class="fa fa-home"></i></span>
                  <span class="menuItem">Financials</span>
                  </a>
               </li>
               <li class="menu-link">
                  <a href="#"> <span class="icon"><i class="fa fa-home"></i></span>
                  <span class="menuItem">Florist Profile </span>
                  </a>
               </li>
               <li class="menu-link">
                  <a href="#"> <span class="icon"><i class="fa fa-gears"></i></span>
                  <span class="menuItem">Settings</span>
                  </a>
               </li>
            </ul>
         </div>
