<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Add product - Bloom Runner</title>
      <!--favicon-->
      <link rel="icon" type="image/x-icon" href="/assets/images/favicon.png">
      <!--common css-->
      <link rel="stylesheet" href="assets/css/common.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/header.css" type="text/css"/>
	   <link rel="stylesheet" href="assets/css/sidebar.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/fonts.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/footer.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/materialdesignicons.min.css" type="text/css"/>
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic|Roboto+Mono:400,500|Material+Icons" rel="stylesheet">
      <!---->
      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet"/>
<!--      <link rel="stylesheet" href="assets/css/dataTables.css" type="text/css"/>-->
      <!---->
   </head>
   <body>
      <!---->
      <div class="pcoded-container navbar-wrapper">
      <?php include_once('common/header.php') ?>
      <!---->
      <div class="mainContainer">
         <?php include_once('common/sidebar.php') ?> 
         <!----> 
         <div class="pcodedContent">
            <div class="pcodedInnerContent">
               <div class="pageBody">
				  <div class="row">
				  <div class="col-sm-12">
					<h4 class="page-title">Add Product</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Add Product</li>
					 </ol>
				 </div>
				  </div>
                  <div class="row">
                     <div class="col-12 col-lg-12">
                        <div class="card ">
                           <div class="card-body">
                              <div class="row">
							    <div class="col-sm-6">
								  <div class="form-group">
								   <label>Product Name</label>	
								   <input class="form-control" type="text"/>
								  </div>  
								</div>
							    <div class="col-sm-6">
								  <div class="form-group">
								   <label>Product Description</label>	
								   <input class="form-control" type="text"/>
								  </div>  
								</div>
								<div class="col-sm-6">
								  <div class="form-group">
								   <label>Product Price</label>	
								   <input class="form-control" type="text"/>
								  </div>  
								</div>
								  <div class="col-sm-6">
								  <div class="form-group">
								   <label>Product Category</label>	
								   <input class="form-control" type="text"/>
								  </div>  
								</div>
								  <div class="col-sm-6">
								  <div class="form-group">
								   <label>Product Color</label>	
								   <input class="form-control" type="text"/>
								  </div>  
								</div>
							  </div>
                             </div>
                        </div>
                     </div>
                     <!---->
                  </div>
               </div>
            </div>
         </div>
         <!---->
      </div>
      <!---->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<!--
      <script src="assets/js/jquery.dataTables.min.js"></script>
      <script src="assets/js/dataTables.bootstrap4.min.js"></script>
      <script src="assets/js/datatables-demo.js"></script>
-->
      <script type="text/javascript">
         $(document).ready(function(){
         $(".menu-link ul").hide();
         // $(".menu-link ul").find('li').hide();
         $('.menu-link').click(function() {
         $(this).children('ul').find('li').show();
         $(this).children('ul').slideToggle();
         });
         });
      </script>
   </body>
</html>