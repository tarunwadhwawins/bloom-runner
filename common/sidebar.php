<?php
   $current_page = $_SERVER['REQUEST_URI']; 
   //	echo $current_page; die;
   ?>
<div class="br-sideleft ">
   <ul class="br-sideleft-menu">
      <li class="menu-link <?php echo $current_page == '/Bloomrunner/index' ? 'active':'' ?>">
         <a href="index">
         <span class="icon"><i class="fa fa-dashboard"></i></span>
         <span class="menuItem">Dashboard</span>
         </a>
      </li>
	   <li class="menu-link <?php echo $current_page == '/Bloomrunner/product' ? 'active':'' ?>">
         <a href="product">
         <span class="icon"><i class="fa fa-product-hunt"></i></span>
         <span class="menuItem">Product</span>
         </a>
      </li>
	    <li class="menu-link <?php echo $current_page == '/Bloomrunner/users' ? 'active':'' ?>">
         <a href="users">
         <span class="icon"><i class="fa fa-user"></i></span>
         <span class="menuItem">User Management</span>
         </a>
      </li>
	  <li class="menu-link <?php echo $current_page == '/Bloomrunner/florist-profile' ? 'active':'' ?>">
         <a href="florist-profile"> <span class="icon"><i class="fa fa-home"></i></span>
         <span class="menuItem">Florist Profile </span>
         </a>
      </li>
	   <li class="menu-link <?php echo $current_page == '/Bloomrunner/customers' ? 'active':'' ?>">
         <a href="customers"> <span class="icon"><i class="fa fa-user-circle-o"></i></span>
         <span class="menuItem">Customers</span>
         </a>
      </li>
	    <li class="menu-link <?php echo $current_page == '/Bloomrunner/vendors' ? 'active':'' ?>">
         <a href="vendors"> <span class="icon"><i class="fa fa-industry"></i></span>
         <span class="menuItem">Vendors </span>
         </a>
      </li>
      <li class="menu-link <?php echo $current_page == '/Bloomrunner/crm' ? 'active':'' ?>">
         <a href="crm">
         <span class="icon"><i class="fa fa-users"></i></span>
         <span class="menuItem">CRM</span>
         </a>
      </li>
      <li class="menu-link <?php echo $current_page == '/Bloomrunner/orders' ? 'active':'' ?>">
         <a href="orders"> <span class="icon"><i class="fa fa-shopping-cart"></i></span>
         <span class="menuItem">Orders</span>
         </a>
      </li>
     <li class="menu-link <?php echo $current_page == '/Bloomrunner/subscription' ? 'active':'' ?>">
         <a href="subscription"> <span class="icon"><i class="fa fa-trophy"></i></span>
         <span class="menuItem">Subscription Plan</span>
         </a>
      </li>
	    <li class="menu-link <?php echo $current_page == '/Bloomrunner/client-subscriptions' ? 'active':'' ?>">
         <a href="client-subscriptions"> <span class="icon"><i class="fa fa-address-card-o"></i></span>
         <span class="menuItem">Client Subscriptions</span>
         </a>
      </li>
	    <li class="menu-link <?php echo $current_page == '/Bloomrunner/discount-management' ? 'active':'' ?>">
         <a href="discount-management"> <span class="icon"><i class="fa fa-percent" aria-hidden="true"></i></span>
         <span class="menuItem">Manage Discounts</span>
         </a>
      </li>
	    <li class="menu-link <?php echo $current_page == '/Bloomrunner/bloom-builder' ? 'active':'' ?>">
         <a href="bloom-builder"> <span class="icon"><i class="fa fa-percent" aria-hidden="true"></i></span>
         <span class="menuItem">Bloom Builder</span>
         </a>
      </li>
   </ul>
</div>
