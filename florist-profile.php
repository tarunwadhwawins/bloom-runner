<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
	   <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" /> 
	   <meta http-equiv="Pragma" content="no-cache" /> 
	   <meta http-equiv="Expires" content="0" />
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Florist Profile - Bloom Runner</title>
      <!--favicon-->
      <link rel="icon" type="image/x-icon" href="assets/images/favicon.png">
      <!--common css-->
      <link rel="stylesheet" href="assets/css/common.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/header.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/sidebar.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/fonts.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/floral.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/footer.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/materialdesignicons.min.css" type="text/css"/>
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic|Roboto+Mono:400,500|Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
      <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
      <!---->
   </head>
   <body>
      <!---->
      <div class="pcoded-container navbar-wrapper">
      <?php include_once('common/header.php') ?>
      <!---->
      <div class="mainContainer">
      <?php include_once('common/sidebar.php') ?> 
      <!----> 
      <div class="pcodedContent">
         <div class="pcodedInnerContent">
            <div class="pageBody">
               <div class="breadcrumbInfo">
                  <div class="row">
                     <div class="col-sm-12">                        
						<div class="row mb-3">
                     	<div class="col-sm-12 col-md-10">
                        <h4 class="page-title">Florist profile</h4>
					  </div>
					 	<div class="col-sm-12 col-md-2 text-right">
								<div class="headerCommon">
									 <div class="backBtn">
										<a class="btn btn-outline-primary btn-round waves-effect waves-light backActionBtn" href="#" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
									 </div>
								  </div>
							</div>
				   </div>
                        <ol class="breadcrumb">
                           <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
                           <li class="breadcrumb-item active" aria-current="page">Florist profile</li>
                        </ol>
                     </div>
                  </div>
               </div>
               <!---->
               <div class="commonTabbing">
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="commonTabs">
                           <div class="card">
                              <div class="card-body">
                                 <ul class="nav nav-tabs nav-tabs-primary">
                                    <li class="nav-item">
                                       <a class="nav-link active" data-toggle="tab" href="#tabe-1"><i class="icon-home"></i> <span class="hidden-xs">Shop Location</span></a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" href="#tabe-2"><i class="icon-user"></i> <span class="hidden-xs">Delivery Set up</span></a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" href="#tabe-3"><i class="icon-user"></i> <span class="hidden-xs">Shop Hours</span></a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" href="#tabe-4"><i class="icon-user"></i> <span class="hidden-xs">About the Shop</span></a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" href="#tabe-5"><i class="icon-user"></i> <span class="hidden-xs">About the Owner</span></a>
                                    </li>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" href="#tabe-6"><i class="icon-user"></i> <span class="hidden-xs">Social Networking</span></a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" href="#tabe-7"><i class="icon-user"></i> <span class="hidden-xs">Preferneces</span></a>
                                 </ul>
                                 <!-- Tab panes -->
                                 <div class="tab-content">
                                    <div id="tabe-1" class=" tab-pane active">
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>Service Area</label>
                                                <input type="text" class="form-control"/> 
                                             </div>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control"/> 
                                             </div>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>Country</label>
                                                <input type="text" class="form-control"/> 
                                             </div>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>City</label>
                                                <input type="text" class="form-control"/> 
                                             </div>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>State</label>
                                                <input type="text" class="form-control"/> 
                                             </div>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>Zip Code</label>
                                                <input type="text" class="form-control"/> 
                                             </div>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>Mobile No.</label>
                                                <input type="number" class="form-control"/> 
                                             </div>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>Alt Mobile No.</label>
                                                <input type="number" class="form-control"/> 
                                             </div>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>Fax</label>
                                                <input type="number" class="form-control"/> 
                                             </div>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>Cell Phone</label>
                                                <input type="number" class="form-control"/> 
                                             </div>
                                          </div>
                                          <div class="col-sm-12">
                                             <div class="form-group">
                                                <label>Street Address</label>
                                                <textarea class="form-control"></textarea>
                                             </div>
                                          </div>
                                          <div class="col-sm-12">
                                             <hr>
                                          </div>
                                          <div class="col-sm-12">
                                             <div class="form-group">
                                                <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="tabe-2" class=" tab-pane fade">
                                       <div class="addBtn">
                                          <button type="button" data-toggle="modal" data-target="#myModal"><span><img src="assets/images/plus.png" alt="image"/></span></button>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-12">
                                             <div class="tableGrid">
                                                <div class="table-responsive">
                                                   <table class="table align-items-center table-flush">
                                                      <thead>
                                                         <tr>
                                                            <th>Zip Code</th>
                                                            <th>Delivery Charge</th>
                                                            <th>Country </th>
                                                            <th>State</th>
                                                            <th>Current Status</th>
                                                            <th>Active Status</th>
                                                            <th>Action</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody>
                                                         <tr>
                                                            <td data-label="Zip Code">160001</td>
                                                            <td data-label="Delivery Charge">15.00</td>
                                                            <td data-label="Country">India </td>
                                                            <td data-label="State">Haryana</td>
                                                            <td data-label="Current Status"><span class="badge-dot">
                                                               <i class="bg-success"></i> Enabled
                                                               </span>
                                                            </td>
                                                            <td data-label="Active Status"><span class="badge-dot">
                                                               <i class="bg-danger"></i> pending
                                                               </span>
                                                            </td>
                                                            <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                               </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                               </button>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td data-label="Zip Code">160002</td>
                                                            <td data-label="Delivery Charge">15.00</td>
                                                            <td data-label="Country">India </td>
                                                            <td data-label="State">Haryana</td>
                                                            <td data-label="Current Status"><span class="badge-dot">
                                                               <i class="bg-danger"></i> Disabled
                                                               </span>
                                                            </td>
                                                            <td data-label="Active Status"><span class="badge-dot">
                                                               <i class="bg-danger"></i> pending
                                                               </span>
                                                            </td>
                                                            <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                               </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                               </button>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td data-label="Zip Code">160003</td>
                                                            <td data-label="Delivery Charge">15.00</td>
                                                            <td data-label="Country">India </td>
                                                            <td data-label="State">Haryana</td>
                                                            <td data-label="Current Status"><span class="badge-dot">
                                                               <i class="bg-success"></i> Enabled
                                                               </span>
                                                            </td>
                                                            <td data-label="Active Status"><span class="badge-dot">
                                                               <i class="bg-success"></i> Completed
                                                               </span>
                                                            </td>
                                                            <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                               </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                               </button>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td data-label="Zip Code">160004</td>
                                                            <td data-label="Delivery Charge">15.00</td>
                                                            <td data-label="Country">India </td>
                                                            <td data-label="State">Haryana</td>
                                                            <td data-label="Current Status"><span class="badge-dot">
                                                               <i class="bg-danger"></i> Disabled
                                                               </span>
                                                            </td>
                                                            <td data-label="Active Status"><span class="badge-dot">
                                                               <i class="bg-info"></i>  On Schedule
                                                               </span>
                                                            </td>
                                                            <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                               </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                               </button>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td data-label="Zip Code">160005</td>
                                                            <td data-label="Delivery Charge">15.00</td>
                                                            <td data-label="Country">India </td>
                                                            <td data-label="State">Haryana</td>
                                                            <td data-label="Current Status"><span class="badge-dot">
                                                               <i class="bg-success"></i> Enabled
                                                               </span>
                                                            </td>
                                                            <td data-label="Active Status"><span class="badge-dot">
                                                               <i class="bg-warning"></i> Delayed
                                                               </span>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <table class="table align-items-center table-flush">
                                                     <tbody>
                                                       <tr>
                                                         <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                           </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                           </button>
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                         <td data-label="Zip Code">160006</td>
                                                         <td data-label="Delivery Charge">15.00</td>
                                                         <td data-label="Country">India </td>
                                                         <td data-label="State">Haryana</td>
                                                         <td data-label="Current Status"><span class="badge-dot">
                                                           <i class="bg-danger"></i> Disabled
                                                           </span>
                                                          </td>
                                                         <td data-label="Active Status"><span class="badge-dot">
                                                           <i class="bg-warning"></i> Delayed
                                                           </span>
                                                          </td>
                                                         <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                           </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                           </button>
                                                          </td>
                                                        </tr>
                                                       <tr>
                                                         <td data-label="Zip Code">160007</td>
                                                         <td data-label="Delivery Charge">15.00</td>
                                                         <td data-label="Country">India </td>
                                                         <td data-label="State">Haryana</td>
                                                         <td data-label="Current Status"><span class="badge-dot">
                                                           <i class="bg-danger"></i> Disabled
                                                           </span>
                                                          </td>
                                                         <td data-label="Active Status"><span class="badge-dot">
                                                           <i class="bg-warning"></i> Delayed
                                                           </span>
                                                          </td>
                                                         <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                           </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                           </button>
                                                          </td>
                                                        </tr>
                                                       <tr>
                                                         <td data-label="Zip Code">160008</td>
                                                         <td data-label="Delivery Charge">15.00</td>
                                                         <td data-label="Country">India </td>
                                                         <td data-label="State">Haryana</td>
                                                         <td data-label="Current Status"><span class="badge-dot">
                                                           <i class="bg-success"></i> Enabled
                                                           </span>
                                                          </td>
                                                         <td data-label="Active Status"><span class="badge-dot">
                                                           <i class="bg-warning"></i> Delayed
                                                           </span>
                                                          </td>
                                                         <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                           </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                           </button>
                                                          </td>
                                                        </tr>
                                                       <tr>
                                                         <td data-label="Zip Code">160009</td>
                                                         <td data-label="Delivery Charge">15.00</td>
                                                         <td data-label="Country">India </td>
                                                         <td data-label="State">Haryana</td>
                                                         <td data-label="Current Status"><span class="badge-dot">
                                                           <i class="bg-danger"></i> Disabled
                                                           </span>
                                                          </td>
                                                         <td data-label="Active Status"><span class="badge-dot">
                                                           <i class="bg-warning"></i> Delayed
                                                           </span>
                                                          </td>
                                                         <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                           </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                           </button>
                                                          </td>
                                                        </tr>
                                                       <tr>
                                                         <td data-label="Zip Code">160010</td>
                                                         <td data-label="Delivery Charge">15.00</td>
                                                         <td data-label="Country">India </td>
                                                         <td data-label="State">Haryana</td>
                                                         <td data-label="Current Status"><span class="badge-dot">
                                                           <i class="bg-danger"></i> Disabled
                                                           </span>
                                                          </td>
                                                         <td data-label="Active Status"><span class="badge-dot">
                                                           <i class="bg-warning"></i> Delayed
                                                           </span>
                                                          </td>
                                                         <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                           </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                           </button>
                                                          </td>
                                                        </tr>
                                                     </tbody>
                                                   </table>
                                                </div>
                                             </div>
                                          </div>
                                         <div class="col-sm-12 col-md-6">
                                          <div class="showEntries">
                                             <label>
                                                Show 
                                                <select class="custom-select custom-select-sm form-control form-control-sm">
                                                   <option value="10">10</option>
                                                   <option value="25">25</option>
                                                   <option value="50">50</option>
                                                   <option value="100">100</option>
                                                </select>
                                                entries
                                             </label>
                                          </div>
                                       </div>
										   <div class="col-sm-12 col-md-6">
                                          <div class="gridPag">
                                             <ul class="pagination">
                                                <li class="page-item disabled">
                                                   <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                </li>
                                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                <li class="page-item active">
                                                   <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                                </li>
                                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                <li class="page-item">
                                                   <a class="page-link" href="#">Next</a>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                       </div>

                                    </div>
                                    <div id="tabe-3" class=" tab-pane fade">
                                       <div class="addBtn">
                                          <button type="button" data-toggle="modal" data-target="#myModalShop"><span><img src="assets/images/plus.png" alt="image"/></span></button>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-12">
                                             <div class="tableGrid">
                                                <div class="table-responsive">
                                                   <table class="table align-items-center table-flush">
                                                      <thead>
                                                         <tr>
                                                            <th>First Day Closed</th>
                                                            <th>Last Day Closed</th>
                                                            <th># Days Closed </th>
                                                            <th>Action</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody>
                                                         <tr>
                                                            <td data-label="First Day Closed">Monday , 06/22/20</td>
                                                            <td data-label="Last Day Closed">Friday , 06/26/20</td>
                                                            <td data-label="# Days Closed">5 days Online Only </td>
                                                            <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                               </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                               </button>
                                                            </td>
                                                         </tr>
                                                      
                                                          <tr>
                                                            <td data-label="First Day Closed">Monday , 06/22/20</td>
                                                            <td data-label="Last Day Closed">Friday , 06/26/20</td>
                                                            <td data-label="# Days Closed">5 days Online Only </td>
                                                            <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                               </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                               </button>
                                                            </td>
                                                         </tr>
                                                        <tr>
                                                            <td data-label="First Day Closed">Monday , 06/22/20</td>
                                                            <td data-label="Last Day Closed">Friday , 06/26/20</td>
                                                            <td data-label="# Days Closed">5 days Online Only </td>
                                                            <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                               </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                               </button>
                                                            </td>
                                                         </tr>
                                                       <tr>
                                                            <td data-label="First Day Closed">Monday , 06/22/20</td>
                                                            <td data-label="Last Day Closed">Friday , 06/26/20</td>
                                                            <td data-label="# Days Closed">5 days Online Only </td>
                                                            <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                               </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                               </button>
                                                            </td>
                                                         </tr>
                                                        <tr>
                                                            <td data-label="First Day Closed">Monday , 06/22/20</td>
                                                            <td data-label="Last Day Closed">Friday , 06/26/20</td>
                                                            <td data-label="# Days Closed">5 days Online Only </td>
                                                            <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                               </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                               </button>
                                                            </td>
                                                         </tr>
                                                          <tr>
                                                            <td data-label="First Day Closed">Monday , 06/22/20</td>
                                                            <td data-label="Last Day Closed">Friday , 06/26/20</td>
                                                            <td data-label="# Days Closed">5 days Online Only </td>
                                                            <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                               </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                               </button>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td data-label="First Day Closed">Monday , 06/22/20</td>
                                                            <td data-label="Last Day Closed">Friday , 06/26/20</td>
                                                            <td data-label="# Days Closed">5 days Online Only </td>
                                                            <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                               </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                               </button>
                                                            </td>
                                                         </tr>
                                                        <tr>
                                                            <td data-label="First Day Closed">Monday , 06/22/20</td>
                                                            <td data-label="Last Day Closed">Friday , 06/26/20</td>
                                                            <td data-label="# Days Closed">5 days Online Only </td>
                                                            <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                               </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                               </button>
                                                            </td>
                                                         </tr>
                                                        <tr>
                                                            <td data-label="First Day Closed">Monday , 06/22/20</td>
                                                            <td data-label="Last Day Closed">Friday , 06/26/20</td>
                                                            <td data-label="# Days Closed">5 days Online Only </td>
                                                            <td data-label="Action" class="td-actions"><button type="button" rel="tooltip"    class="btn btn-success btn-sm"> <i class="material-icons">edit</i>
                                                               </button><button type="button" rel="tooltip" class="btn btn-danger btn-sm"> <i class="material-icons">close</i>
                                                               </button>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </div>
                                             </div>
                                          </div>
                                                <div class="col-sm-12 col-md-6">
                                          <div class="showEntries">
                                             <label>
                                                Show 
                                                <select class="custom-select custom-select-sm form-control form-control-sm">
                                                   <option value="10">10</option>
                                                   <option value="25">25</option>
                                                   <option value="50">50</option>
                                                   <option value="100">100</option>
                                                </select>
                                                entries
                                             </label>
                                          </div>
                                       </div>
										   <div class="col-sm-12 col-md-6">
                                          <div class="gridPag">
                                             <ul class="pagination">
                                                <li class="page-item disabled">
                                                   <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                </li>
                                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                <li class="page-item active">
                                                   <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                                </li>
                                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                <li class="page-item">
                                                   <a class="page-link" href="#">Next</a>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                       </div>
                                    </div>
                                    <div id="tabe-4" class=" tab-pane fade">
                                       <div class="row">
                                          <div class="col-md-3">
                                             <div class="row">
                                                <div class="col-sm-12">
                                                   <div class="form-group">
                                                      <label>Florist Image</label>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="thumbImage">
                                                               <img src="assets/images/image-3.jpg" class="img-thumbnail" alt="image"/>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-12">
                                                            <div id="mybutton">
                                                               <input type="file" id="myfile" name="upload">
                                                               <a href="#"><i class="fa fa-picture-o" aria-hidden="true"></i> Browse For Photo</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-9">
                                             <div class="row">
                                                <div class="col-sm-6">
                                                   <div class="form-group">
                                                      <label>Full Name </label>
                                                      <input type="text" class="form-control"/>      
                                                   </div>
                                                </div>
                                                <div class="col-sm-6">
                                                   <div class="form-group">
                                                      <label>Event Florist</label>
                                                      <select class="form-control">
                                                         <option value="0"> 1</option>
                                                         <option value="0"> 2</option>
                                                         <option value="0"> 3</option>
                                                      </select>
                                                   </div>
                                                </div>
                                                <div class="col-sm-12">
                                                   <div class="form-group">
                                                      <label>Florist Description</label>
                                                      <textarea class="form-control"></textarea>
                                                   </div>
                                                </div>
                                                <div class="col-sm-12">
                                                   <div class="form-group">
                                                      <label>Min. Dollar Value for Designer's Choice</label>
                                                      <input type="text" class="form-control"/>     
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-sm-12">
                                             <hr>
                                          </div>
                                          <div class="col-sm-12">
                                             <div class="form-group">
                                                <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="tabe-5" class=" tab-pane fade">
                                       <div class="row">
                                          <div class="col-md-3">
                                             <div class="row">
                                                <div class="col-sm-12">
                                                   <div class="form-group">
                                                      <label>Florist Image</label>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="thumbImage">
                                                               <img src="assets/images/image-3.jpg" class="img-thumbnail" alt="image"/>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-12">
                                                            <div id="mybutton">
                                                               <input type="file" id="myfile" name="upload">
                                                               <a href="#"><i class="fa fa-picture-o" aria-hidden="true"></i> Browse For Photo</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-9">
                                             <div class="row">
                                                <div class="col-sm-6">
                                                   <div class="form-group">
                                                      <label>Florist Position</label>
                                                      <input type="text" class="form-control"/>      
                                                   </div>
                                                </div>
                                                <div class="col-sm-6">
                                                   <div class="form-group">
                                                      <label>Florist Name</label>
                                                      <input type="text" class="form-control"/>      
                                                   </div>
                                                </div>
                                                <div class="col-sm-12">
                                                   <div class="form-group">
                                                      <label>Florist Description</label>
                                                      <textarea class="form-control"></textarea>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-sm-12">
                                             <hr>
                                          </div>
                                          <div class="col-sm-12">
                                             <div class="form-group">
                                                <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="tabe-6" class=" tab-pane fade">
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>Facebook</label>
                                                <input type="text" class="form-control" placeholder="Enter facebook Url"/>
                                             </div>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>Twitter</label>
                                                <input type="text" class="form-control" placeholder="Enter Twitter Url"/>
                                             </div>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>Instagram</label>
                                                <input type="text" class="form-control" placeholder="Enter Instagram Url"/>
                                             </div>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>Pinterest</label>
                                                <input type="text" class="form-control" placeholder="Enter Pinterest Url"/>
                                             </div>
                                          </div>
                                          <div class="col-sm-12">
                                             <hr>
                                          </div>
                                          <div class="col-sm-12">
                                             <div class="form-group">
                                                <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="tabe-7" class=" tab-pane fade">
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label>Email</label>
                                                <div class="icheck-material-primary">
                                                   <input type="checkbox" id="primary" checked="">
                                                   <label for="primary">Increase my orders with marketing emails</label>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-sm-12">
                                             <hr>
                                          </div>
                                          <div class="col-sm-12">
                                             <div class="form-group">
                                                <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss">Save</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!---->
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!---->
            </div>
         </div>
         <!---->
      </div>
      <!--delievery modal-->
      <div id="myModal" class="modal fade" role="dialog">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">New Zone</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <div class="row">
                     <div class="col-sm-12">
                        <form id="product_grid_search" method="" name="product_grid_search" class="form-inline" role="form">
                           <div class="form-group">
                              <label>From</label>
                           </div>
                           <div class="form-group">
                              <div class="input-group input-group-sm">
                                 <input type="number" name="pfrom"  class="form-control atext small-field" value="" data-orgvalue="" placeholder="0">
                              </div>
                           </div>
                           <div class="form-group">
                              <label>To</label>
                           </div>
                           <div class="form-group">
                              <div class="input-group input-group-sm">
                                 <input type="number" name="pto"  class="form-control atext small-field" value="" data-orgvalue="" placeholder="10">
                              </div>
                           </div>
                           <div class="form-group">
                              <label>Miles</label>
                           </div>
                           <div class="addBtn">
                              <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-outline-primary btn-round waves-effect waves-light btn-sm"><i class="fa fa-plus" aria-hidden="true"></i><span>Add </span> </button>
                           </div>
                        </form>
                        <div class="form-group">
                           <div class="zipCode">
                              <div class="zipcodeList">
                                 <ul>
                                    <li>160001 <i class="material-icons">close</i></li>
                                    <li>160002 <i class="material-icons">close</i></li>
                                    <li>160003 <i class="material-icons">close</i></li>
                                    <li>160004 <i class="material-icons">close</i></li>
                                    <li>160005 <i class="material-icons">close</i></li>
                                    <li>160006 <i class="material-icons">close</i></li>
                                    <li>160007 <i class="material-icons">close</i></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="form-group">
                           <label>Delivery Fee</label>
                           <input type="number" class="form-control" placeholder="$0"/>             
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="form-group">
                           <label>Same day Delivery available</label>
                           <label class="switch">
                           <input type="checkbox"  data-toggle="toggle" data-on="Ready" data-off="Not Ready" data-onstyle="success" data-offstyle="danger">
                           <span class="slider round"></span>
                           </label> 
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="form-group">
                           <label>Cut Off Time</label>
                           <input type="text" class="form-control timepicker"/>             
                        </div>
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss ">Save</button>  
               </div>
            </div>
         </div>
      </div>
      <!--Shop hours modal-->
      <div id="myModalShop" class="modal fade" role="dialog">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Shop Hours</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group">
                           <label>Days Open</label>
                           <div class="row">
                              <div class="form-check">
                                 <label>Sunday</label>
                                 <label class="switch">
                                 <input type="checkbox"  data-toggle="toggle" data-on="Ready" data-off="Not Ready" data-onstyle="success" data-offstyle="danger">
                                 <span class="slider round"></span>
                                 </label> 
                              </div>
                              <div class="form-check">
                                 <label>Monday</label>
                                 <label class="switch">
                                 <input type="checkbox"  data-toggle="toggle" data-on="Ready" data-off="Not Ready" data-onstyle="success" data-offstyle="danger">
                                 <span class="slider round"></span>
                                 </label> 
                              </div>
                              <div class="form-check">
                                 <label>Tuesday</label>
                                 <label class="switch">
                                 <input type="checkbox"  data-toggle="toggle" data-on="Ready" data-off="Not Ready" data-onstyle="success" data-offstyle="danger">
                                 <span class="slider round"></span>
                                 </label> 
                              </div>
                              <div class="form-check">
                                 <label>Wednesday</label>
                                 <label class="switch">
                                 <input type="checkbox"  data-toggle="toggle" data-on="Ready" data-off="Not Ready" data-onstyle="success" data-offstyle="danger">
                                 <span class="slider round"></span>
                                 </label> 
                              </div>
                              <div class="form-check">
                                 <label>Thursday</label>
                                 <label class="switch">
                                 <input type="checkbox"  data-toggle="toggle" data-on="Ready" data-off="Not Ready" data-onstyle="success" data-offstyle="danger">
                                 <span class="slider round"></span>
                                 </label> 
                              </div>
                              <div class="form-check">
                                 <label>Friday</label>
                                 <label class="switch">
                                 <input type="checkbox"  data-toggle="toggle" data-on="Ready" data-off="Not Ready" data-onstyle="success" data-offstyle="danger">
                                 <span class="slider round"></span>
                                 </label> 
                              </div>
                              <div class="form-check">
                                 <label>Saturday</label>
                                 <label class="switch">
                                 <input type="checkbox"  data-toggle="toggle" data-on="Ready" data-off="Not Ready" data-onstyle="success" data-offstyle="danger">
                                 <span class="slider round"></span>
                                 </label> 
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="shopTables">
                           <label>Holidays</label>
                           <div class="table-responsive">
                              <table class="table align-items-center table-flush table-bordered">
                                 <thead>
                                    <tr>
                                       <th>Holiday Date</th>
                                       <th>Holiday Name </th>
                                       <th>Holiday Day </th>
                                       <th class="text-right">Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>06/23/2020</td>
                                       <td>Ratha Yatra</td>
                                       <td>Tuesday </td>
                                       <td class="td-actions text-right">
                                          <div class="radioBtn">
                                             <label class="radioBox"><input type="radio" id="success1" name="successname" checked=""><span class="primary"></span>
                                             </label>
                                             <span class="checkActive">Open</span>
                                          </div>
                                          <div class="radioBtn">
                                             <label class="radioBox"><input type="radio" id="success2" name="successname"><span class="primary"></span>
                                             </label>	
                                             <span class="checkActive">Close</span>
                                          </div>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>06/28/2020</td>
                                       <td>Kharchi Puja</td>
                                       <td>Sunday </td>
                                       <td class="td-actions text-right">
                                          <div class="radioBtn">
                                             <label class="radioBox"><input type="radio" id="success1" name="successname" checked=""><span class="primary"></span>
                                             </label>
                                             <span class="checkActive">Open</span>
                                          </div>
                                          <div class="radioBtn">
                                             <label class="radioBox"><input type="radio" id="success2" name="successname"><span class="primary"></span>
                                             </label>	
                                             <span class="checkActive">Close</span>
                                          </div>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>07/05/2020</td>
                                       <td>Guru Purnima Festival</td>
                                       <td>Sunday </td>
                                       <td class="td-actions text-right">
                                          <div class="radioBtn">
                                             <label class="radioBox"><input type="radio" id="success1" name="successname" checked=""><span class="primary"></span>
                                             </label>
                                             <span class="checkActive">Open</span>
                                          </div>
                                          <div class="radioBtn">
                                             <label class="radioBox"><input type="radio" id="success2" name="successname"><span class="primary"></span>
                                             </label>	
                                             <span class="checkActive">Close</span>
                                          </div>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>07/14/2020</td>
                                       <td>Ker Puja</td>
                                       <td>Tuesday </td>
                                       <td class="td-actions text-right">
                                          <div class="radioBtn">
                                             <label class="radioBox"><input type="radio" id="success1" name="successname" checked=""><span class="primary"></span>
                                             </label>
                                             <span class="checkActive">Open</span>
                                          </div>
                                          <div class="radioBtn">
                                             <label class="radioBox"><input type="radio" id="success2" name="successname"><span class="primary"></span>
                                             </label>	
                                             <span class="checkActive">Close</span>
                                          </div>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>07/16/2020</td>
                                       <td>Bonalu</td>
                                       <td>Thursday </td>
                                       <td class="td-actions text-right">
                                          <div class="radioBtn">
                                             <label class="radioBox"><input type="radio" id="success1" name="successname" checked=""><span class="primary"></span>
                                             </label>
                                             <span class="checkActive">Open</span>
                                          </div>
                                          <div class="radioBtn">
                                             <label class="radioBox"><input type="radio" id="success2" name="successname"><span class="primary"></span>
                                             </label>	
                                             <span class="checkActive">Close</span>
                                          </div>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>07/23/2020</td>
                                       <td>Haryali Teej</td>
                                       <td>Thursday </td>
                                       <td class="td-actions text-right">
                                          <div class="radioBtn">
                                             <label class="radioBox"><input type="radio" id="success1" name="successname" checked=""><span class="primary"></span>
                                             </label>
                                             <span class="checkActive">Open</span>
                                          </div>
                                          <div class="radioBtn">
                                             <label class="radioBox"><input type="radio" id="success2" name="successname"><span class="primary"></span>
                                             </label>	
                                             <span class="checkActive">Close</span>
                                          </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label>Weekday Hours</label>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <input type="text" class="form-control timepicker" placeholder="From"/>  
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <input type="text" class="form-control timepicker" placeholder="To"/>  
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label>Saturday Hours</label>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <input type="text" class="form-control timepicker" placeholder="From"/>  
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <input type="text" class="form-control timepicker" placeholder="To"/>  
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label>Sunday Hours</label>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <input type="text" class="form-control timepicker" placeholder="From"/>  
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <input type="text" class="form-control timepicker" placeholder="To"/>  
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-outline-primary btn-round waves-effect waves-light btnCss ">Save</button>  
               </div>
            </div>
         </div>
      </div>
      <!---->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
      <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
      <script src="assets/js/custom.js"></script>
      <script>
         $('.timepicker').timepicker({
             uiLibrary: 'bootstrap4'
         });
      </script>
		  		  <script>
		  $(document).ready(function(){
			$('.barIcon').click(function() { 
			$('.br-sideleft').toggleClass('show')
			$('.pcodedContent').toggleClass('show')
		      });  
			});  
		$(document).ready(function(){
			$('.searchBar').click(function() { 
			$('.searchBox').toggleClass('show')
		      });  
			});  
		  </script>
      <!---->
   </body>
</html>