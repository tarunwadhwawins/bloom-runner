<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
	   <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" /> 
	   <meta http-equiv="Pragma" content="no-cache" /> 
	   <meta http-equiv="Expires" content="0" />
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Orders - Bloom Runner</title>
      <!--favicon-->
      <link rel="icon" type="image/x-icon" href="assets/images/favicon.png">
      <!--common css-->
      <link rel="stylesheet" href="assets/css/common.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/header.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/sidebar.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/fonts.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/floral.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/footer.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/materialdesignicons.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/tagger.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/wizard.css" type="text/css"/>
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic|Roboto+Mono:400,500|Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/select.dataTables.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css" type="text/css"/>
   </head>
   <body>
      <!---->
      <div class="pcoded-container navbar-wrapper">
      <?php include_once('common/header.php') ?>
      <!---->
      <div class="mainContainer">
      <?php include_once('common/sidebar.php') ?> 
      <!----> 
      <div class="pcodedContent">
         <div class="pcodedInnerContent">
            <div class="pageBody">
               <div class="breadcrumbInfo">
                  <div class="row">
                     <div class="col-sm-12">
						<div class="row mb-3">
							<div class="col-sm-12 col-md-10">
								<h4 class="page-title">Orders</h4>
							</div>
							<div class="col-sm-12 col-md-2 text-right">
								<div class="headerCommon">
									 <div class="backBtn">
										<a class="btn btn-outline-primary btn-round waves-effect waves-light backActionBtn" href="#" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
									 </div>
								  </div>
							</div>
						</div>
                        <ol class="breadcrumb">
                           <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
                           <li class="breadcrumb-item active" aria-current="page">Orders</li>
                        </ol>
                     </div>
                  </div>
               </div>
               <!---->
               <div class="commonTabbing">
                  <!--div class="headerCommon">
                     <div class="backBtn">
                        <a class="btn btn-outline-primary btn-round waves-effect waves-light" href="#" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                     </div>
                  </div-->
                  <div class="usersListing">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="card">
                              <div class="card-body">
                                 <div class="usersList">
                                    <div class="row">
                                       <!---->
                                       <div class="col-sm-12 col-md-11">
                                          <div class="searchListingCol">
											  <div class="searchFilter">
												 <label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="dtBasicExample"></label>
											  </div>
											  <div class="advanceSearch">
												 <div class="dropdown">
													<button class="btn btn-outline-primary btn-round waves-effect waves-light dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-filter"></i></button>
													<ul class="dropdown-menu">
													   <input class="form-control" id="myInput" type="text" placeholder="Search..">
													   <li><a href="#">Sort A -> Z</a></li>
													   <li><a href="#">Sort Z -> A</a></li>
													   <li><a href="#">Sort by Color</a></li>
													</ul>
												 </div>
											  </div>
											  <div class="proListing">
												 <ul class="order-bar">
													<li><a href="#">All <span>(2)</span></a></li>
													<li><a href="#">Trash <span>(5)</span></a></li>
													<li><a href="#">Pending <span>(3)</span></a></li>
													<li><a href="#">Processing <span>(1)</span></a></li>
													<li><a href="#">Onhold <span>(2)</span></a></li>
													<li><a href="#">Completed <span>(4)</span></a></li>
												 </ul>
											  </div>
                                          </div>
                                       </div>
                                       <div class="col-sm-12 col-md-1">
                                          <div class="createUser">
                                             <a class="btn btn-icon add-product" href="#" title="Add User"><span><img src="assets/images/plus.png" alt="image"/></span></a>
                                          </div>
                                       </div>
                                       <div class="col-sm-12">
                                          <div class="tableGrid">
                                             <div class="table-responsive">
                                                <table id="dtBasicExample" class="table table-hover">
                                                   <thead>
                                                      <tr>
                                                         <th>Order</th>
                                                         <th>Date</th>
                                                         <th>Status</th>
                                                         <th>Ship to</th>
                                                         <th>Total</th>
                                                         <th>Actions</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      <tr>
                                                         <td>#455 Lorum Ipsum</td>
                                                         <td>Jun 10,2020</td>
                                                         <td><span class="badge-dot"><i class="bg-success"></i> Completed</span></td>
                                                         <td>Lorum Ipsum</td>
                                                         <td>$9</td>
                                                         <td class="td-actions">
                                                            <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-ellipsis-h"></i></button>
                                                            <button type="button" title="Edit"  class="btn btn-success btn-sm">
                                                               <i class="fa fa-check"></i>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                      <td>#455 Lorum Ipsum</td>
                                                      <td>Jun 10,2020</td>
                                                      <td><span class="badge-dot"><i class="bg-danger"></i> Pending</span></td>
                                                      <td>Lorum Ipsum</td>
                                                      <td>$9</td>
                                                      <td class="td-actions">
                                                      <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-ellipsis-h"></i></button>
                                                      <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="fa fa-check"></i>
                                                      </td>
                                                      </tr>
                                                      <tr>
                                                      <td>#455 Lorum Ipsum</td>
                                                      <td>Jun 10,2020</td>
                                                      <td><span class="badge-dot"><i class="bg-success"></i> Completed</span></td>
                                                      <td>Lorum Ipsum</td>
                                                      <td>$9</td>
                                                      <td class="td-actions">
                                                      <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-ellipsis-h"></i></button>
                                                      <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="fa fa-check"></i>
                                                      </td>
                                                      </tr>
                                                      <tr>
                                                      <td>#455 Lorum Ipsum</td>
                                                      <td>Jun 10,2020</td>
                                                      <td><span class="badge-dot"><i class="bg-danger"></i> Pending</span></td>
                                                      <td>Lorum Ipsum</td>
                                                      <td>$9</td>
                                                      <td class="td-actions">
                                                      <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-ellipsis-h"></i></button>
                                                      <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="fa fa-check"></i>
                                                      </td>
                                                      </tr>
                                                      <tr>
                                                      <td>#455 Lorum Ipsum</td>
                                                      <td>Jun 10,2020</td>
                                                      <td><span class="badge-dot"><i class="bg-success"></i> Completed</span></td>
                                                      <td>Lorum Ipsum</td>
                                                      <td>$9</td>
                                                      <td class="td-actions">
                                                      <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-ellipsis-h"></i></button>
                                                      <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="fa fa-check"></i>
                                                      </td>
                                                      </tr>
                                                      <tr>
                                                      <td>#455 Lorum Ipsum</td>
                                                      <td>Jun 10,2020</td>
                                                      <td><span class="badge-dot"><i class="bg-danger"></i> Pending</span></td>
                                                      <td>Lorum Ipsum</td>
                                                      <td>$9</td>
                                                      <td class="td-actions">
                                                      <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-ellipsis-h"></i></button>
                                                      <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="fa fa-check"></i>
                                                      </td>
                                                      </tr>
                                                      <tr>
                                                      <td>#455 Lorum Ipsum</td>
                                                      <td>Jun 10,2020</td>
                                                      <td><span class="badge-dot"><i class="bg-success"></i> Completed</span></td>
                                                      <td>Lorum Ipsum</td>
                                                      <td>$9</td>
                                                      <td class="td-actions">
                                                      <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-ellipsis-h"></i></button>
                                                      <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="fa fa-check"></i>
                                                      </td>
                                                      </tr>
                                                      <tr>
                                                      <td>#455 Lorum Ipsum</td>
                                                      <td>Jun 10,2020</td>
                                                      <td><span class="badge-dot"><i class="bg-danger"></i> Pending</span></td>
                                                      <td>Lorum Ipsum</td>
                                                      <td>$9</td>
                                                      <td class="td-actions">
                                                      <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-ellipsis-h"></i></button>
                                                      <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="fa fa-check"></i>
                                                      </td>
                                                      </tr>
                                                      <tr>
                                                      <td>#455 Lorum Ipsum</td>
                                                      <td>Jun 10,2020</td>
                                                      <td><span class="badge-dot"><i class="bg-success"></i> Completed</span></td>
                                                      <td>Lorum Ipsum</td>
                                                      <td>$9</td>
                                                      <td class="td-actions">
                                                      <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-ellipsis-h"></i></button>
                                                      <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="fa fa-check"></i>
                                                      </td>
                                                      </tr>
                                                      <tr>
                                                      <td>#455 Lorum Ipsum</td>
                                                      <td>Jun 10,2020</td>
                                                      <td><span class="badge-dot"><i class="bg-danger"></i> Pending</span></td>
                                                      <td>Lorum Ipsum</td>
                                                      <td>$9</td>
                                                      <td class="td-actions">
                                                      <button type="button" title="View" class="btn btn-info btn-sm"><i class="fa fa-ellipsis-h"></i></button>
                                                      <button type="button" title="Edit"  class="btn btn-success btn-sm"> <i class="fa fa-check"></i>
                                                      </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                       </div>
                                       <!---->
                                       <div class="col-sm-12 col-md-6">
                                       <div class="showEntries">
                                       <label>
                                       Show 
                                       <select class="custom-select custom-select-sm form-control form-control-sm">
                                       <option value="10">10</option>
                                       <option value="25">25</option>
                                       <option value="50">50</option>
                                       <option value="100">100</option>
                                       </select>
                                       entries
                                       </label>
                                       </div>
                                       </div>
                                       <div class="col-sm-12 col-md-6">
                                       <div class="gridPag">
                                       <ul class="pagination">
                                       <li class="page-item disabled">
                                       <a class="page-link" href="#" tabindex="-1">Previous</a>
                                       </li>
                                       <li class="page-item"><a class="page-link" href="#">1</a></li>
                                       <li class="page-item active">
                                       <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                       </li>
                                       <li class="page-item"><a class="page-link" href="#">3</a></li>
                                       <li class="page-item">
                                       <a class="page-link" href="#">Next</a>
                                       </li>
                                       </ul>
                                       </div>
                                       </div>
                                       <!---->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="usersData">
   <div class="row">
      <div class="col-sm-12">
         <div class="add-product-detail">
            <div class="card">
               <div class="row">
                  <div class="col-md-12 mx-0">
                     <form class="msform">
                        <!-- progressbar -->
                        <ul class="progressbar">
                           <li class="active search"><strong>Customer Search</strong></li>
                           <li class="account"><strong>Customer Info</strong></li>
                           <li class="personal"><strong>Recipient Info</strong></li>
                           <li class="payment"><strong>Payment Info</strong></li>
                           <li class="order"><strong>Order Info</strong></li>
                           <li class="product"><strong>Product  Info</strong></li>
                           <li class="confirm"><strong>Finish</strong></li>
                        </ul>
                        <!-- fieldsets -->
                        <fieldset>
                           <div class="form-card">
                              <h2 class="fs-title">Customer Search</h2>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="action-btn">
                                       <ul>
                                          <li><a href="#" class="edit"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                          <li><a href="#" class="copy"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                          <li><a href="#" class="close"><i class="fa fa-times-circle-o" aria-hidden="true"></i></a></li>
                                       </ul>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>First Name</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>Last Name</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>Company</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>Email</label>
                                       <input type="email" name="email" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>Address</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>City</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>Zip Code</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>Phone Number</label>
                                       <input type="text" name="uname" class="form-control" />
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                           <input type="button" name="next" class="next action-button" value="Next Step" />
                        </fieldset>
                        <fieldset>
                           <div class="form-card">
                              <h2 class="fs-title">Customer Info</h2>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group"><label>First Name</label>
                                       <input type="text" name="uname" class="form-control" />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Last Name</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Company</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Email Id</label>
                                       <input type="email" name="email" class="form-control"  />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Address</label>
                                       <input type="text" name="uname" class="form-control" />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>City</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Zip Code</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Phone Number</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                        </fieldset>
                        <fieldset>
                           <div class="form-card">
                              <h2 class="fs-title">Recipient Information</h2>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group"><label>First Name</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Last Name</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Company</label>
                                       <input type="text" name="uname" class="form-control" />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Email Id</label>
                                       <input type="email" name="email" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Address</label>
                                       <input type="text" name="uname" class="form-control" />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>City</label>
                                       <input type="text" name="uname" class="form-control" />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Zip Code"</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Phone Number</label>
                                       <input type="text" name="uname" class="form-control" />
                                    </div>
                                 </div>
                                 <div class="col-6">
                                    <div class="form-group">
                                       <label>Location</label>
                                       <select class="list-dt form-control" id="month" name="expmonth">
                                          <option selected> Select Location </option>
                                          <option>Location 2</option>
                                          <option>Location 3</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-6">
                                    <div class="form-group">
                                       <label>Zone</label>
                                       <select class="list-dt form-control" id="month" name="expmonth">
                                          <option selected> Select Zone</option>
                                          <option>Zone 2</option>
                                          <option>Zone 3</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-6">
                                    <div class="form-group">
                                       <label>Delivery Option</label>
                                       <select class="list-dt form-control" id="month" name="expmonth">
                                          <option selected>Select Delivery Option</option>
                                          <option>Delivery 2</option>
                                          <option>Delivery 3</option>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                        </fieldset>
                        <fieldset>
                           <div class="form-card">
                              <h2 class="fs-title">Payment Information</h2>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group"><label>First Name</label>
                                       <input type="text" name="uname" class="form-control"  s />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Last Name</label>
                                       <input type="text" name="uname" class="form-control"   />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>CC Number</label>
                                       <input type="text" name="uname" class="form-control"   />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Expiry Date</label>
                                       <input type="Date" name="email" class="form-control"   />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>CVV</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Zip Code</label>
                                       <input type="text" name="uname" class="form-control" />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>City</label>
                                       <input type="text" name="uname" class="form-control"  />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Total</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Coupon</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Discount</label>
                                       <input type="text" name="uname" class="form-control" />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Tax</label>
                                       <input type="text" name="uname" class="form-control" />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Sub-Total</label>
                                       <input type="text" name="uname" class="form-control"/>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="make_payment" class="next action-button" value="Next" />
                        </fieldset>
                        <fieldset>
                           <div class="form-card">
                              <h2 class="fs-title">Order Information</h2>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Order ID</label>
                                       <input type="text" name="uname" class="form-control" ssss/>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Product Id</label>
                                       <input type="text" name="uname" class="form-control"   />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Product Name</label>
                                       <input type="text" name="uname" class="form-control"  />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Sub-Total</label>
                                       <input type="text" name="uname" class="form-control" />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Discount</label>
                                       <input type="text" name="email" class="form-control"  />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group"><label>Income</label>
                                       <input type="text" name="uname" class="form-control"  />
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <div class="form-group"><label>Card Message</label>
                                       <textarea type="text" name="uname" class="form-control"   rows="4" /></textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="make_payment" class="next action-button" value="Next" />
                        </fieldset>
                        <fieldset>
                           <div class="form-card">
                              <h2 class="fs-title">Product Information</h2>
                              <table class="table table-borderless">
                                 <thead>
                                    <tr>
                                       <th>Item</th>
                                       <th>Description</th>
                                       <th>Quantity</th>
                                       <th>Price</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td><input type="text" name="uname" placeholder="Item" class="form-control"/></td>
                                       <td><input type="text" name="uname" placeholder="Description" class="form-control"/></td>
                                       <td><input type="text" name="uname" placeholder="Quantity" class="form-control" /></td>
                                       <td><input type="text" name="uname" placeholder="Price" class="form-control"/></td>
                                       <td>
                                          <div class="edit">
                                             <ul>
                                                <li><a href="#" class="add-product"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                <li><a href="#" class="add-product"><i class="fa fa-times-circle" aria-hidden="true"></i></a></li>
                                             </ul>
                                          </div>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td><input type="text" name="uname" placeholder="Item" class="form-control"/></td>
                                       <td><input type="text" name="uname" placeholder="Description" class="form-control" /></td>
                                       <td><input type="text" name="uname" placeholder="Quantity" class="form-control"  /></td>
                                       <td><input type="text" name="uname" placeholder="Price" class="form-control"ss /></td>
                                       <td>
                                          <div class="edit">
                                             <ul>
                                                <li><a href="#" class="add-product"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                <li><a href="#" class="add-product"><i class="fa fa-times-circle" aria-hidden="true"></i></a></li>
                                             </ul>
                                          </div>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td><input type="text" name="uname" placeholder="Item" class="form-control" /></td>
                                       <td><input type="text" name="uname" placeholder="Description" class="form-control" /></td>
                                       <td><input type="text" name="uname" placeholder="Quantity" class="form-control" /></td>
                                       <td><input type="text" name="uname" placeholder="Price" class="form-control" /></td>
                                       <td>
                                          <div class="edit">
                                             <ul>
                                                <li><a href="#" class="add-product"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                <li><a href="#" class="add-product"><i class="fa fa-times-circle" aria-hidden="true"></i></a></li>
                                             </ul>
                                          </div>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td><input type="text" name="uname" placeholder="Item" class="form-control" /></td>
                                       <td><input type="text" name="uname" placeholder="Description" class="form-control"/></td>
                                       <td><input type="text" name="uname" placeholder="Quantity" class="form-control" /></td>
                                       <td><input type="text" name="uname" placeholder="Price" class="form-control" /></td>
                                       <td>
                                          <div class="edit">
                                             <ul>
                                                <li><a href="#" class="add-product"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                <li><a href="#" class="add-product"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
                                             </ul>
                                          </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <div class="row">
                                 <div class="col-md-6">
                                    <textarea type="text" name="uname" placeholder="Special Instructions " rows="4" class="form-control"/></textarea>
                                 </div>
                                 <div class="col-md-6">
                                    <textarea type="text" name="uname" placeholder="Shipping Instructions" rows="4" class="form-control" /></textarea>
                                 </div>
                              </div>
                           </div>
                           <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="make_payment" class="next action-button" value="Confirm" />
                        </fieldset>
                        <fieldset>
                           <div class="form-card">
                              <h2 class="fs-title text-center">Success !</h2>
                              <br><br>
                              <div class="row justify-content-center">
                                 <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>
                              </div>
                              <br><br>
                              <div class="row justify-content-center">
                                 <div class="col-7 text-center">
                                    <!--  <h5>You Have Successfully Signed Up</h5>-->
                                 </div>
                              </div>
                           </div>
                        </fieldset>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
               </div>
               <!---->
            </div>
         </div>
         <!---->
      </div>
      <!---->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
      <script src="assets/js/custom.js"></script>
      <script>
         $(document).ready(function() {
         $('#example').DataTable( {
         columnDefs: [ {
         orderable: false,
         className: 'select-checkbox',
         targets:   0
         } ],
         select: {
         style:    'os',
         selector: 'td:first-child'
         },
         order: [[ 1, 'asc' ]]
         } );
         $('#example1').DataTable( {
         columnDefs: [ {
         orderable: false,
         className: 'select-checkbox',
         targets:   0
         } ],
         select: {
         style:    'os',
         selector: 'td:first-child'
         },
         order: [[ 1, 'asc' ]]
         } );
         } ); 
      </script>
      <script>
         $(".add-product").click(function(){
                	$('.usersData').addClass("show");
                	$('.usersListing').addClass("hide");
             $('.headerCommon').addClass("show");
             $('.shortcut').addClass("hide");
                  });  
         $(".backBtn").click(function(){
                	$('.usersData').removeClass("show");
                	$('.usersListing').removeClass("hide");
             $('.headerCommon').removeClass("show");
             $('.shortcut').removeClass("hide");
                  });  
          
      </script>
      <script>
         $(document).ready(function(){
         $('.barIcon').click(function() { 
         $('.br-sideleft').toggleClass('show')
         $('.pcodedContent').toggleClass('show')
             });  
         });  
      </script>
      <script>
         $(document).ready(function(){
         
         var current_fs, next_fs, previous_fs; //fieldsets
         var opacity;
         
         $(".next").click(function(){
         
         current_fs = $(this).parent();
         next_fs = $(this).parent().next();
         
         //Add Class Active
         $(".progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
         
         //show the next fieldset
         next_fs.show();
         //hide the current fieldset with style
         current_fs.animate({opacity: 0}, {
         step: function(now) {
         // for making fielset appear animation
         opacity = 1 - now;
         
         current_fs.css({
         'display': 'none',
         'position': 'relative'
         });
         next_fs.css({'opacity': opacity});
         },
         duration: 600
         });
         });
         
         $(".previous").click(function(){
         
         current_fs = $(this).parent();
         previous_fs = $(this).parent().prev();
         
         //Remove class active
         $(".progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
         
         //show the previous fieldset
         previous_fs.show();
         
         //hide the current fieldset with style
         current_fs.animate({opacity: 0}, {
         step: function(now) {
         // for making fielset appear animation
         opacity = 1 - now;
         
         current_fs.css({
         'display': 'none',
         'position': 'relative'
         });
         previous_fs.css({'opacity': opacity});
         },
         duration: 600
         });
         });
         
         $('.radio-group .radio').click(function(){
         $(this).parent().find('.radio').removeClass('selected');
         $(this).addClass('selected');
         });
         
         $(".submit").click(function(){
         return false;
         })
         
         });
      </script>
      <!---->
   </body>
</html>