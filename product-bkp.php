<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Product - Bloom Runner</title>
      <!--favicon-->
      <link rel="icon" type="image/x-icon" href="assets/images/favicon.png">
      <!--common css-->
      <link rel="stylesheet" href="assets/css/common.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/header.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/sidebar.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/fonts.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/footer.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/materialdesignicons.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/tagger.css" type="text/css"/>
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic|Roboto+Mono:400,500|Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet"/>
      <link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css" type="text/css"/>
      <!---->
   </head>
   <body>
      <!---->
      <div class="pcoded-container navbar-wrapper">
      <?php include_once('common/header.php') ?>
      <!---->
      <div class="mainContainer">
         <?php include_once('common/sidebar.php') ?> 
         <!----> 
         <div class="pcodedContent">
            <div class="pcodedInnerContent">
               <div class="pageBody">
                  <div class="row">
                     <div class="col-sm-12">
                        <h4 class="page-title">Product List</h4>
                        <ol class="breadcrumb">
                           <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
                           <li class="breadcrumb-item active" aria-current="page">Product List</li>
                        </ol>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-12 col-lg-12">
                        <div class="card ">
                           <div class="card-body orderTables">
                              <div class="grid">
                                 <div class="row">
                                    <div class="col-sm-2">
                                       <div class="grid-icon">
                                          <ul>
                                             <li><a href="#" class="gridView"><i class="fa fa-th" aria-hidden="true"></i></a></li>
                                             <li><a href="#" class="listingView"><i class="fa fa-list" aria-hidden="true"></i></a></li>
                                          </ul>
                                       </div>
                                    </div>
                                    <div class="col-sm-10">
                                       <div class="shortcut">
                                          <div class="wrapper">
                                             <a class="btn btn-icon add-product" href="#"><i class="fa fa-product-hunt" aria-hidden="true"></i><span>Add More Products</span></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="add-product-heading">
                                 <div class="row">
                                    <div class="col-md-1">
                                       <a href="#" class="back-button"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col-md-1">
                                       <div class="empty-img"><i class="fa fa-clone" aria-hidden="true"></i></div>
                                    </div>
                                    <div class="col-md-4">
                                       <h5>New Product</h5>
                                       <p>Product Detail</p>
                                    </div>
                                    <div class="col-md-6 text-right">
                                       <button class="save">Save as Draft</button>
                                       <button class="save">Preview </button>
                                       <button class="save">Publish</button>
                                    </div>
                                 </div>
                              </div>
                              <div class="proListing">
								 <div class="productTable">
                                  <div class="table-responsive">
                                    <table id="productList" class="table align-items-center table-flush product-list-table" id="dataTable1">
                                       <thead>
                                          <tr>
                                             <th>Image</th>
                                             <th>Name</th>
                                             <th>Cost</th>
                                             <th>Selling Price</th>
                                             <th>Total Sold</th>
                                             <th>Status</th>
                                             <th>Actions</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr>
                                             <td>
                                                <img src="assets/images/image-1.jpg">
                                             </td>
                                             <td>Succulent Terrariums</td>
                                             <td>$15</td>
                                             <td>$10</td>
                                             <td>$30</td>
                                             <td>Lorum Ipsum</td>
                                             <td>
                                                <div class="edit">
                                                   <ul>
                                                      <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                   </ul>
                                                </div>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <img src="assets/images/image-3.jpg">
                                             </td>
                                             <td>Planters</td>
                                             <td>$15</td>
                                             <td>$20</td>
                                             <td>$30</td>
                                             <td>Lorum Ipsum</td>
                                             <td>
                                                <div class="edit">
                                                   <ul>
                                                      <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                   </ul>
                                                </div>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <img src="assets/images/image-4.jpg">
                                             </td>
                                             <td>Chocolates </td>
                                             <td>$15</td>
                                             <td>$15</td>
                                             <td>$39</td>
                                             <td>Lorum Ipsum</td>
                                             <td>
                                                <div class="edit">
                                                   <ul>
                                                      <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                   </ul>
                                                </div>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <img src="assets/images/image-1.jpg">
                                             </td>
                                             <td>Fragrance</td>
                                             <td>$15</td>
                                             <td>$40</td>
                                             <td>$30</td>
                                             <td>Lorum Ipsum</td>
                                             <td>
                                                <div class="edit">
                                                   <ul>
                                                      <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                   </ul>
                                                </div>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <img src="assets/images/image-3.jpg">
                                             </td>
                                             <td>Greeting Cards</td>
                                             <td>$15</td>
                                             <td>$10</td>
                                             <td>$30</td>
                                             <td>Lorum Ipsum</td>
                                             <td>
                                                <div class="edit">
                                                   <ul>
                                                      <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                   </ul>
                                                </div>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <img src="assets/images/image-4.jpg">
                                             </td>
                                             <td>Green and Flowering house plants</td>
                                             <td>$15</td>
                                             <td>$20</td>
                                             <td>$10</td>
                                             <td>Lorum Ipsum</td>
                                             <td>
                                                <div class="edit">
                                                   <ul>
                                                      <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                   </ul>
                                                </div>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <img src="assets/images/image-5.jpg">
                                             </td>
                                             <td>Hand -tied bouquets</td>
                                             <td>$15</td>
                                             <td>$10</td>
                                             <td>$20</td>
                                             <td>Lorum Ipsum</td>
                                             <td>
                                                <div class="edit">
                                                   <ul>
                                                      <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                   </ul>
                                                </div>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <img src="assets/images/image-1.jpg">
                                             </td>
                                             <td>Unique containers</td>
                                             <td>$15</td>
                                             <td>$10</td>
                                             <td>$30</td>
                                             <td>Lorum Ipsum</td>
                                             <td>
                                                <div class="edit">
                                                   <ul>
                                                      <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                   </ul>
                                                </div>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <img src="assets/images/image-3.jpg">
                                             </td>
                                             <td>Vases </td>
                                             <td>$15</td>
                                             <td>$15</td>
                                             <td>$10</td>
                                             <td>Lorum Ipsum</td>
                                             <td>
                                                <div class="edit">
                                                   <ul>
                                                      <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                      <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                   </ul>
                                                </div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
							    </div>
                                 <div class="product-descr">
                                    <div class="row">
                                       <div class="col-md-12">
                                          <nav class="nav-justified ">
                                             <div class="nav nav-tabs " id="nav-tab" role="tablist">
                                                <a class="nav-item nav-link active" id="pop1-tab" data-toggle="tab" href="#pop1" role="tab" aria-controls="pop1" aria-selected="true">Product Details</a>
                                                <a class="nav-item nav-link" id="pop2-tab" data-toggle="tab" href="#pop2" role="tab" aria-controls="pop2" aria-selected="false">SEO Fields</a>
                                                <a class="nav-item nav-link" id="pop3-tab" data-toggle="tab" href="#pop3" role="tab" aria-controls="pop3" aria-selected="false">Tags</a>
                                                <a class="nav-item nav-link" id="pop4-tab" data-toggle="tab" href="#pop4" role="tab" aria-controls="pop3" aria-selected="false">Color Pallete</a>
                                                <a class="nav-item nav-link" id="pop5-tab" data-toggle="tab" href="#pop5" role="tab" aria-controls="pop3" aria-selected="false">Gallery</a>
                                                <a class="nav-item nav-link" id="pop5-tab" data-toggle="tab" href="#pop6" role="tab" aria-controls="pop3" aria-selected="false">Pricing</a>
                                                <a class="nav-item nav-link" id="pop5-tab" data-toggle="tab" href="#pop7" role="tab" aria-controls="pop3" aria-selected="false">Availability</a>
                                                <a class="nav-item nav-link" id="pop5-tab" data-toggle="tab" href="#pop8" role="tab" aria-controls="pop3" aria-selected="false">Addons</a>
                                             </div>
                                          </nav>
                                          <div class="tab-content" id="nav-tabContent">
                                             <div class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">
                                                <div class="pt-3"></div>
                                                <div class="row">
                                                   <div class="col-md-6">
                                                      <div class="form-group">
                                                         <label for="prd">Product Name</label>
                                                         <input type="text" class="form-control" id="prd" value="">
                                                      </div>
                                                   </div>
                                                   <div class="col-md-6">
                                                      <div class="form-group">
                                                         <label for="stu">Status:</label>
                                                         <select class="form-control" id="stu">
                                                            <option>Select one</option>
                                                            <option>Item 1</option>
                                                            <option>Item 2</option>
                                                            <option>Item 3</option>
                                                            <option>Item 4</option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-12">
                                                      <div class="form-group">
                                                         <label for="shtdesc">Product Description</label>
                                                         <textarea type="text" class="form-control text-area" rows="4" id="shtdesc"></textarea>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="tab-pane fade" id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
                                                <div class="pt-3"></div>
                                                <form action="/action_page.php">
                                                   <div class="form-group">
                                                      <label for="description">Meta Description</label>
                                                      <textarea type="text" class="form-control text-area" rows="4" id="shtdesc"></textarea>
                                                   </div>
                                                   <div class="form-group">
                                                      <label for="shtdesc">Meta Keywords</label>
                                                      <input type="text" value="Lorem Ipsum, Lorem Ipsum, Lorem Ipsum" name="tags1" />
                                                   </div>
                                                </form>
                                             </div>
                                             <div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
                                                <div class="pt-3"></div>
                                                <form action="/action_page.php">
                                                   <div class="form-group">
                                                      <label for="shtdesc">Tags</label>
                                                      <input type="text" value="Lorem Ipsum, Lorem Ipsum, Lorem Ipsum" name="tags2" />
                                                   </div>
                                                </form>
                                             </div>
                                             <div class="tab-pane fade" id="pop4" role="tabpanel" aria-labelledby="pop3-tab">
                                                <div class="pt-3"></div>
                                                <div class="container">
                                                   <div class="row justify-content-center">
                                                      <div class="col-12">
                                                         <form role="form">
                                                            <fieldset>
                                                               <legend>
                                                                  Choose Color
                                                               </legend>
                                                               <label class="custom-control black-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control lime-green-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control silver-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control blue-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control maroon-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control brown-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control khaki-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control orange-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control silver-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control gray-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control red-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control yellow-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control olive-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control lime-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control green-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control aqua-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control honeydew-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control azure-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control beige-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control ivory-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control lavenderblush-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control darkslategray-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control green-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control brown-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control aliceblue-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control antiquewhite-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control aquamarine-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control bisque-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control blanchedalmond-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control blueviolet-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control cadetblue-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                               <label class="custom-control chartreuse-checkbox checkBox">
                                                               <input type="checkbox" class="fill-control-input">
                                                               <span class="fill-control-indicator"></span>
                                                               <span class="fill-control-description"></span>
                                                               </label>
                                                            </fieldset>
                                                         </form>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="tab-pane fade" id="pop5" role="tabpanel" aria-labelledby="pop2-tab">
                                                <div class="pt-3"></div>
                                                <div class="row">
                                                   <div class="col-md-4">
                                                      <div class="box">
                                                         <div class="js--image-preview"></div>
                                                         <div class="upload-options">
                                                            <label>
                                                            <input type="file" class="image-upload" accept="image/*" />
                                                            </label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-4">
                                                      <div class="box">
                                                         <div class="js--image-preview"></div>
                                                         <div class="upload-options">
                                                            <label>
                                                            <input type="file" class="image-upload" accept="image/*" />
                                                            </label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-4">
                                                      <div class="box">
                                                         <div class="js--image-preview"></div>
                                                         <div class="upload-options">
                                                            <label>
                                                            <input type="file" class="image-upload" accept="image/*" />
                                                            </label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="tab-pane fade" id="pop6" role="tabpanel" aria-labelledby="pop3-tab">
                                                <div class="pt-3"></div>
                                                <div class="row">
                                                   <div class="col-md-6">
                                                      <div class="form-group">
                                                         <label for="exc">Tax Excluding Price</label>
                                                         <input type="text" class="form-control" id="exc" value="">
                                                      </div>
                                                   </div>
                                                   <div class="col-md-6">
                                                      <div class="form-group">
                                                         <label for="inc">Tax Including Price</label>
                                                         <input type="text" class="form-control" id="inc" value="">
                                                      </div>
                                                   </div>
                                                   <div class="col-md-6">
                                                      <div class="form-group">
                                                         <label for="tax">Tax Rate</label>
                                                         <input type="text" class="form-control" id="tax" value="%">
                                                      </div>
                                                   </div>
                                                   <div class="col-md-6">
                                                      <div class="form-group">
                                                         <label for="rgp">Regular Price</label>
                                                         <input type="text" class="form-control" id="rgp" value="">
                                                      </div>
                                                   </div>
                                                   <div class="col-md-12">
                                                      <div class="form-group">
                                                         <label for="sp">Sale Price</label>
                                                         <input type="text" class="form-control" id="sp" value="">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="tab-pane fade" id="pop7" role="tabpanel" aria-labelledby="pop3-tab">
                                                <div class="pt-3"></div>
                                                <form action="/action_page.php">
                                                   <div class="form-group">
                                                      <label for="prd">Stock Quantity</label>
                                                      <input type="Number"class="form-control" id="prd" value="">
                                                   </div>
                                                   <div class="form-group">
                                                      <label for="stu">Stock Availability</label>
                                                      <select class="form-control" id="stu">
                                                         <option>Limited Stock</option>
                                                         <option>Unlimited Stock</option>
                                                         <option>No Stock</option>
                                                      </select>
                                                   </div>
                                                </form>
                                             </div>
                                             <div class="tab-pane fade" id="pop8" role="tabpanel" aria-labelledby="pop3-tab">
                                                <div class="pt-3"></div>
                                                <form enctype="multipart/form-data">
                                                   <table class="table table-borderless add-ons-table" id="tbl_posts">
                                                      <thead>
                                                         <tr>
                                                            <th>Option</th>
                                                            <th>Price</th>
                                                            <th></th>
                                                            <th></th>
                                                         </tr>
                                                      </thead>
                                                      <tbody id="tbl_posts_body">
                                                         <tr id="rec-1">
                                                            <td>
                                                               <div class="form-group">
                                                                  <input type="text" class="form-control" id="description" placeholder="Option">
                                                               </div>
                                                            </td>
                                                            <td>
                                                               <div class="form-group">
                                                                  <select class="form-control" id="stu">
                                                                     <option>Flat Rate</option>
                                                                     <option>Standard Rate</option>
                                                                  </select>
                                                               </div>
                                                            </td>
                                                            <td>
                                                               <div class="form-group">
                                                                  <input type="number" class="form-control" id="description" value="0.00">
                                                               </div>
                                                            </td>
                                                            <td>
                                                               <div class="delete-btn"><a class="btn btn-xs delete-record" data-id="0"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <div style="display:none;">
                                                      <table id="sample_table">
                                                         <tr id="">
                                                            <td>
                                                               <div class="form-group">
                                                                  <input type="text" class="form-control" id="description" placeholder="Option">
                                                               </div>
                                                            </td>
                                                            <td>
                                                               <div class="form-group">
                                                                  <select class="form-control" id="stu">
                                                                     <option>Flat Rate</option>
                                                                     <option>Standard Rate</option>
                                                                  </select>
                                                               </div>
                                                            </td>
                                                            <td>
                                                               <div class="form-group">
                                                                  <input type="number" class="form-control" id="description" value="0.00">
                                                               </div>
                                                            </td>
                                                            <td>
                                                               <div class="delete-btn"><a class="btn btn-xs delete-record" data-id="0"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                                            </td>
                                                         </tr>
                                                      </table>
                                                   </div>
                                                   <div class="well ">
                                                      <a class="btn  pull-right add-record save" data-added="0"><i class="glyphicon glyphicon-plus"></i> Add Row</a>
                                                   </div>
                                                </form>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="grid-view">
                                    <div class="row">
                                       <div class="col-xl-4 col-lg-6">
                                          <div class="grid-box">
                                             <div class="image">
                                                <img src="assets/images/image-1.jpg">
                                                <div class="name">Succulent Terrariums</div>
                                             </div>
                                             <div class="grid-desc">
                                                <div class="row">
                                                   <div class="col-sm-5">
                                                      <p>Cost - $15</p>
                                                   </div>
                                                   <div class="col-sm-7 text-right">
                                                      <p>Selling Price - $15</p>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <p>Status - Lorum</p>
                                                   </div>
                                                   <div class="col-sm-6 text-right">
                                                      <p>Total Sold - $20</p>
                                                   </div>
                                                   <div class="col-lg-12 text-right">
                                                      <div class="edit">
                                                         <ul>
                                                            <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-xl-4 col-lg-6">
                                          <div class="grid-box">
                                             <div class="image">
                                                <img src="assets/images/image-1.jpg">
                                                <div class="name">Succulent Terrariums</div>
                                             </div>
                                             <div class="grid-desc">
                                                <div class="row">
                                                   <div class="col-sm-5">
                                                      <p>Cost - $15</p>
                                                   </div>
                                                   <div class="col-sm-7 text-right">
                                                      <p>Selling Price - $15</p>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <p>Status - Lorum</p>
                                                   </div>
                                                   <div class="col-sm-6 text-right">
                                                      <p>Total Sold - $20</p>
                                                   </div>
                                                   <div class="col-lg-12 text-right">
                                                      <div class="edit">
                                                         <ul>
                                                            <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-xl-4 col-lg-6">
                                          <div class="grid-box">
                                             <div class="image">
                                                <img src="assets/images/image-1.jpg">
                                                <div class="name">Succulent Terrariums</div>
                                             </div>
                                             <div class="grid-desc">
                                                <div class="row">
                                                   <div class="col-sm-5">
                                                      <p>Cost - $15</p>
                                                   </div>
                                                   <div class="col-sm-7 text-right">
                                                      <p>Selling Price - $15</p>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <p>Status - Lorum</p>
                                                   </div>
                                                   <div class="col-sm-6 text-right">
                                                      <p>Total Sold - $20</p>
                                                   </div>
                                                   <div class="col-lg-12 text-right">
                                                      <div class="edit">
                                                         <ul>
                                                            <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-xl-4 col-lg-6">
                                          <div class="grid-box">
                                             <div class="image">
                                                <img src="assets/images/image-1.jpg">
                                                <div class="name">Succulent Terrariums</div>
                                             </div>
                                             <div class="grid-desc">
                                                <div class="row">
                                                   <div class="col-sm-5">
                                                      <p>Cost - $15</p>
                                                   </div>
                                                   <div class="col-sm-7 text-right">
                                                      <p>Selling Price - $15</p>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <p>Status - Lorum</p>
                                                   </div>
                                                   <div class="col-sm-6 text-right">
                                                      <p>Total Sold - $20</p>
                                                   </div>
                                                   <div class="col-md-12 text-right">
                                                      <div class="edit">
                                                         <ul>
                                                            <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-xl-4 col-lg-6">
                                          <div class="grid-box">
                                             <div class="image">
                                                <img src="assets/images/image-1.jpg">
                                                <div class="name">Succulent Terrariums</div>
                                             </div>
                                             <div class="grid-desc">
                                                <div class="row">
                                                   <div class="col-sm-5">
                                                      <p>Cost - $15</p>
                                                   </div>
                                                   <div class="col-sm-7 text-right">
                                                      <p>Selling Price - $15</p>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <p>Status - Lorum</p>
                                                   </div>
                                                   <div class="col-sm-6 text-right">
                                                      <p>Total Sold - $20</p>
                                                   </div>
                                                   <div class="col-sm-12 text-right">
                                                      <div class="edit">
                                                         <ul>
                                                            <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-xl-4 col-lg-6">
                                          <div class="grid-box">
                                             <div class="image">
                                                <img src="assets/images/image-1.jpg">
                                                <div class="name">Succulent Terrariums</div>
                                             </div>
                                             <div class="grid-desc">
                                                <div class="row">
                                                   <div class="col-sm-5">
                                                      <p>Cost - $15</p>
                                                   </div>
                                                   <div class="col-sm-7 text-right">
                                                      <p>Selling Price - $15</p>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <p>Status - Lorum</p>
                                                   </div>
                                                   <div class="col-sm-6 text-right">
                                                      <p>Total Sold - $20</p>
                                                   </div>
                                                   <div class="col-lg-12 text-right">
                                                      <div class="edit">
                                                         <ul>
                                                            <li><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-clone" aria-hidden="true"></i></a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!---->
                  </div>
               </div>
            </div>
         </div>
         <!---->
      </div>
      <!---->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="assets/js/bootstrap.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
      <script src="assets/js/tagger.js"></script>
      <script>
         var t1 = tagger(document.querySelector('[name="tags1"]'), {
           allow_duplicates: true,
           allow_spaces: true,
         });
         var t2 = tagger(document.querySelector('[name="tags2"]'), {
           allow_duplicates: true,
           allow_spaces: true,
         });
      </script>
      <script>
         $(document).ready(function () {
         $('#productList').DataTable();
         $('.dataTables_length').addClass('bs-select');
         });	  
         
      </script>
      <script>
         $(document).ready(function(){
           $(".gridView").click(function(){
         	$('.grid-view').addClass("display-block");
         	$('.product-list-table').addClass("display-none");
           });
         $(".listingView").click(function(){
         	$('.grid-view').removeClass("display-block");
         	$('.product-list-table').removeClass("display-none");
           });
         });
      </script>
      <script>
         $(document).ready(function(){
           $(".orderTables .product-list-table tbody tr").click(function(){
         	$('.product-descr').addClass("display-block");
         	$('.add-product-heading').addClass("display-block");
         	$('.product-list-table').addClass("display-none");
         	$('.grid').addClass("display-none");
           });
         	$(".listingView").click(function(){
         	$('.grid-view').removeClass("display-block");
         	$('.product-list-table').removeClass("display-none");
           });
         	$(".back-button").click(function(){
         	$('.product-descr').removeClass("display-block");
         	$('.product-detail-heading').removeClass("display-block");
         	$('.productTable').removeClass("display-none");
         	$('.grid').removeClass("display-none");
         	$('.add-product-heading').removeClass("display-block");
           });
         	$(".grid-box").click(function(){
         	$('.grid-view').removeClass("display-block");
         	$('.add-product-heading').addClass("display-block");
         	$('.product-descr').addClass("display-block");
         	$('.grid').addClass("display-none");
           });
         	$(".add-product").click(function(){
         	$('.grid').addClass("display-none");
         	$('.grid-view').removeClass("display-block");
         	$('.productTable').addClass("display-none");
         	$('.product-descr').addClass("display-block");
         	$('.add-product-heading').addClass("display-block");
           });
         });
      </script>
      <script>
         jQuery(document).delegate('a.add-record', 'click', function(e) {
            e.preventDefault();    
            var content = jQuery('#sample_table tr'),
            size = jQuery('#tbl_posts >tbody >tr').length + 1,
            element = null,    
            element = content.clone();
            element.attr('id', 'rec-'+size);
            element.find('.delete-record').attr('data-id', size);
            element.appendTo('#tbl_posts_body');
            element.find('.sn').html(size);
          });
      </script>
      <script>
         jQuery(document).delegate('a.delete-record', 'click', function(e) {
            e.preventDefault();    
            var didConfirm = confirm("Are you sure You want to delete");
            if (didConfirm == true) {
             var id = jQuery(this).attr('data-id');
             var targetDiv = jQuery(this).attr('targetDiv');
             jQuery('#rec-' + id).remove();
             
           $('#tbl_posts_body tr').each(function(index) {
             $(this).find('span.sn').html(index+1);
           });
           return true;
         } else {
           return false;
         }
         });
      </script>
      <script>
         function initImageUpload(box) {
          let uploadField = box.querySelector('.image-upload');
         
          uploadField.addEventListener('change', getFile);
         
          function getFile(e){
            let file = e.currentTarget.files[0];
            checkType(file);
          }
          
          function previewImage(file){
            let thumb = box.querySelector('.js--image-preview'),
                reader = new FileReader();
         
            reader.onload = function() {
              thumb.style.backgroundImage = 'url(' + reader.result + ')';
            }
            reader.readAsDataURL(file);
            thumb.className += ' js--no-default';
          }
         
          function checkType(file){
            let imageType = /image.*/;
            if (!file.type.match(imageType)) {
              throw 'Datei ist kein Bild';
            } else if (!file){
              throw 'Kein Bild gewählt';
            } else {
              previewImage(file);
            }
          }
          
         }
         
         var boxes = document.querySelectorAll('.box');
         
         for (let i = 0; i < boxes.length; i++) {
          let box = boxes[i];
          initDropEffect(box);
          initImageUpload(box);
         }
         function initDropEffect(box){
          let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
          
          area = box.querySelector('.js--image-preview');
          area.addEventListener('click', fireRipple);
          
          function fireRipple(e){
            area = e.currentTarget
            if(!drop){
              drop = document.createElement('span');
              drop.className = 'drop';
              this.appendChild(drop);
            }
            drop.className = 'drop';
            
            areaWidth = getComputedStyle(this, null).getPropertyValue("width");
            areaHeight = getComputedStyle(this, null).getPropertyValue("height");
            maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));
         
            drop.style.width = maxDistance + 'px';
            drop.style.height = maxDistance + 'px';
            
            dropWidth = getComputedStyle(this, null).getPropertyValue("width");
            dropHeight = getComputedStyle(this, null).getPropertyValue("height");
         
            x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
            y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
            
            drop.style.top = y + 'px';
            drop.style.left = x + 'px';
            drop.className += ' animate';
            e.stopPropagation();
            
          }
         }
         
      </script>
		  		  <script>
		  $(document).ready(function(){
			$('.barIcon').click(function() { 
			$('.br-sideleft').toggleClass('show')
			$('.pcodedContent').toggleClass('show')
		      });  
			});  
		  </script>
   </body>
</html>