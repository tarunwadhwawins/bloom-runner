<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Bloom Runner</title>
      <!--favicon-->
     <link rel="icon" type="image/x-icon" href="assets/images/favicon.png">
      <!--common css-->
      <link rel="stylesheet" href="assets/css/common.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/header.css" type="text/css"/>
	  <link rel="stylesheet" href="assets/css/sidebar.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/fonts.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/footer.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css"/>
      <link rel="stylesheet" href="assets/css/materialdesignicons.min.css" type="text/css"/>
      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,
				  alert("hiii");100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
	  
      <!---->
   </head>
   <body>
      <!---->
      <div class="pcoded-container navbar-wrapper">
      <?php include_once('common/header.php') ?>
      <!---->
      <div class="mainContainer">
       <?php include_once('common/sidebar.php') ?> 
	  <!----> 
         <div class="pcodedContent">
            <div class="pcodedInnerContent">
               <div class="pageBody">
				<div class="row">
				  <div class="col-sm-12">
					<h4 class="page-title">Dashboard</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
					 </ol>
				 </div>
				  </div>
				<div class="top-widgets">
                  <div class="row">
                     <div class="col-md-6 col-xl-4 grid-margin stretch-card">
                        <div class="card card-statistics bg-1">
                           <div class="card-body">
							<div class="row">
								<div class="col-md-5">
									<div class="icon">
										<span><i class="fa fa-user-plus" aria-hidden="true"></i></span>
									</div>
								</div>
								<div class="col-md-7">
									<div class="desc">
									<h4>New Clients</h4>
									<h4>20</h4>
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:30%">
										  <span class="sr-only">30% Complete</span>
										</div>
									</div>
									<p>30% Increase</p>
									</div>
								</div>
							</div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-xl-4 grid-margin stretch-card">
                        <div class="card card-statistics bg-1">
                           <div class="card-body">
							<div class="row">
								<div class="col-md-5">
									<div class="icon">
										<span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>
									</div>
								</div>
								<div class="col-md-7">
									<div class="desc">
									<h4>New Orders</h4>
									<h4>15</h4>
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:10%">
										  <span class="sr-only">10% Complete</span>
										</div>
									</div>
									<p>10% Increase</p>
									</div>
								</div>
							</div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-xl-4 grid-margin stretch-card">
                        <div class="card card-statistics bg-1">
                           <div class="card-body">
							<div class="row">
								<div class="col-md-5">
									<div class="icon">
										<span><i class="fa fa-usd" aria-hidden="true"></i></span>
									</div>
								</div>
								<div class="col-md-7">
									<div class="desc">
									<h4>New Subscriptions</h4>
									<h4>40</h4>
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:55%">
										  <span class="sr-only">55% Complete</span>
										</div>
									</div>
									<p>55% Increase</p>
									</div>
								</div>
							</div>
                           </div>
                        </div>
                     </div>
                  </div>
				</div>
				
				
				<div class="graph-widgets">
                  <div class="row">
					<div class="col-md-5">
                       <div class="graph-boxs">
							<div class="card card-statistics">
                           <div class="card-body">
                              <div class="heading">
								<h5>Product Sale</h5>
								<div class="ellipsis">
								<ul>
								<li><a href=""><i class="fa fa-minus" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-times" aria-hidden="true"></i></a></li>
								</ul>
                                </div>
                           </div>
							<img src="assets/images/bar-graph.png" alt="svg">
                        </div>
						</div>
                     </div>
                  </div>

					<div class="col-md-7">
                       <div class="graph-boxs">
							<div class="card card-statistics">
                           <div class="card-body">
                              <div class="heading">
								<h5>Active Users</h5>
								<div class="ellipsis">
								<ul>
								<li><a href=""><i class="fa fa-minus" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-times" aria-hidden="true"></i></a></li>
								</ul>
                                </div>
									</div>
							<img src="assets/images/earth-graph.png" alt="svg">
						
                     </div>
                  </div>
				</div>
				</div>

					

                     <div class="col-md-6">
                       <div class="graph-boxs">
							<div class="card card-statistics">
                           <div class="card-body">
                              <div class="heading">
								<h5>Sales</h5>
								<div class="ellipsis">
								<ul>
								<li><a href=""><i class="fa fa-minus" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-times" aria-hidden="true"></i></a></li>
								</ul>
                                </div>
                           </div>
							<img src="svg/order-day.svg" alt="svg">
                        </div>
						</div>
                     </div>
                  </div>

					<div class="col-md-6">
                       <div class="graph-boxs">
							<div class="card card-statistics">
                           <div class="card-body">
                              <div class="heading">
								<h5>Our Recipients</h5>
								<div class="ellipsis">
								<ul>
								<li><a href=""><i class="fa fa-minus" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-times" aria-hidden="true"></i></a></li>
								</ul>
                                </div>
                           </div>
							<img src="svg/delivery-man.svg" alt="svg">
                        </div>
						</div>
                     </div>
                  </div>
			
               </div>
			 </div>
            </div>
         </div>
         <!---->
      </div>
      <!---->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
		  <script>
		  $(document).ready(function(){
			$('.barIcon').click(function() { 
			$('.br-sideleft').toggleClass('show')
			$('.pcodedContent').toggleClass('show')
		      });  
			});  
			  
			 $(document).ready(function(){
			$('.searchBar').click(function() { 
			$('.searchBox').toggleClass('show')
		      });  
			});  
		  </script>
<!--
	 <script>
	    $(document).ready(function(){
         $(".menu-link ul").hide();			
         // $(".menu-link ul").find('li').hide();
			 $('.menu-link').click(function() {
				 if($(this).find('a:first').hasClass('active'))
				 {
					 $(this).children('ul').slideToggle();
				 }
				 else
				 {
					 $('.menu-link').find('a:first').removeClass('active');
					 $(this).find('a:first').addClass('active');

					 $('.menu-link').children('ul').hide();
					 $(this).children('ul').slideToggle();
				 } 
			 });
         });  
	  </script>
-->
   </body>
</html>